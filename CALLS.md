## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Alkira. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Alkira.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Alkira. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">loginUsersUsingPOST(body, callback)</td>
    <td style="padding:15px">loginUsersUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/users/login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logoutUsersUsingPOST(name, body, callback)</td>
    <td style="padding:15px">logoutUsersUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/users/logout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLogin(body, callback)</td>
    <td style="padding:15px">login</td>
    <td style="padding:15px">{base_path}/{version}/login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">oauthLogin(body, callback)</td>
    <td style="padding:15px">oauthLogin</td>
    <td style="padding:15px">{base_path}/{version}/oauth/login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogout(callback)</td>
    <td style="padding:15px">logout</td>
    <td style="padding:15px">{base_path}/{version}/logout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSession(body, callback)</td>
    <td style="padding:15px">createSession</td>
    <td style="padding:15px">{base_path}/{version}/sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSession(callback)</td>
    <td style="padding:15px">deleteSession</td>
    <td style="padding:15px">{base_path}/{version}/sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">otpLogin(otp, callback)</td>
    <td style="padding:15px">otpLogin</td>
    <td style="padding:15px">{base_path}/{version}/otp/login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getsegments(tenantNetworkId, callback)</td>
    <td style="padding:15px">getsegments</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/segments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSegment(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createSegment</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/segments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSegment(tenantNetworkId, segmentId, callback)</td>
    <td style="padding:15px">getSegment</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/segments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSegment(tenantNetworkId, segmentId, body, callback)</td>
    <td style="padding:15px">updateSegment</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/segments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSegment(tenantNetworkId, segmentId, callback)</td>
    <td style="padding:15px">deleteSegment</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/segments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgroups(tenantNetworkId, segmentId, serviceId, groupTypes, callback)</td>
    <td style="padding:15px">getgroups</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">creategroups(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">creategroups</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgroup(tenantNetworkId, groupId, callback)</td>
    <td style="padding:15px">getgroup</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/groups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updategroups(tenantNetworkId, groupId, body, callback)</td>
    <td style="padding:15px">updategroups</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/groups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletegroups(tenantNetworkId, groupId, callback)</td>
    <td style="padding:15px">deletegroups</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/groups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllUsersUsingGET(userName, active, callback)</td>
    <td style="padding:15px">getAllUsersUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUsersUsingPOST(body, callback)</td>
    <td style="padding:15px">createUsersUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersByIdUsingGET(name, userId, callback)</td>
    <td style="padding:15px">getUsersByIdUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUsersByIdUsingPUT(userId, body, callback)</td>
    <td style="padding:15px">updateUsersByIdUsingPUT</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUsersByIdUsingDELETE(name, userId, callback)</td>
    <td style="padding:15px">deleteUsersByIdUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserProfileUsingGET(name, callback)</td>
    <td style="padding:15px">getUserProfileUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/userprofile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUserProfileUsingPUT(name, body, callback)</td>
    <td style="padding:15px">updateUserProfileUsingPUT</td>
    <td style="padding:15px">{base_path}/{version}/userprofile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getusergroups(callback)</td>
    <td style="padding:15px">getusergroups</td>
    <td style="padding:15px">{base_path}/{version}/user-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUserGroup(body, callback)</td>
    <td style="padding:15px">createUserGroup</td>
    <td style="padding:15px">{base_path}/{version}/user-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserGroup(usergroupId, callback)</td>
    <td style="padding:15px">getUserGroup</td>
    <td style="padding:15px">{base_path}/{version}/user-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUserGroup(usergroupId, body, callback)</td>
    <td style="padding:15px">updateUserGroup</td>
    <td style="padding:15px">{base_path}/{version}/user-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUserGroup(usergroupId, callback)</td>
    <td style="padding:15px">deleteUserGroup</td>
    <td style="padding:15px">{base_path}/{version}/user-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersInUserGroup(usergroupId, callback)</td>
    <td style="padding:15px">getUsersInUserGroup</td>
    <td style="padding:15px">{base_path}/{version}/user-groups/{pathv1}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addUsersToUserGroup(usergroupId, body, callback)</td>
    <td style="padding:15px">addUsersToUserGroup</td>
    <td style="padding:15px">{base_path}/{version}/user-groups/{pathv1}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceUsersInUserGroup(usergroupId, body, callback)</td>
    <td style="padding:15px">replaceUsersInUserGroup</td>
    <td style="padding:15px">{base_path}/{version}/user-groups/{pathv1}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserFromUserGroup(usergroupId, userId, callback)</td>
    <td style="padding:15px">getUserFromUserGroup</td>
    <td style="padding:15px">{base_path}/{version}/user-groups/{pathv1}/users/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUserFromUserGroup(usergroupId, userId, callback)</td>
    <td style="padding:15px">deleteUserFromUserGroup</td>
    <td style="padding:15px">{base_path}/{version}/user-groups/{pathv1}/users/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRolesUsingGET(callback)</td>
    <td style="padding:15px">getRolesUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRoleUsingPOST(body, callback)</td>
    <td style="padding:15px">createRoleUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoleByIdUsingGET(roleId, callback)</td>
    <td style="padding:15px">getRoleByIdUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRoleUsingPUT1(roleId, body, callback)</td>
    <td style="padding:15px">updateRoleUsingPUT_1</td>
    <td style="padding:15px">{base_path}/{version}/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRoleByIdUsingDELETE(roleId, callback)</td>
    <td style="padding:15px">deleteRoleByIdUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPermissionsUsingGET(showAll, userName, callback)</td>
    <td style="padding:15px">getPermissionsUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityProvidersUsingGET(callback)</td>
    <td style="padding:15px">getIdentityProvidersUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/identityproviders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIdentityProviderUsingPOST(body, callback)</td>
    <td style="padding:15px">addIdentityProviderUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/identityproviders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityProviderUsingGET(identityProviderId, callback)</td>
    <td style="padding:15px">getIdentityProviderUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/identityproviders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIdentityProviderUsingPUT(identityProviderId, body, callback)</td>
    <td style="padding:15px">updateIdentityProviderUsingPUT</td>
    <td style="padding:15px">{base_path}/{version}/identityproviders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIdentityProviderUsingDELETE(identityProviderId, callback)</td>
    <td style="padding:15px">deleteIdentityProviderUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/identityproviders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getconnectors(tenantNetworkId, callback)</td>
    <td style="padding:15px">getconnectors</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/connectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConnector(tenantNetworkId, connectorId, callback)</td>
    <td style="padding:15px">getConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/connectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getawsvpcconnectors(tenantNetworkId, callback)</td>
    <td style="padding:15px">getawsvpcconnectors</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/awsvpcconnectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAWSVPCConnector(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createAWSVPCConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/awsvpcconnectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAWSVPCConnector(tenantNetworkId, awsvpcconnectorId, callback)</td>
    <td style="padding:15px">getAWSVPCConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/awsvpcconnectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAWSVPCConnector(tenantNetworkId, awsvpcconnectorId, body, callback)</td>
    <td style="padding:15px">updateAWSVPCConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/awsvpcconnectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAWSVPCConnector(tenantNetworkId, awsvpcconnectorId, callback)</td>
    <td style="padding:15px">deleteAWSVPCConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/awsvpcconnectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getazurevnetconnectors(tenantNetworkId, callback)</td>
    <td style="padding:15px">getazurevnetconnectors</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/azurevnetconnectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAzureVNETConnector(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createAzureVNETConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/azurevnetconnectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAzureVNETConnector(tenantNetworkId, azurevnetconnectorId, callback)</td>
    <td style="padding:15px">getAzureVNETConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/azurevnetconnectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAzureVNETConnector(tenantNetworkId, azurevnetconnectorId, body, callback)</td>
    <td style="padding:15px">updateAzureVNETConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/azurevnetconnectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAzureVNETConnector(tenantNetworkId, azurevnetconnectorId, callback)</td>
    <td style="padding:15px">deleteAzureVNETConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/azurevnetconnectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgcpconnectors(tenantNetworkId, callback)</td>
    <td style="padding:15px">getgcpconnectors</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/gcpvpcconnectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGCPConnector(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createGCPConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/gcpvpcconnectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGCPConnector(tenantNetworkId, gcpconnectorId, callback)</td>
    <td style="padding:15px">getGCPConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/gcpvpcconnectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGCPConnector(tenantNetworkId, gcpconnectorId, body, callback)</td>
    <td style="padding:15px">updateGCPConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/gcpvpcconnectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGCPConnector(tenantNetworkId, gcpconnectorId, callback)</td>
    <td style="padding:15px">deleteGCPConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/gcpvpcconnectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOCIVCNConnectors(tenantNetworkId, callback)</td>
    <td style="padding:15px">getOCIVCNConnectors</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/oci-vcn-connectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOCIVCNConnector(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createOCIVCNConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/oci-vcn-connectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOCIVCNConnector(tenantNetworkId, oCIVCNConnectorId, callback)</td>
    <td style="padding:15px">getOCIVCNConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/oci-vcn-connectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOCIVCNConnector(tenantNetworkId, oCIVCNConnectorId, body, callback)</td>
    <td style="padding:15px">updateOCIVCNConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/oci-vcn-connectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOCIVCNConnector(tenantNetworkId, oCIVCNConnectorId, callback)</td>
    <td style="padding:15px">deleteOCIVCNConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/oci-vcn-connectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getipsecconnectors(tenantNetworkId, callback)</td>
    <td style="padding:15px">getipsecconnectors</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/ipsecconnectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIPSecConnector(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createIPSecConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/ipsecconnectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPSecConnector(tenantNetworkId, ipsecconnectorId, callback)</td>
    <td style="padding:15px">getIPSecConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/ipsecconnectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIPSecConnector(tenantNetworkId, ipsecconnectorId, body, callback)</td>
    <td style="padding:15px">updateIPSecConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/ipsecconnectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIPSecConnector(tenantNetworkId, ipsecconnectorId, callback)</td>
    <td style="padding:15px">deleteIPSecConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/ipsecconnectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPSecConnectorSiteConfiguration(tenantNetworkId, ipsecconnectorId, siteId, callback)</td>
    <td style="padding:15px">getIPSecConnectorSiteConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/ipsecconnectors/{pathv2}/sites/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getciscosdwaningresses(tenantNetworkId, callback)</td>
    <td style="padding:15px">getciscosdwaningresses</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/ciscosdwaningresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCiscoSDWANIngress(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createCiscoSDWANIngress</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/ciscosdwaningresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCiscoSDWANIngress(tenantNetworkId, ciscosdwaningressId, callback)</td>
    <td style="padding:15px">getCiscoSDWANIngress</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/ciscosdwaningresses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCiscoSDWANIngress(tenantNetworkId, ciscosdwaningressId, body, callback)</td>
    <td style="padding:15px">updateCiscoSDWANIngress</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/ciscosdwaningresses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCiscoSDWANIngress(tenantNetworkId, ciscosdwaningressId, callback)</td>
    <td style="padding:15px">deleteCiscoSDWANIngress</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/ciscosdwaningresses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCiscoSDWANConnectorInstanceConfiguration(tenantNetworkId, ciscosdwaningressId, instanceId, callback)</td>
    <td style="padding:15px">getCiscoSDWANConnectorInstanceConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/ciscosdwaningresses/{pathv2}/instances/{pathv3}/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getinternetconnectors(tenantNetworkId, callback)</td>
    <td style="padding:15px">getinternetconnectors</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/internetconnectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createInternetConnector(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createInternetConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/internetconnectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInternetConnector(tenantNetworkId, internetconnectorId, callback)</td>
    <td style="padding:15px">getInternetConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/internetconnectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateInternetConnector(tenantNetworkId, internetconnectorId, body, callback)</td>
    <td style="padding:15px">updateInternetConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/internetconnectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInternetConnector(tenantNetworkId, internetconnectorId, callback)</td>
    <td style="padding:15px">deleteInternetConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/internetconnectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInternetConnectorConfiguration(tenantNetworkId, internetconnectorId, callback)</td>
    <td style="padding:15px">getInternetConnectorConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/internetconnectors/{pathv2}/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getdirectconnectconnectors(tenantNetworkId, callback)</td>
    <td style="padding:15px">getdirectconnectconnectors</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/directconnectconnectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDirectConnectConnector(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createDirectConnectConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/directconnectconnectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDirectConnectConnector(tenantNetworkId, directconnectconnectorId, callback)</td>
    <td style="padding:15px">getDirectConnectConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/directconnectconnectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDirectConnectConnector(tenantNetworkId, directconnectconnectorId, body, callback)</td>
    <td style="padding:15px">updateDirectConnectConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/directconnectconnectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDirectConnectConnector(tenantNetworkId, directconnectconnectorId, callback)</td>
    <td style="padding:15px">deleteDirectConnectConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/directconnectconnectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDirectConnectConnectorInstanceConfiguration(tenantNetworkId, directconnectconnectorId, instanceId, callback)</td>
    <td style="padding:15px">getDirectConnectConnectorInstanceConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/directconnectconnectors/{pathv2}/instances/{pathv3}/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAzureErConnectors(tenantNetworkId, callback)</td>
    <td style="padding:15px">getAzureErConnectors</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/azure-express-route-connectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAzureErConnector(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createAzureErConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/azure-express-route-connectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAzureErConnector(tenantNetworkId, azureErConnectorId, callback)</td>
    <td style="padding:15px">getAzureErConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/azure-express-route-connectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAzureErConnector(tenantNetworkId, azureErConnectorId, body, callback)</td>
    <td style="padding:15px">updateAzureErConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/azure-express-route-connectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAzureErConnector(tenantNetworkId, azureErConnectorId, callback)</td>
    <td style="padding:15px">deleteAzureErConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/azure-express-route-connectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAzureErConnectorInstanceConfiguration(tenantNetworkId, azureErConnectorId, instanceId, callback)</td>
    <td style="padding:15px">getAzureErConnectorInstanceConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/azure-express-route-connectors/{pathv2}/instances/{pathv3}/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditLogs(status, type, search, sortBy, offset, limit, startTime, endTime, tags, callback)</td>
    <td style="padding:15px">getAuditLogs</td>
    <td style="padding:15px">{base_path}/{version}/auditlogs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditLogTags(limit, callback)</td>
    <td style="padding:15px">getAuditLogTags</td>
    <td style="padding:15px">{base_path}/{version}/auditlog-tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenantNetworks(callback)</td>
    <td style="padding:15px">getTenantNetworks</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTenantNetwork(body, callback)</td>
    <td style="padding:15px">createTenantNetwork</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenantNetwork(tenantNetworkId, callback)</td>
    <td style="padding:15px">getTenantNetwork</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTenantNetwork(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">updateTenantNetwork</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTenantNetwork(tenantNetworkId, callback)</td>
    <td style="padding:15px">deleteTenantNetwork</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">provisionTenantNetwork(tenantNetworkId, callback)</td>
    <td style="padding:15px">provisionTenantNetwork</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/provision?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrievestrafficstatsforconnectorsinthistenant(tenantNetworkId, segment, cxp, startTime, endTime, connectorId, instanceId, showBandwidth, type, callback)</td>
    <td style="padding:15px">Retrieves traffic stats for connectors in this tenant</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/stats/connectortraffic/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrievestheInterCXPtraffic(tenantNetworkId, segment, cxp, startTime, endTime, showBandwidth, remoteCXP, callback)</td>
    <td style="padding:15px">Retrieves the Inter-CXP traffic</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/stats/intercxptraffic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrievestheoveralltrafficgoingtointernetfromCXP(tenantNetworkId, segment, cxp, startTime, endTime, showBandwidth, callback)</td>
    <td style="padding:15px">Retrieves the overall traffic going to internet from CXP</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/stats/internettraffic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrievestrafficstatsforservicesinthistenant(tenantNetworkId, segment, cxp, startTime, endTime, serviceId, instanceId, showBandwidth, callback)</td>
    <td style="padding:15px">Retrieves traffic stats for services in this tenant</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/stats/servicetraffic/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrievesactivesessionscountforservicesinthistenant(tenantNetworkId, segment, cxp, startTime, endTime, serviceId, instanceId, callback)</td>
    <td style="padding:15px">Retrieves active sessions count for services in this tenant</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/stats/servicetraffic/{pathv2}/activesessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrievesinstancecountforservicesinthistenant(tenantNetworkId, segment, cxp, startTime, endTime, serviceId, callback)</td>
    <td style="padding:15px">Retrieves instance count for services in this tenant</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/stats/servicetraffic/{pathv2}/instancecount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getfirewallzones(tenantNetworkId, callback)</td>
    <td style="padding:15px">getfirewallzones</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/firewallzones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirewallZone(tenantNetworkId, firewallzoneId, callback)</td>
    <td style="padding:15px">getFirewallZone</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/firewallzones/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getinternetapplications(tenantNetworkId, callback)</td>
    <td style="padding:15px">getinternetapplications</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/internet-applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createInternetApplication(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createInternetApplication</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/internet-applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInternetApplication(tenantNetworkId, internetapplicationId, callback)</td>
    <td style="padding:15px">getInternetApplication</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/internet-applications/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateInternetApplication(tenantNetworkId, internetapplicationId, body, callback)</td>
    <td style="padding:15px">updateInternetApplication</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/internet-applications/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInternetApplication(tenantNetworkId, internetapplicationId, callback)</td>
    <td style="padding:15px">deleteInternetApplication</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/internet-applications/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getpanfwservices(tenantNetworkId, callback)</td>
    <td style="padding:15px">getpanfwservices</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/panfwservices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPANFWService(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createPANFWService</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/panfwservices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPANFWService(tenantNetworkId, serviceId, callback)</td>
    <td style="padding:15px">getPANFWService</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/panfwservices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePANFWService(tenantNetworkId, serviceId, body, callback)</td>
    <td style="padding:15px">updatePANFWService</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/panfwservices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePANFWService(tenantNetworkId, serviceId, callback)</td>
    <td style="padding:15px">deletePANFWService</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/panfwservices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPANFWServiceInstanceConfiguration(tenantNetworkId, serviceId, instanceId, callback)</td>
    <td style="padding:15px">getPANFWServiceInstanceConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/panfwservices/{pathv2}/instances/{pathv3}/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPANAutoscaleOptionsUsingGET(serviceId, tenantNetworkId, callback)</td>
    <td style="padding:15px">getPANAutoscaleOptionsUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/panfwservices/{pathv2}/autoscaleoptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePANAutoscaleOptionsUsingPUT(serviceId, tenantNetworkId, body, callback)</td>
    <td style="padding:15px">updatePANAutoscaleOptionsUsingPUT</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/panfwservices/{pathv2}/autoscaleoptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePANAutoscaleOptionsUsingDELETE(serviceId, tenantNetworkId, callback)</td>
    <td style="padding:15px">deletePANAutoscaleOptionsUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/panfwservices/{pathv2}/autoscaleoptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getservices(tenantNetworkId, callback)</td>
    <td style="padding:15px">getservices</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getService(tenantNetworkId, serviceId, callback)</td>
    <td style="padding:15px">getService</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listofallproducts(type = 'CXP', category = 'INFRA', size = 'SMALL', provider = 'CISCO', callback)</td>
    <td style="padding:15px">List of all products</td>
    <td style="padding:15px">{base_path}/{version}/inventory/products?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenantResourceLimits(callback)</td>
    <td style="padding:15px">getTenantResourceLimits</td>
    <td style="padding:15px">{base_path}/{version}/resourcelimits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourceUsage(category = 'UNKNOWN', type = 'UNKNOWN', scope = 'UNKNOWN', callback)</td>
    <td style="padding:15px">getResourceUsage</td>
    <td style="padding:15px">{base_path}/{version}/resourceusage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCXPs(name, state = 'AVAILABLE', callback)</td>
    <td style="padding:15px">getCXPs</td>
    <td style="padding:15px">{base_path}/{version}/inventory/cxps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getbillinginformation(callback)</td>
    <td style="padding:15px">Get billing information</td>
    <td style="padding:15px">{base_path}/{version}/billing-info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getinvoicesummaries(fromYearMonth, page, size, sort, toYearMonth, callback)</td>
    <td style="padding:15px">getinvoicesummaries</td>
    <td style="padding:15px">{base_path}/{version}/invoice-summaries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInvoiceSummary(invoiceId, callback)</td>
    <td style="padding:15px">getInvoiceSummary</td>
    <td style="padding:15px">{base_path}/{version}/invoice-summaries/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pingFromConnector(tenantNetworkId, connectorId, body, callback)</td>
    <td style="padding:15px">pingFromConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/connectors/{pathv2}/troubleshooting/ping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pingFromConnectorInstance(tenantNetworkId, connectorId, instanceId, body, callback)</td>
    <td style="padding:15px">pingFromConnectorInstance</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/connectors/{pathv2}/instances/{pathv3}/troubleshooting/ping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobs(status, type, search, sortBy, offset, limit, startTime, endTime, tags, callback)</td>
    <td style="padding:15px">getJobs</td>
    <td style="padding:15px">{base_path}/{version}/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJob(jobId, callback)</td>
    <td style="padding:15px">getJob</td>
    <td style="padding:15px">{base_path}/{version}/jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlerts(status, priority, theTypeOfTheAlertsToFilterOn, search, sortBy, offset, limit, startTime, endTime, tags, markedRead, callback)</td>
    <td style="padding:15px">getAlerts</td>
    <td style="padding:15px">{base_path}/{version}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertTags(limit, callback)</td>
    <td style="padding:15px">getAlertTags</td>
    <td style="padding:15px">{base_path}/{version}/alert-tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resolved(id, callback)</td>
    <td style="padding:15px">resolved</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">traceRouteFromConnector(tenantNetworkId, connectorId, body, callback)</td>
    <td style="padding:15px">traceRouteFromConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/connectors/{pathv2}/troubleshooting/traceroute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">traceRouteFromConnectorInstance(tenantNetworkId, connectorId, instanceId, body, callback)</td>
    <td style="padding:15px">traceRouteFromConnectorInstance</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/connectors/{pathv2}/instances/{pathv3}/troubleshooting/traceroute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logsFromConnector(tenantNetworkId, connectorId, body, callback)</td>
    <td style="padding:15px">logsFromConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/connectors/{pathv2}/troubleshooting/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logsFromConnectorInstance(tenantNetworkId, connectorId, instanceId, body, callback)</td>
    <td style="padding:15px">logsFromConnectorInstance</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/connectors/{pathv2}/instances/{pathv3}/troubleshooting/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMessages(requestId, fromOffset, callback)</td>
    <td style="padding:15px">getMessages</td>
    <td style="padding:15px">{base_path}/{version}/messages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSessionCountUsingGET(tenantNetworkId, cxp, segmentName, startTime, endTime, appName, maxItems, callback)</td>
    <td style="padding:15px">getSessionCountUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/stats/application-sessions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppNameTimeSeriesUsingGET(tenantNetworkId, cxp, segmentName, startTime, endTime, appName, maxItems, callback)</td>
    <td style="padding:15px">getAppNameTimeSeriesUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/stats/applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRuleNameTimeSeriesUsingGET(tenantNetworkId, cxp, segmentName, startTime, endTime, ruleName, callback)</td>
    <td style="padding:15px">getRuleNameTimeSeriesUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/stats/policyhits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopApplicationsUsingGET(tenantNetworkId, cxp, segmentName, startTime, endTime, callback)</td>
    <td style="padding:15px">getTopApplicationsUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/stats/topapplications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopApplicationsbyConnectorUsingGET(tenantNetworkId, segmentName, startTime, endTime, connectorId, callback)</td>
    <td style="padding:15px">getTopApplicationsbyConnectorUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/stats/topapplications/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopPolicyHitsUsingGET(tenantNetworkId, cxp, segmentName, startTime, endTime, callback)</td>
    <td style="padding:15px">getTopPolicyHitsUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/stats/toppolicyhits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopPolicyHitsbyConnectorUsingGET(tenantNetworkId, segmentName, startTime, endTime, connectorId, callback)</td>
    <td style="padding:15px">getTopPolicyHitsbyConnectorUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/stats/toppolicyhits/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRemoteAccessConnectors(tenantNetworkId, callback)</td>
    <td style="padding:15px">getRemoteAccessConnectors</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/alkira-remote-access-connectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRemoteAccesssConnector(connectorId, tenantNetworkId, callback)</td>
    <td style="padding:15px">getRemoteAccesssConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/alkira-remote-access-connectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRemoteAccesssConnectorConfiguration(connectorId, tenantNetworkId, cxp, userGroup, callback)</td>
    <td style="padding:15px">getRemoteAccesssConnectorConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/alkira-remote-access-connectors/{pathv2}/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAllRemoteAccessSessions(connectorTemplateId, cxpName, callback)</td>
    <td style="padding:15px">deleteAllRemoteAccessSessions</td>
    <td style="padding:15px">{base_path}/{version}/alkira-remote-access-connector-templates/{pathv1}/sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRemoteAccessSession(connectorTemplateId, sessionId, callback)</td>
    <td style="padding:15px">deleteRemoteAccessSession</td>
    <td style="padding:15px">{base_path}/{version}/alkira-remote-access-connector-templates/{pathv1}/sessions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConnectorTemplates(tenantNetworkId, callback)</td>
    <td style="padding:15px">getConnectorTemplates</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/alkira-remote-access-connector-templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createConnectorTemplate(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createConnectorTemplate</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/alkira-remote-access-connector-templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConnectorTemplate(connectorTemplateId, tenantNetworkId, callback)</td>
    <td style="padding:15px">getConnectorTemplate</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/alkira-remote-access-connector-templates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateConnectorTemplate(connectorTemplateId, tenantNetworkId, body, callback)</td>
    <td style="padding:15px">updateConnectorTemplate</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/alkira-remote-access-connector-templates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConnectorTemplate(connectorTemplateId, tenantNetworkId, callback)</td>
    <td style="padding:15px">deleteConnectorTemplate</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/alkira-remote-access-connector-templates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenant(tenantId, id, callback)</td>
    <td style="padding:15px">getTenant</td>
    <td style="padding:15px">{base_path}/{version}/tenant/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTenant(tenantId, id, body, callback)</td>
    <td style="padding:15px">updateTenant</td>
    <td style="padding:15px">{base_path}/{version}/tenant/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenantPreferences(tenantId, callback)</td>
    <td style="padding:15px">getTenantPreferences</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/preferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTenantPreferences(tenantId, body, callback)</td>
    <td style="padding:15px">updateTenantPreferences</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/preferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntegrationsByTypeAndSubType(id, subType, type, callback)</td>
    <td style="padding:15px">getIntegrationsByTypeAndSubType</td>
    <td style="padding:15px">{base_path}/{version}/tenant/{pathv1}/integration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntegrations(id, type, callback)</td>
    <td style="padding:15px">getIntegrations</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/integrations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIntegration(id, body, callback)</td>
    <td style="padding:15px">createIntegration</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/integrations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIntegrations(id, callback)</td>
    <td style="padding:15px">deleteIntegrations</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/integrations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIntegration(id, integrationId, body, callback)</td>
    <td style="padding:15px">updateIntegration</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/integrations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIntegration(id, integrationId, callback)</td>
    <td style="padding:15px">deleteIntegration</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/integrations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudProviderAccounts(callback)</td>
    <td style="padding:15px">getCloudProviderAccounts</td>
    <td style="padding:15px">{base_path}/{version}/cloud-provider-accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCloudProviderAccount(body, callback)</td>
    <td style="padding:15px">createCloudProviderAccount</td>
    <td style="padding:15px">{base_path}/{version}/cloud-provider-accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteallCloudProviderAccounts(callback)</td>
    <td style="padding:15px">Delete all CloudProviderAccounts</td>
    <td style="padding:15px">{base_path}/{version}/cloud-provider-accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudProviderAccount(cloudProviderAccountId, callback)</td>
    <td style="padding:15px">getCloudProviderAccount</td>
    <td style="padding:15px">{base_path}/{version}/cloud-provider-accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudProviderAccount(cloudProviderAccountId, body, callback)</td>
    <td style="padding:15px">updateCloudProviderAccount</td>
    <td style="padding:15px">{base_path}/{version}/cloud-provider-accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudProviderAccount(cloudProviderAccountId, callback)</td>
    <td style="padding:15px">deleteCloudProviderAccount</td>
    <td style="padding:15px">{base_path}/{version}/cloud-provider-accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudProviderObjects(offset, limit, cloudProvider = 'AWS', cloudProviderObjectNativeId, cloudProviderAccountId, cloudProviderObjectType, search, sortBy, callback)</td>
    <td style="padding:15px">getCloudProviderObjects</td>
    <td style="padding:15px">{base_path}/{version}/cloud-provider-objects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudProviderObject(cloudProviderObjectId, callback)</td>
    <td style="padding:15px">getCloudProviderObject</td>
    <td style="padding:15px">{base_path}/{version}/cloud-provider-objects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudProviderObjectTags(cloudProvider, cloudProviderObjectType, callback)</td>
    <td style="padding:15px">getCloudProviderObjectTags</td>
    <td style="padding:15px">{base_path}/{version}/cloud-provider-objects/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudProviderObjectCounts(callback)</td>
    <td style="padding:15px">getCloudProviderObjectCounts</td>
    <td style="padding:15px">{base_path}/{version}/cloud-provider-object-count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudProviderObjectCountByTimes(callback)</td>
    <td style="padding:15px">getCloudProviderObjectCountByTimes</td>
    <td style="padding:15px">{base_path}/{version}/cloud-provider-object-count-by-time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudProviderObjectSyncRequests(offset, limit, paginated, callback)</td>
    <td style="padding:15px">getCloudProviderObjectSyncRequests</td>
    <td style="padding:15px">{base_path}/{version}/cloud-provider-object-sync-requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCloudProviderObjectSyncRequest(body, callback)</td>
    <td style="padding:15px">createCloudProviderObjectSyncRequest</td>
    <td style="padding:15px">{base_path}/{version}/cloud-provider-object-sync-requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteallCloudProviderObjectSyncRequests(callback)</td>
    <td style="padding:15px">Delete all CloudProviderObjectSyncRequests</td>
    <td style="padding:15px">{base_path}/{version}/cloud-provider-object-sync-requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudProviderObjectSyncRequest(cloudProviderObjectSyncRequestId, callback)</td>
    <td style="padding:15px">getCloudProviderObjectSyncRequest</td>
    <td style="padding:15px">{base_path}/{version}/cloud-provider-objects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInsights(offset, limit, cloudProvider = 'AWS', type = 'AWS_EIP_UNASSOCIATED', types, category = 'SECURITY', severity = 'HIGH', cloudProviderAccountId, expanded, callback)</td>
    <td style="padding:15px">getCloudInsights</td>
    <td style="padding:15px">{base_path}/{version}/cloud-insights?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteallCloudInsights(callback)</td>
    <td style="padding:15px">Delete all CloudInsights</td>
    <td style="padding:15px">{base_path}/{version}/cloud-insights?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInsight(cloudInsightId, callback)</td>
    <td style="padding:15px">getCloudInsight</td>
    <td style="padding:15px">{base_path}/{version}/cloud-insights/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInsight(cloudInsightId, body, callback)</td>
    <td style="padding:15px">updateCloudInsight</td>
    <td style="padding:15px">{base_path}/{version}/cloud-insights/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInsightCategorySummaries(cloudProvider = 'AWS', cloudProviderAccountId, callback)</td>
    <td style="padding:15px">getCloudInsightCategorySummaries</td>
    <td style="padding:15px">{base_path}/{version}/cloud-insights-summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInsightRefreshRequests(offset, limit, state = 'PENDING', callback)</td>
    <td style="padding:15px">getCloudInsightRefreshRequests</td>
    <td style="padding:15px">{base_path}/{version}/cloud-insight-refresh-requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCloudInsightRefreshRequest(body, callback)</td>
    <td style="padding:15px">createCloudInsightRefreshRequest</td>
    <td style="padding:15px">{base_path}/{version}/cloud-insight-refresh-requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteallCloudInsightRefreshRequests(callback)</td>
    <td style="padding:15px">Delete all CloudInsightRefreshRequests</td>
    <td style="padding:15px">{base_path}/{version}/cloud-insight-refresh-requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInsightRefreshRequest(cloudInsightRefreshRequestId, callback)</td>
    <td style="padding:15px">getCloudInsightRefreshRequest</td>
    <td style="padding:15px">{base_path}/{version}/cloud-insight-refresh-requests/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInsightDefinitions(offset, limit, cloudProvider = 'AWS', type = 'AWS_EIP_UNASSOCIATED', category = 'SECURITY', severity = 'HIGH', callback)</td>
    <td style="padding:15px">getCloudInsightDefinitions</td>
    <td style="padding:15px">{base_path}/{version}/cloud-insight-definitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInsightDefinition(cloudInsightDefinitionId, callback)</td>
    <td style="padding:15px">getCloudInsightDefinition</td>
    <td style="padding:15px">{base_path}/{version}/cloud-insight-definitions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudInsightDefinition(cloudInsightDefinitionId, body, callback)</td>
    <td style="padding:15px">updateCloudInsightDefinition</td>
    <td style="padding:15px">{base_path}/{version}/cloud-insight-definitions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInsightDefinitionSummaries(offset, limit, cloudProvider = 'AWS', type = 'AWS_EIP_UNASSOCIATED', category = 'SECURITY', severity = 'HIGH', callback)</td>
    <td style="padding:15px">getCloudInsightDefinitionSummaries</td>
    <td style="padding:15px">{base_path}/{version}/cloud-insight-definitions-summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInsightReportConfigurations(offset, limit, search, callback)</td>
    <td style="padding:15px">getCloudInsightReportConfigurations</td>
    <td style="padding:15px">{base_path}/{version}/cloud-insight-report-configurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCloudInsightReportConfiguration(body, callback)</td>
    <td style="padding:15px">createCloudInsightReportConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/cloud-insight-report-configurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteallCloudInsightReportConfigurations(callback)</td>
    <td style="padding:15px">Delete all CloudInsightReportConfigurations</td>
    <td style="padding:15px">{base_path}/{version}/cloud-insight-report-configurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInsightReportConfiguration(cloudInsightReportConfigurationId, callback)</td>
    <td style="padding:15px">getCloudInsightReportConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/cloud-insight-report-configurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInsightReports(offset, limit, configurationId, callback)</td>
    <td style="padding:15px">getCloudInsightReports</td>
    <td style="padding:15px">{base_path}/{version}/cloud-insight-reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteallCloudInsightReports(configurationId, callback)</td>
    <td style="padding:15px">Delete all CloudInsightReports</td>
    <td style="padding:15px">{base_path}/{version}/cloud-insight-reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInsightReport(cloudInsightReportId, callback)</td>
    <td style="padding:15px">getCloudInsightReport</td>
    <td style="padding:15px">{base_path}/{version}/cloud-insight-reports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInsightReport(cloudInsightReportId, callback)</td>
    <td style="padding:15px">deleteCloudInsightReport</td>
    <td style="padding:15px">{base_path}/{version}/cloud-insight-reports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getbillingtags(callback)</td>
    <td style="padding:15px">getbillingtags</td>
    <td style="padding:15px">{base_path}/{version}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBillingTag(body, callback)</td>
    <td style="padding:15px">createBillingTag</td>
    <td style="padding:15px">{base_path}/{version}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBillingTag(billingTagId, callback)</td>
    <td style="padding:15px">getBillingTag</td>
    <td style="padding:15px">{base_path}/{version}/tags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBillingTag(billingTagId, body, callback)</td>
    <td style="padding:15px">updateBillingTag</td>
    <td style="padding:15px">{base_path}/{version}/tags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBillingTag(billingTagId, callback)</td>
    <td style="padding:15px">deleteBillingTag</td>
    <td style="padding:15px">{base_path}/{version}/tags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaEdgeconnectconnectors(tenantNetworkId, callback)</td>
    <td style="padding:15px">getArubaEdgeconnectconnectors</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/aruba-edge-connectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createArubaEdgeConnectConnector(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createArubaEdgeConnectConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/aruba-edge-connectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaEdgeConnectConnector(tenantNetworkId, arubaEdgeconnectConnectorId, callback)</td>
    <td style="padding:15px">getArubaEdgeConnectConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/aruba-edge-connectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateArubaEdgeConnectConnector(tenantNetworkId, arubaEdgeconnectConnectorId, body, callback)</td>
    <td style="padding:15px">updateArubaEdgeConnectConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/aruba-edge-connectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaEdgeConnectConnector(tenantNetworkId, arubaEdgeconnectConnectorId, callback)</td>
    <td style="padding:15px">deleteArubaEdgeConnectConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/aruba-edge-connectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArubaEdgeConnectConnectorInstanceConfiguration(tenantNetworkId, arubaEdgeconnectconnectorId, instanceId, callback)</td>
    <td style="padding:15px">getArubaEdgeConnectConnectorInstanceConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/aruba-edge-connectors/{pathv2}/instances/{pathv3}/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAkamaiProlexicconnectors(tenantNetworkId, callback)</td>
    <td style="padding:15px">getAkamaiProlexicconnectors</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/akamai-prolexic-connectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAkamaiProlexicConnector(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createAkamaiProlexicConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/akamai-prolexic-connectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAkamaiProlexicConnector(tenantNetworkId, id, callback)</td>
    <td style="padding:15px">getAkamaiProlexicConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/akamai-prolexic-connectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAkamaiProlexicConnector(tenantNetworkId, id, body, callback)</td>
    <td style="padding:15px">updateAkamaiProlexicConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/akamai-prolexic-connectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAkamaiProlexicConnector(tenantNetworkId, id, callback)</td>
    <td style="padding:15px">deleteAkamaiProlexicConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/akamai-prolexic-connectors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudNativeServices(cloudProvider, name, callback)</td>
    <td style="padding:15px">getCloudNativeServices</td>
    <td style="padding:15px">{base_path}/{version}/inventory/cloudnativeservices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCountUsingGET(tenantNetworkId, cxp, segmentName, startTime, endTime, active, sessionId, templateId, userName, userGroupId, callback)</td>
    <td style="padding:15px">getCountUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/stats/remote-access-sessions/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRemoteAccessTopApplications(tenantNetworkId, cxp, segmentName, startTime, endTime, maxItems, sessionId, templateId, userName, userGroupId, userGroup, callback)</td>
    <td style="padding:15px">getRemoteAccessTopApplications</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/stats/remote-access-sessions/topapplications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRemoteAccessDataUtilizationUsingGET(tenantNetworkId, templateId, cxp, segmentName, startTime, endTime, active, sessionId, userName, userGroupId, maxItems, callback)</td>
    <td style="padding:15px">getRemoteAccessDataUtilizationUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/stats/remote-access-sessions/data-utilization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRemoteAccessConnectorDataUtilizationUsingGET(tenantNetworkId, startTime, endTime, connectorId, userGroupId, callback)</td>
    <td style="padding:15px">getRemoteAccessConnectorDataUtilizationUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/stats/remote-access-sessions/utilization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRemoteAccessTopPolicyHits(tenantNetworkId, cxp, segmentName, startTime, endTime, maxItems, sessionId, templateId, userName, userGroupId, userGroup, callback)</td>
    <td style="padding:15px">getRemoteAccessTopPolicyHits</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/stats/remote-access-sessions/toppolicyhits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRemoteAccessBandwidthUtilizationUsingGET(tenantNetworkId, templateId, cxp, segmentName, startTime, endTime, active, sessionId, userName, userGroupId, maxItems, callback)</td>
    <td style="padding:15px">getRemoteAccessBandwidthUtilizationUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/stats/remote-access-sessions/bandwidth-utilization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthenticationAttemptsUsingGET(tenantNetworkId, startTime, endTime, limit, offset, status, templateId, callback)</td>
    <td style="padding:15px">getAuthenticationAttemptsUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/alkira-remote-access-connector-templates/{pathv2}/authentication-attempts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRemoteAccessSessionsUsingGET(tenantNetworkId, cxp, segmentName, startTime, endTime, active, templateId, limit, offset, callback)</td>
    <td style="padding:15px">getRemoteAccessSessionsUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/alkira-remote-access-connector-templates/{pathv2}/sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNATPolicies(tenantNetworkId, callback)</td>
    <td style="padding:15px">getNATPolicies</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/nat-policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNATPolicy(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createNATPolicy</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/nat-policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNATPolicy(tenantNetworkId, nATPolicyId, callback)</td>
    <td style="padding:15px">getNATPolicy</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/nat-policies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNATPolicy(tenantNetworkId, nATPolicyId, body, callback)</td>
    <td style="padding:15px">updateNATPolicy</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/nat-policies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNATPolicy(tenantNetworkId, nATPolicyId, callback)</td>
    <td style="padding:15px">deleteNATPolicy</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/nat-policies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNATRules(tenantNetworkId, callback)</td>
    <td style="padding:15px">getNATRules</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/nat-rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNATRule(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createNATRule</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/nat-rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNATRule(tenantNetworkId, nATRuleId, callback)</td>
    <td style="padding:15px">getNATRule</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/nat-rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNATRule(tenantNetworkId, nATRuleId, body, callback)</td>
    <td style="padding:15px">updateNATRule</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/nat-rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNATRule(tenantNetworkId, nATRuleId, callback)</td>
    <td style="padding:15px">deleteNATRule</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/nat-rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGlobalCidrLists(tenantNetworkId, callback)</td>
    <td style="padding:15px">getGlobalCidrLists</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/global-cidr-lists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGlobalCidrList(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createGlobalCidrList</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/global-cidr-lists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGlobalCidrList(tenantNetworkId, globalCidrListId, callback)</td>
    <td style="padding:15px">getGlobalCidrList</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/global-cidr-lists/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGlobalCidrList(tenantNetworkId, globalCidrListId, body, callback)</td>
    <td style="padding:15px">updateGlobalCidrList</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/global-cidr-lists/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGlobalCidrList(tenantNetworkId, globalCidrListId, callback)</td>
    <td style="padding:15px">deleteGlobalCidrList</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/global-cidr-lists/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getsegmentResources(tenantNetworkId, callback)</td>
    <td style="padding:15px">getsegmentResources</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/segment-resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSegmentResource(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createSegmentResource</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/segment-resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSegmentResource(tenantNetworkId, segmentResourceId, callback)</td>
    <td style="padding:15px">getSegmentResource</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/segment-resources/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSegmentResource(tenantNetworkId, segmentResourceId, body, callback)</td>
    <td style="padding:15px">updateSegmentResource</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/segment-resources/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSegmentResource(tenantNetworkId, segmentResourceId, callback)</td>
    <td style="padding:15px">deleteSegmentResource</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/segment-resources/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSegmentResourceShares(tenantNetworkId, callback)</td>
    <td style="padding:15px">getSegmentResourceShares</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/segment-resource-shares?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSegmentResourceShare(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createSegmentResourceShare</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/segment-resource-shares?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSegmentResourceShare(tenantNetworkId, segmentResourceShareId, callback)</td>
    <td style="padding:15px">getSegmentResourceShare</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/segment-resource-shares/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSegmentResourceShare(tenantNetworkId, segmentResourceShareId, body, callback)</td>
    <td style="padding:15px">updateSegmentResourceShare</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/segment-resource-shares/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSegmentResourceShare(tenantNetworkId, segmentResourceShareId, callback)</td>
    <td style="padding:15px">deleteSegmentResourceShare</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/segment-resource-shares/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCheckPointFWServices(tenantNetworkId, callback)</td>
    <td style="padding:15px">getCheckPointFWServices</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/chkp-fw-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCheckPointFWService(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createCheckPointFWService</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/chkp-fw-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCheckPointFWService(tenantNetworkId, serviceId, callback)</td>
    <td style="padding:15px">getCheckPointFWService</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/chkp-fw-services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCheckPointFWService(tenantNetworkId, serviceId, body, callback)</td>
    <td style="padding:15px">updateCheckPointFWService</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/chkp-fw-services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCheckPointFWService(tenantNetworkId, serviceId, callback)</td>
    <td style="padding:15px">deleteCheckPointFWService</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/chkp-fw-services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCheckPointFWServiceInstanceConfiguration(tenantNetworkId, serviceId, instanceId, callback)</td>
    <td style="padding:15px">getCheckPointFWServiceInstanceConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/chkp-fw-services/{pathv2}/instances/{pathv3}/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFortinetFWServices(tenantNetworkId, callback)</td>
    <td style="padding:15px">getFortinetFWServices</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/ftnt-fw-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFortinetFWService(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createFortinetFWService</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/ftnt-fw-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFortinetFWService(tenantNetworkId, serviceId, callback)</td>
    <td style="padding:15px">getFortinetFWService</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/ftnt-fw-services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFortinetFWService(tenantNetworkId, serviceId, body, callback)</td>
    <td style="padding:15px">updateFortinetFWService</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/ftnt-fw-services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFortinetFWService(tenantNetworkId, serviceId, callback)</td>
    <td style="padding:15px">deleteFortinetFWService</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/ftnt-fw-services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFortinetFWServiceInstanceConfiguration(tenantNetworkId, serviceId, instanceId, callback)</td>
    <td style="padding:15px">getFortinetFWServiceInstanceConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/ftnt-fw-services/{pathv2}/instances/{pathv3}/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCiscoFTDvFWServices(tenantNetworkId, callback)</td>
    <td style="padding:15px">getCiscoFTDvFWServices</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/cisco-ftdv-fw-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCiscoFTDvFWService(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createCiscoFTDvFWService</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/cisco-ftdv-fw-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCiscoFTDvFWService(tenantNetworkId, serviceId, callback)</td>
    <td style="padding:15px">getCiscoFTDvFWService</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/cisco-ftdv-fw-services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCiscoFTDvFWService(tenantNetworkId, serviceId, body, callback)</td>
    <td style="padding:15px">updateCiscoFTDvFWService</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/cisco-ftdv-fw-services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCiscoFTDvFWService(tenantNetworkId, serviceId, callback)</td>
    <td style="padding:15px">deleteCiscoFTDvFWService</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/cisco-ftdv-fw-services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCiscoFTDvFWServiceInstanceConfiguration(tenantNetworkId, serviceId, instanceId, callback)</td>
    <td style="padding:15px">getCiscoFTDvFWServiceInstanceConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/cisco-ftdv-fw-services/{pathv2}/instances/{pathv3}/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZscalerInternetAccessServices(tenantNetworkId, callback)</td>
    <td style="padding:15px">getZscalerInternetAccessServices</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/zscaler-internet-access-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createZscalerInternetAccessService(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createZscalerInternetAccessService</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/zscaler-internet-access-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZscalerInternetAccessService(tenantNetworkId, serviceId, callback)</td>
    <td style="padding:15px">getZscalerInternetAccessService</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/zscaler-internet-access-services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateZscalerInternetAccessService(tenantNetworkId, serviceId, body, callback)</td>
    <td style="padding:15px">updateZscalerInternetAccessService</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/zscaler-internet-access-services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteZscalerInternetAccessService(tenantNetworkId, serviceId, callback)</td>
    <td style="padding:15px">deleteZscalerInternetAccessService</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/zscaler-internet-access-services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfoBloxServices(tenantNetworkId, callback)</td>
    <td style="padding:15px">getInfoBloxServices</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/infoblox-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createInfoBloxService(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createInfoBloxService</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/infoblox-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfoBloxService(tenantNetworkId, serviceId, callback)</td>
    <td style="padding:15px">getInfoBloxService</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/infoblox-services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateInfoBloxService(tenantNetworkId, serviceId, body, callback)</td>
    <td style="padding:15px">updateInfoBloxService</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/infoblox-services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInfoBloxService(tenantNetworkId, serviceId, callback)</td>
    <td style="padding:15px">deleteInfoBloxService</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/infoblox-services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFeatureFlag(feature, callback)</td>
    <td style="padding:15px">getFeatureFlag</td>
    <td style="padding:15px">{base_path}/{version}/tenantfeatureflags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenantEnabledFeatures(callback)</td>
    <td style="padding:15px">getTenantEnabledFeatures</td>
    <td style="padding:15px">{base_path}/{version}/tenantenabledfeatures?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">capturePacketsFromConnector(tenantNetworkId, connectorId, body, callback)</td>
    <td style="padding:15px">capturePacketsFromConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/connectors/{pathv2}/troubleshooting/capturepackets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPacketCaptureResponses(callback)</td>
    <td style="padding:15px">getPacketCaptureResponses</td>
    <td style="padding:15px">{base_path}/{version}/troubleshooting/capturepackets/responses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadPacketCapture(pathParam, callback)</td>
    <td style="padding:15px">downloadPacketCapture</td>
    <td style="padding:15px">{base_path}/{version}/troubleshooting/capturepackets/download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllBYOIP(tenantNetworkId, callback)</td>
    <td style="padding:15px">getAllBYOIP</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/byoips?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBYOIP(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createBYOIP</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/byoips?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAllBYOIP(tenantNetworkId, callback)</td>
    <td style="padding:15px">deleteAllBYOIP</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/byoips?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBYOIP(tenantNetworkId, byoipId, callback)</td>
    <td style="padding:15px">getBYOIP</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/byoips/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBYOIP(tenantNetworkId, byoipId, body, callback)</td>
    <td style="padding:15px">updateBYOIP</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/byoips/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBYOIP(tenantNetworkId, byoipId, callback)</td>
    <td style="padding:15px">deleteBYOIP</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/byoips/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBYOIPReferences(tenantNetworkId, byoipId, callback)</td>
    <td style="padding:15px">getBYOIPReferences</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/byoips/{pathv2}/references?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getallcredentials(tenantNetworkId, callback)</td>
    <td style="padding:15px">getallcredentials</td>
    <td style="padding:15px">{base_path}/{version}/credentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAWSVPCCredentialsUsingPOST(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">addAWSVPCCredentialsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/credentials/awsvpc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAWSVPCCredentialsUsingPUT(tenantNetworkId, credentialId, body, callback)</td>
    <td style="padding:15px">updateAWSVPCCredentialsUsingPUT</td>
    <td style="padding:15px">{base_path}/{version}/credentials/awsvpc/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAwsVpcCredentialsUsingDELETE(tenantNetworkId, credentialId, force, callback)</td>
    <td style="padding:15px">deleteAwsVpcCredentialsUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/credentials/awsvpc/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAzureCredentialsUsingPOST(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">addAzureCredentialsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/credentials/azurevnet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAzureCredentialsUsingPUT(tenantNetworkId, credentialId, body, callback)</td>
    <td style="padding:15px">updateAzureCredentialsUsingPUT</td>
    <td style="padding:15px">{base_path}/{version}/credentials/azurevnet/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAzureCredentialsUsingDELETE(tenantNetworkId, credentialId, force, callback)</td>
    <td style="padding:15px">deleteAzureCredentialsUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/credentials/azurevnet/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addOCIVCNCredentialsUsingPOST(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">addOCIVCNCredentialsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/credentials/ocivcn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOCIVCNCredentialsUsingPUT(tenantNetworkId, credentialId, body, callback)</td>
    <td style="padding:15px">updateOCIVCNCredentialsUsingPUT</td>
    <td style="padding:15px">{base_path}/{version}/credentials/ocivcn/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePOCIVCNCredentialsUsingDELETE(tenantNetworkId, credentialId, force, callback)</td>
    <td style="padding:15px">deletePOCIVCNCredentialsUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/credentials/ocivcn/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCiscoSDWANInstanceCredentialsUsingPOST(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">addCiscoSDWANInstanceCredentialsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/credentials/ciscosdwan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCiscoSDWANInstanceCredentialsUsingPUT(tenantNetworkId, credentialId, body, callback)</td>
    <td style="padding:15px">updateCiscoSDWANInstanceCredentialsUsingPUT</td>
    <td style="padding:15px">{base_path}/{version}/credentials/ciscosdwan/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCiscosdwanCredentialsUsingDELETE(tenantNetworkId, credentialId, force, callback)</td>
    <td style="padding:15px">deleteCiscosdwanCredentialsUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/credentials/ciscosdwan/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addGCPCredentialsUsingPOST(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">addGCPCredentialsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/credentials/gcpvpc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGCPCredentialsUsingPUT(tenantNetworkId, credentialId, body, callback)</td>
    <td style="padding:15px">updateGCPCredentialsUsingPUT</td>
    <td style="padding:15px">{base_path}/{version}/credentials/gcpvpc/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGcpvpcCredentialsUsingDELETE(tenantNetworkId, credentialId, force, callback)</td>
    <td style="padding:15px">deleteGcpvpcCredentialsUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/credentials/gcpvpc/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addLDAPCredentialsUsingPOST(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">addLDAPCredentialsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/credentials/ldap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateLDAPCredentialsUsingPUT(tenantNetworkId, credentialId, body, callback)</td>
    <td style="padding:15px">updateLDAPCredentialsUsingPUT</td>
    <td style="padding:15px">{base_path}/{version}/credentials/ldap/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLDAPCredentialsUsingDELETE(tenantNetworkId, credentialId, force, callback)</td>
    <td style="padding:15px">deleteLDAPCredentialsUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/credentials/ldap/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPANCredentialsUsingPOST(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">addPANCredentialsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/credentials/pan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePANCredentialsUsingPUT(tenantNetworkId, credentialId, body, callback)</td>
    <td style="padding:15px">updatePANCredentialsUsingPUT</td>
    <td style="padding:15px">{base_path}/{version}/credentials/pan/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePanCredentialsUsingDELETE(tenantNetworkId, credentialId, force, callback)</td>
    <td style="padding:15px">deletePanCredentialsUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/credentials/pan/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPANInstanceCredentialsUsingPOST(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">addPANInstanceCredentialsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/credentials/paninstance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePANInstanceCredentialsUsingPUT(tenantNetworkId, credentialId, body, callback)</td>
    <td style="padding:15px">updatePANInstanceCredentialsUsingPUT</td>
    <td style="padding:15px">{base_path}/{version}/credentials/paninstance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePanInstanceCredentialsUsingDELETE(tenantNetworkId, credentialId, force, callback)</td>
    <td style="padding:15px">deletePanInstanceCredentialsUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/credentials/paninstance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPANMasterKeyCredentialsUsingPOST(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">addPANMasterKeyCredentialsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/credentials/pan-masterkey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePANMasterKeyCredentialsUsingPUT(tenantNetworkId, credentialId, body, callback)</td>
    <td style="padding:15px">updatePANMasterKeyCredentialsUsingPUT</td>
    <td style="padding:15px">{base_path}/{version}/credentials/pan-masterkey/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePanMasterKeyCredentialsUsingDELETE(tenantNetworkId, credentialId, force, callback)</td>
    <td style="padding:15px">deletePanMasterKeyCredentialsUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/credentials/pan-masterkey/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPANRegistrationCredentialsUsingPOST(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">addPANRegistrationCredentialsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/credentials/pan-registration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePANRegistrationCredentialsUsingPUT(tenantNetworkId, credentialId, body, callback)</td>
    <td style="padding:15px">updatePANRegistrationCredentialsUsingPUT</td>
    <td style="padding:15px">{base_path}/{version}/credentials/pan-registration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePanRegistrationCredentialsUsingDELETE(tenantNetworkId, credentialId, force, callback)</td>
    <td style="padding:15px">deletePanRegistrationCredentialsUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/credentials/pan-registration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addKeyPairCredentialsUsingPOST(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">addKeyPairCredentialsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/credentials/keypair?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteKeyPairCredentialsUsingDELETE(tenantNetworkId, credentialId, force, callback)</td>
    <td style="padding:15px">deleteKeyPairCredentialsUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/credentials/keypair/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addArubaEdgeConnectCredentialsUsingPOST(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">addArubaEdgeConnectCredentialsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/credentials/aruba-edge-connector-instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteArubaEdgeConnectCredentialsUsingDELETE(tenantNetworkId, credentialId, force, callback)</td>
    <td style="padding:15px">deleteArubaEdgeConnectCredentialsUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/credentials/aruba-edge-connector-instances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAkamaiProlexicCredentialsUsingPOST(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">addAkamaiProlexicCredentialsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/credentials/akamai-prolexic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAkamaiProlexicCredentialsUsingDELETE(tenantNetworkId, credentialId, force, callback)</td>
    <td style="padding:15px">deleteAkamaiProlexicCredentialsUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/credentials/akamai-prolexic/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCheckPointFWServiceCredentialsUsingPOST(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">addCheckPointFWServiceCredentialsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/credentials/chkp-fw?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCheckPointFWServiceCredentialsUsingPUT(tenantNetworkId, credentialId, body, callback)</td>
    <td style="padding:15px">updateCheckPointFWServiceCredentialsUsingPUT</td>
    <td style="padding:15px">{base_path}/{version}/credentials/chkp-fw/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePCheckPointFWServiceCredentialsUsingDELETE(tenantNetworkId, credentialId, force, callback)</td>
    <td style="padding:15px">deletePCheckPointFWServiceCredentialsUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/credentials/chkp-fw/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCheckPointFWServiceInstanceCredentialsUsingPOST(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">addCheckPointFWServiceInstanceCredentialsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/credentials/chkp-fw-instance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCheckPointFWServiceInstanceCredentialsUsingPUT(tenantNetworkId, credentialId, body, callback)</td>
    <td style="padding:15px">updateCheckPointFWServiceInstanceCredentialsUsingPUT</td>
    <td style="padding:15px">{base_path}/{version}/credentials/chkp-fw-instance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePCheckPointFWServiceInstanceCredentialsUsingDELETE(tenantNetworkId, credentialId, force, callback)</td>
    <td style="padding:15px">deletePCheckPointFWServiceInstanceCredentialsUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/credentials/chkp-fw-instance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCheckPointFWManagementServerCredentialsUsingPOST(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">addCheckPointFWManagementServerCredentialsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/credentials/chkp-fw-management-server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCheckPointFWManagementServerCredentialsUsingPUT(tenantNetworkId, credentialId, body, callback)</td>
    <td style="padding:15px">updateCheckPointFWManagementServerCredentialsUsingPUT</td>
    <td style="padding:15px">{base_path}/{version}/credentials/chkp-fw-management-server/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePCheckPointFWManagementServerCredentialsUsingDELETE(tenantNetworkId, credentialId, force, callback)</td>
    <td style="padding:15px">deletePCheckPointFWManagementServerCredentialsUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/credentials/chkp-fw-management-server/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addUsernamePasswordCredentialsUsingPOST(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">addUsernamePasswordCredentialsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/credentials/username-password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUsernamePasswordCredentialsUsingPUT(tenantNetworkId, credentialId, body, callback)</td>
    <td style="padding:15px">updateUsernamePasswordCredentialsUsingPUT</td>
    <td style="padding:15px">{base_path}/{version}/credentials/username-password/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUsernamePasswordCredentialsUsingDELETE(tenantNetworkId, credentialId, force, callback)</td>
    <td style="padding:15px">deleteUsernamePasswordCredentialsUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/credentials/username-password/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSplunkHECTokenCredentialsUsingPOST(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">addSplunkHECTokenCredentialsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/credentials/splunk-hec-token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSplunkHECTokenCredentialsUsingPUT(tenantNetworkId, credentialId, body, callback)</td>
    <td style="padding:15px">updateSplunkHECTokenCredentialsUsingPUT</td>
    <td style="padding:15px">{base_path}/{version}/credentials/splunk-hec-token/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSplunkHECTokenCredentialsUsingDELETE(tenantNetworkId, credentialId, force, callback)</td>
    <td style="padding:15px">deleteSplunkHECTokenCredentialsUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/credentials/splunk-hec-token/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addFtntFWServiceCredentialsUsingPOST(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">addFtntFWServiceCredentialsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/credentials/ftntfw?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFtntFWServiceCredentialsUsingPUT(tenantNetworkId, credentialId, body, callback)</td>
    <td style="padding:15px">updateFtntFWServiceCredentialsUsingPUT</td>
    <td style="padding:15px">{base_path}/{version}/credentials/ftntfw/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFtntFWServiceCredentialsUsingDELETE(tenantNetworkId, credentialId, force, callback)</td>
    <td style="padding:15px">deleteFtntFWServiceCredentialsUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/credentials/ftntfw/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addFtntFWServiceInstanceCredentialsUsingPOST(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">addFtntFWServiceInstanceCredentialsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/credentials/ftntfw-instance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFtntFWServiceInstanceCredentialsUsingPUT(tenantNetworkId, credentialId, body, callback)</td>
    <td style="padding:15px">updateFtntFWServiceInstanceCredentialsUsingPUT</td>
    <td style="padding:15px">{base_path}/{version}/credentials/ftntfw-instance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFtntFWServiceInstanceCredentialsUsingDELETE(tenantNetworkId, credentialId, force, callback)</td>
    <td style="padding:15px">deleteFtntFWServiceInstanceCredentialsUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/credentials/ftntfw-instance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCiscoFTDvFWCredentialsUsingPOST(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">addCiscoFTDvFWCredentialsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/credentials/cisco-ftdv-fw?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCiscoFTDvFWCredentialsUsingPUT(tenantNetworkId, credentialId, body, callback)</td>
    <td style="padding:15px">updateCiscoFTDvFWCredentialsUsingPUT</td>
    <td style="padding:15px">{base_path}/{version}/credentials/cisco-ftdv-fw/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCiscoFTDvFWCredentialsUsingDELETE(tenantNetworkId, credentialId, force, callback)</td>
    <td style="padding:15px">deleteCiscoFTDvFWCredentialsUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/credentials/cisco-ftdv-fw/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCiscoFTDvFWInstanceCredentialsUsingPOST(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">addCiscoFTDvFWInstanceCredentialsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/credentials/cisco-ftdv-fw-instance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCiscoFTDvFWInstanceCredentialsUsingPUT(tenantNetworkId, credentialId, body, callback)</td>
    <td style="padding:15px">updateCiscoFTDvFWInstanceCredentialsUsingPUT</td>
    <td style="padding:15px">{base_path}/{version}/credentials/cisco-ftdv-fw-instance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCiscoFTDvFWInstanceCredentialsUsingDELETE(tenantNetworkId, credentialId, force, callback)</td>
    <td style="padding:15px">deleteCiscoFTDvFWInstanceCredentialsUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/credentials/cisco-ftdv-fw-instance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addInfoBloxCredentialsUsingPOST(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">addInfoBloxCredentialsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/credentials/infoblox?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addInfoBloxInstanceCredentialsUsingPOST(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">addInfoBloxInstanceCredentialsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/credentials/infoblox-instance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addInfoBloxGridMasterCredentialsUsingPOST(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">addInfoBloxGridMasterCredentialsUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/credentials/infoblox-grid-master?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllUnexpiredOTP(callback)</td>
    <td style="padding:15px">getAllUnexpiredOTP</td>
    <td style="padding:15px">{base_path}/{version}/otps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOneTimePassword(body, callback)</td>
    <td style="padding:15px">createOneTimePassword</td>
    <td style="padding:15px">{base_path}/{version}/otps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteValidOneTimePasswords(callback)</td>
    <td style="padding:15px">deleteValidOneTimePasswords</td>
    <td style="padding:15px">{base_path}/{version}/otps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOneTimePasswordById(otpId, callback)</td>
    <td style="padding:15px">deleteOneTimePasswordById</td>
    <td style="padding:15px">{base_path}/{version}/otps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoginSpecification(callback)</td>
    <td style="padding:15px">getLoginSpecification</td>
    <td style="padding:15px">{base_path}/{version}/login-specification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateLoginSpecification(body, callback)</td>
    <td style="padding:15px">createOrUpdateLoginSpecification</td>
    <td style="padding:15px">{base_path}/{version}/login-specification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLoginSpecification(callback)</td>
    <td style="padding:15px">deleteLoginSpecification</td>
    <td style="padding:15px">{base_path}/{version}/login-specification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPasswordSpecification(callback)</td>
    <td style="padding:15px">getPasswordSpecification</td>
    <td style="padding:15px">{base_path}/{version}/password-specification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdatePasswordSpecification(body, callback)</td>
    <td style="padding:15px">createOrUpdatePasswordSpecification</td>
    <td style="padding:15px">{base_path}/{version}/password-specification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePasswordSpecification(callback)</td>
    <td style="padding:15px">deletePasswordSpecification</td>
    <td style="padding:15px">{base_path}/{version}/password-specification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRoutePolicies(tenantNetworkId, callback)</td>
    <td style="padding:15px">getAllRoutePolicies</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/route-policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRoutePolicies(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createRoutePolicies</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/route-policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAllRoutePolicies(tenantNetworkId, callback)</td>
    <td style="padding:15px">deleteAllRoutePolicies</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/route-policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutePolicy(tenantNetworkId, routePolicyId, callback)</td>
    <td style="padding:15px">getRoutePolicy</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/route-policies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRoutePolicy(tenantNetworkId, routePolicyId, body, callback)</td>
    <td style="padding:15px">updateRoutePolicy</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/route-policies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRoutePolicy(tenantNetworkId, routePolicyId, callback)</td>
    <td style="padding:15px">deleteRoutePolicy</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/route-policies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInvoiceDetails(fromYearMonth, page, size, sort, toYearMonth, callback)</td>
    <td style="padding:15px">getInvoiceDetails</td>
    <td style="padding:15px">{base_path}/{version}/invoices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInvoiceDetail(invoiceId, callback)</td>
    <td style="padding:15px">getInvoiceDetail</td>
    <td style="padding:15px">{base_path}/{version}/invoices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMeters(fromYearMonth, page, size, sort, toYearMonth, callback)</td>
    <td style="padding:15px">getMeters</td>
    <td style="padding:15px">{base_path}/{version}/meters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMeterItems(meterId, page, size, sort, callback)</td>
    <td style="padding:15px">getMeterItems</td>
    <td style="padding:15px">{base_path}/{version}/meters/{pathv1}/items?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutes(tenantNetworkId, type = 'received', cxp, segmentName, segmentId, connectorId, search, lpmPrefix, prefixType, offset, limit, callback)</td>
    <td style="padding:15px">getRoutes</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/routes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicies(tenantNetworkId, fromConnector, toConnector, callback)</td>
    <td style="padding:15px">getPolicies</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicy(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createPolicy</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicy(tenantNetworkId, id, callback)</td>
    <td style="padding:15px">getPolicy</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/policies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicy(tenantNetworkId, id, body, callback)</td>
    <td style="padding:15px">updatePolicy</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/policies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicy(tenantNetworkId, id, callback)</td>
    <td style="padding:15px">deletePolicy</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/policies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRules(tenantNetworkId, callback)</td>
    <td style="padding:15px">getRules</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRule(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createRule</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRule(tenantNetworkId, id, callback)</td>
    <td style="padding:15px">getRule</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRule(tenantNetworkId, id, body, callback)</td>
    <td style="padding:15px">updateRule</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRule(tenantNetworkId, id, callback)</td>
    <td style="padding:15px">deleteRule</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRulelists(tenantNetworkId, callback)</td>
    <td style="padding:15px">getRulelists</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/rulelists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRulelist(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createRulelist</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/rulelists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRulelist(tenantNetworkId, id, callback)</td>
    <td style="padding:15px">getRulelist</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/rulelists/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRulelist(tenantNetworkId, id, body, callback)</td>
    <td style="padding:15px">updateRulelist</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/rulelists/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRulelist(tenantNetworkId, id, callback)</td>
    <td style="padding:15px">deleteRulelist</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/rulelists/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPrefixlists(tenantNetworkId, callback)</td>
    <td style="padding:15px">getPrefixlists</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/prefixlists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPrefixlist(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createPrefixlist</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/prefixlists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPrefixlist(tenantNetworkId, id, callback)</td>
    <td style="padding:15px">getPrefixlist</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/prefixlists/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePrefixlist(tenantNetworkId, id, body, callback)</td>
    <td style="padding:15px">updatePrefixlist</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/prefixlists/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePrefixlist(tenantNetworkId, id, callback)</td>
    <td style="padding:15px">deletePrefixlist</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/prefixlists/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveshealthforallconnectorsandservicesinthistenant(tenantNetworkId, callback)</td>
    <td style="padding:15px">Retrieves health for all connectors and services in this tenant</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/health?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveshealthforaspecificconnectorinthistenant(tenantNetworkId, connectorId, callback)</td>
    <td style="padding:15px">Retrieves health for a specific connector in this tenant</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/health/connector/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveshealthforaspecificinstanceofaconnectorinthistenant(tenantNetworkId, connectorId, instanceId, callback)</td>
    <td style="padding:15px">Retrieves health for a specific instance of a connector in this tenant</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/health/connector/{pathv2}/instance/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveshealthforallinternetapplicationsinthistenant(tenantNetworkId, callback)</td>
    <td style="padding:15px">Retrieves health for all internet applications in this tenant</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/health/internetapplications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveshealthforaspecificserviceinthistenant(tenantNetworkId, serviceId, callback)</td>
    <td style="padding:15px">Retrieves health for a specific service in this tenant</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/health/service/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveshealthforaspecificinstanceofaserviceinthistenant(tenantNetworkId, serviceId, instanceId, callback)</td>
    <td style="padding:15px">Retrieves health for a specific instance of a service in this tenant</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/health/service/{pathv2}/instance/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveshealthforaspecificfirewalzoneinthistenant(tenantNetworkId, zoneId, callback)</td>
    <td style="padding:15px">Retrieves health for a specific firewal zone in this tenant</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/health/firewall-zone/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRetrieveshealthforallconnectorsandservicesinthistenant(tenantNetworkId, callback)</td>
    <td style="padding:15px">Retrieves health for all connectors and services in this tenant</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/health/v3?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">captureFlowsFromConnector(tenantNetworkId, connectorId, body, callback)</td>
    <td style="padding:15px">captureFlowsFromConnector</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/connectors/{pathv2}/troubleshooting/captureflows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">captureFlowsFromResourceShare(tenantNetworkId, segmentResourceShareId, body, callback)</td>
    <td style="padding:15px">captureFlowsFromResourceShare</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/segment-resource-shares/{pathv2}/troubleshooting/captureflows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveNatTranslation(tenantNetworkId, connectorId, body, callback)</td>
    <td style="padding:15px">getActiveNatTranslation</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/connectors/{pathv2}/troubleshooting/active-nat-translations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveNatTranslationForResourceShare(tenantNetworkId, segmentResourceShareId, body, callback)</td>
    <td style="padding:15px">getActiveNatTranslationForResourceShare</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/segment-resource-shares/{pathv2}/troubleshooting/active-nat-translations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAsPathLists(tenantNetworkId, callback)</td>
    <td style="padding:15px">getAllAsPathLists</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/as-path-lists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAsPathLists(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createAsPathLists</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/as-path-lists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAllAsPathLists(tenantNetworkId, callback)</td>
    <td style="padding:15px">deleteAllAsPathLists</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/as-path-lists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAsPathList(tenantNetworkId, asPathListId, callback)</td>
    <td style="padding:15px">getAsPathList</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/as-path-lists/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAsPathList(tenantNetworkId, asPathListId, body, callback)</td>
    <td style="padding:15px">updateAsPathList</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/as-path-lists/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAsPathList(tenantNetworkId, asPathListId, callback)</td>
    <td style="padding:15px">deleteAsPathList</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/as-path-lists/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllCommunityLists(tenantNetworkId, callback)</td>
    <td style="padding:15px">getAllCommunityLists</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/community-lists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCommunityLists(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createCommunityLists</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/community-lists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAllCommunityLists(tenantNetworkId, callback)</td>
    <td style="padding:15px">deleteAllCommunityLists</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/community-lists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommunityList(tenantNetworkId, communityListId, callback)</td>
    <td style="padding:15px">getCommunityList</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/community-lists/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCommunityList(tenantNetworkId, communityListId, body, callback)</td>
    <td style="padding:15px">updateCommunityList</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/community-lists/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCommunityList(tenantNetworkId, communityListId, callback)</td>
    <td style="padding:15px">deleteCommunityList</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/community-lists/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllExtendedCommunityLists(tenantNetworkId, callback)</td>
    <td style="padding:15px">getAllExtendedCommunityLists</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/extended-community-lists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createExtendedCommunityLists(tenantNetworkId, body, callback)</td>
    <td style="padding:15px">createExtendedCommunityLists</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/extended-community-lists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAllExtendedCommunityLists(tenantNetworkId, callback)</td>
    <td style="padding:15px">deleteAllExtendedCommunityLists</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/extended-community-lists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtendedCommunityList(tenantNetworkId, extendedCommunityListId, callback)</td>
    <td style="padding:15px">getExtendedCommunityList</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/extended-community-lists/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateExtendedCommunityList(tenantNetworkId, extendedCommunityListId, body, callback)</td>
    <td style="padding:15px">updateExtendedCommunityList</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/extended-community-lists/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtendedCommunityList(tenantNetworkId, extendedCommunityListId, callback)</td>
    <td style="padding:15px">deleteExtendedCommunityList</td>
    <td style="padding:15px">{base_path}/{version}/tenantnetworks/{pathv1}/extended-community-lists/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
