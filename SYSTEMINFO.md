# Alkira

Vendor: Alkira
Homepage: https://www.alkira.com/

Product: Cloud Management
Product Page: https://www.alkira.com/multi-cloud-networking/

## Introduction
We classify Alkira into the Cloud domain as Alkira provide Cloud Services. We also classify it into the Inventory domain because it contains an inventory of Alkira connectors.

"Alkira modernizes networking through a cloud backbone that connects data centers, sites, users and partners" 
"Ensure end-to-end security across your entire cloud network with insight and control" 

The Alkira adapter can be integrated to the Itential Device Broker which will allow your Alkira connectors to be managed within the Itential Configuration Manager Application.

## Why Integrate
The Alkira adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Alkira Cloud Management. With this adapter you have the ability to perform operations such as:

- Configure and Manage Alkira Connectors. 
- Get Segment ID
- Create IPSec Connector
- Create Internet Connector
- Create AWS VPC Connector
- Create Azure VNet Connector

## Additional Product Documentation


