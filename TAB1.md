# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Alkira System. The API that was used to build the adapter for Alkira is usually available in the report directory of this adapter. The adapter utilizes the Alkira API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Alkira adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Alkira Cloud Management. With this adapter you have the ability to perform operations such as:

- Configure and Manage Alkira Connectors. 
- Get Segment ID
- Create IPSec Connector
- Create Internet Connector
- Create AWS VPC Connector
- Create Azure VNet Connector

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
