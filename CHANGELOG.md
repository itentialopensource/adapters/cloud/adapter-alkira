
## 0.6.5 [10-15-2024]

* Changes made at 2024.10.14_19:46PM

See merge request itentialopensource/adapters/adapter-alkira!19

---

## 0.6.4 [08-22-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-alkira!17

---

## 0.6.3 [08-14-2024]

* Changes made at 2024.08.14_17:53PM

See merge request itentialopensource/adapters/adapter-alkira!16

---

## 0.6.2 [08-06-2024]

* Changes made at 2024.08.06_19:07PM

See merge request itentialopensource/adapters/adapter-alkira!15

---

## 0.6.1 [08-05-2024]

* Changes made at 2024.08.05_19:15PM

See merge request itentialopensource/adapters/adapter-alkira!14

---

## 0.6.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-alkira!13

---

## 0.5.4 [03-28-2024]

* Changes made at 2024.03.28_13:09PM

See merge request itentialopensource/adapters/cloud/adapter-alkira!12

---

## 0.5.3 [03-11-2024]

* Changes made at 2024.03.11_15:28PM

See merge request itentialopensource/adapters/cloud/adapter-alkira!11

---

## 0.5.2 [02-28-2024]

* Changes made at 2024.02.28_11:38AM

See merge request itentialopensource/adapters/cloud/adapter-alkira!10

---

## 0.5.1 [02-20-2024]

* changes for broker calls

See merge request itentialopensource/adapters/cloud/adapter-alkira!9

---

## 0.5.0 [12-14-2023]

* More migration changes

See merge request itentialopensource/adapters/cloud/adapter-alkira!8

---

## 0.4.0 [11-06-2023]

* More migration changes

See merge request itentialopensource/adapters/cloud/adapter-alkira!8

---

## 0.3.1 [09-12-2023]

* more migration & metadata changes

See merge request itentialopensource/adapters/cloud/adapter-alkira!7

---

## 0.3.0 [08-16-2023]

* Minor/2023 migration

See merge request itentialopensource/adapters/cloud/adapter-alkira!6

---

## 0.2.0 [08-11-2023]

* Minor/2023 migration

See merge request itentialopensource/adapters/cloud/adapter-alkira!6

---

## 0.1.7 [08-01-2023]

* Bug fixes and performance improvements

See commit a5678d1

---

## 0.1.6 [08-01-2023]

* Migrate adapter base with broker changes

See merge request itentialopensource/adapters/cloud/adapter-alkira!4

---

## 0.1.5 [05-02-2023]

* Migrate adapter base with broker changes

See merge request itentialopensource/adapters/cloud/adapter-alkira!4

---

## 0.1.4 [04-12-2023]

* Patch/fixed get config and get count in adapterBase.json

See merge request itentialopensource/adapters/cloud/adapter-alkira!3

---

## 0.1.3 [04-06-2023]

* bug fix for getConfig and iapGetDeviceCount in adapterBase.js

See merge request itentialopensource/adapters/cloud/adapter-alkira!2

---

## 0.1.2 [12-21-2022]

* Updates to authentication schemas

See merge request itentialopensource/adapters/cloud/adapter-alkira!1

---

## 0.1.1 [11-28-2022]

* Bug fixes and performance improvements

See commit c896979

---
