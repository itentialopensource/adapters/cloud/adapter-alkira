/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-alkira',
      type: 'Alkira',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Alkira = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Alkira Adapter Test', () => {
  describe('Alkira Class Tests', () => {
    const a = new Alkira(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const authenticationPostLoginBodyParam = {};
    describe('#postLogin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postLogin(authenticationPostLoginBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'postLogin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationOauthLoginBodyParam = {};
    describe('#oauthLogin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.oauthLogin(authenticationOauthLoginBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'oauthLogin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationOtp = 'fakedata';
    describe('#otpLogin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.otpLogin(authenticationOtp, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'otpLogin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationCreateSessionBodyParam = {};
    describe('#createSession - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createSession(authenticationCreateSessionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'createSession', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationLoginUsersUsingPOSTBodyParam = {};
    describe('#loginUsersUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.loginUsersUsingPOST(authenticationLoginUsersUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'loginUsersUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationLogoutUsersUsingPOSTBodyParam = {};
    describe('#logoutUsersUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.logoutUsersUsingPOST(null, authenticationLogoutUsersUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'logoutUsersUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogout - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLogout((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'getLogout', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const segmentsTenantNetworkId = 555;
    const segmentsCreateSegmentBodyParam = {};
    describe('#createSegment - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createSegment(segmentsTenantNetworkId, segmentsCreateSegmentBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Segments', 'createSegment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getsegments - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getsegments(segmentsTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Segments', 'getsegments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const segmentsSegmentId = 555;
    const segmentsUpdateSegmentBodyParam = {};
    describe('#updateSegment - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSegment(segmentsTenantNetworkId, segmentsSegmentId, segmentsUpdateSegmentBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Segments', 'updateSegment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSegment - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSegment(segmentsTenantNetworkId, segmentsSegmentId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Segments', 'getSegment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const groupsTenantNetworkId = 555;
    const groupsCreategroupsBodyParam = {};
    describe('#creategroups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.creategroups(groupsTenantNetworkId, groupsCreategroupsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'creategroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getgroups(groupsTenantNetworkId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'getgroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const groupsGroupId = 555;
    const groupsUpdategroupsBodyParam = {};
    describe('#updategroups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updategroups(groupsTenantNetworkId, groupsGroupId, groupsUpdategroupsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'updategroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getgroup(groupsTenantNetworkId, groupsGroupId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'getgroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let usersUserId = 'fakedata';
    const usersCreateUsersUsingPOSTBodyParam = {
      email: 'john@abc.com',
      firstName: 'John',
      lastName: 'Doe',
      password: 'string',
      roles: [
        'netadmin'
      ],
      userName: 'john'
    };
    describe('#createUsersUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createUsersUsingPOST(usersCreateUsersUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('john@abc.com', data.response.email);
                assert.equal('John', data.response.firstName);
                assert.equal('78772b5e-ed20-11ea-adc1-0242ac120777', data.response.id);
                assert.equal('78772b5e-ed20-11ea-adc1-0242ac120002', data.response.idpId);
                assert.equal(8, data.response.lastLoggedIn);
                assert.equal('Doe', data.response.lastName);
                assert.equal('string', data.response.password);
                assert.equal(true, Array.isArray(data.response.roles));
                assert.equal('1', data.response.tenantId);
                assert.equal('john', data.response.userName);
                assert.equal('https://www.alkira.com', data.response.jwksUri);
              } else {
                runCommonAsserts(data, error);
              }
              usersUserId = data.response.id;
              saveMockData('Users', 'createUsersUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUpdateUserProfileUsingPUTBodyParam = {};
    describe('#updateUserProfileUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateUserProfileUsingPUT(null, usersUpdateUserProfileUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'updateUserProfileUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserProfileUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getUserProfileUsingGET(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUserProfileUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllUsersUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllUsersUsingGET(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getAllUsersUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUpdateUsersByIdUsingPUTBodyParam = {
      email: 'john@abc.com',
      firstName: 'John',
      lastName: 'Doe',
      password: 'string',
      roles: [
        'netadmin'
      ],
      userName: 'john'
    };
    describe('#updateUsersByIdUsingPUT - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateUsersByIdUsingPUT(usersUserId, usersUpdateUsersByIdUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'updateUsersByIdUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersByIdUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsersByIdUsingGET(null, usersUserId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('john@abc.com', data.response.email);
                assert.equal('John', data.response.firstName);
                assert.equal('78772b5e-ed20-11ea-adc1-0242ac120777', data.response.id);
                assert.equal('78772b5e-ed20-11ea-adc1-0242ac120002', data.response.idpId);
                assert.equal(10, data.response.lastLoggedIn);
                assert.equal('Doe', data.response.lastName);
                assert.equal('string', data.response.password);
                assert.equal(true, Array.isArray(data.response.roles));
                assert.equal('1', data.response.tenantId);
                assert.equal('john', data.response.userName);
                assert.equal('https://www.alkira.com', data.response.jwksUri);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsersByIdUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userGroupsCreateUserGroupBodyParam = {};
    describe('#createUserGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createUserGroup(userGroupsCreateUserGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserGroups', 'createUserGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userGroupsUsergroupId = 'fakedata';
    const userGroupsAddUsersToUserGroupBodyParam = [
      {}
    ];
    describe('#addUsersToUserGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addUsersToUserGroup(userGroupsUsergroupId, userGroupsAddUsersToUserGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserGroups', 'addUsersToUserGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getusergroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getusergroups((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserGroups', 'getusergroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userGroupsUpdateUserGroupBodyParam = {};
    describe('#updateUserGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateUserGroup(userGroupsUsergroupId, userGroupsUpdateUserGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserGroups', 'updateUserGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getUserGroup(userGroupsUsergroupId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserGroups', 'getUserGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userGroupsReplaceUsersInUserGroupBodyParam = [
      {}
    ];
    describe('#replaceUsersInUserGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.replaceUsersInUserGroup(userGroupsUsergroupId, userGroupsReplaceUsersInUserGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserGroups', 'replaceUsersInUserGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersInUserGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsersInUserGroup(userGroupsUsergroupId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserGroups', 'getUsersInUserGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userGroupsUserId = 'fakedata';
    describe('#getUserFromUserGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUserFromUserGroup(userGroupsUsergroupId, userGroupsUserId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('john@abc.com', data.response.email);
                assert.equal('John', data.response.firstName);
                assert.equal('78772b5e-ed20-11ea-adc1-0242ac120777', data.response.id);
                assert.equal('78772b5e-ed20-11ea-adc1-0242ac120002', data.response.idpId);
                assert.equal(10, data.response.lastLoggedIn);
                assert.equal('Doe', data.response.lastName);
                assert.equal('string', data.response.password);
                assert.equal(true, Array.isArray(data.response.roles));
                assert.equal('1', data.response.tenantId);
                assert.equal('john', data.response.userName);
                assert.equal('https://www.alkira.com', data.response.jwksUri);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserGroups', 'getUserFromUserGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rolesCreateRoleUsingPOSTBodyParam = {
      id: '787723d4-ed20-11ea-adc1-0242ac120002',
      idpName: 'network_administrators',
      name: 'netadmin',
      permissions: [
        'network-configuration-write',
        'user-management-read'
      ],
      readOnly: true
    };
    describe('#createRoleUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createRoleUsingPOST(rolesCreateRoleUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'createRoleUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRolesUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRolesUsingGET((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'getRolesUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rolesRoleId = 'fakedata';
    const rolesUpdateRoleUsingPUT1BodyParam = {
      id: '787723d4-ed20-11ea-adc1-0242ac120002',
      idpName: 'network_administrators',
      name: 'netadmin',
      permissions: [
        'network-configuration-write',
        'user-management-read'
      ],
      readOnly: true
    };
    describe('#updateRoleUsingPUT1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateRoleUsingPUT1(rolesRoleId, rolesUpdateRoleUsingPUT1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'updateRoleUsingPUT1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoleByIdUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRoleByIdUsingGET(rolesRoleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'getRoleByIdUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionsUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPermissionsUsingGET(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response[0]);
                assert.equal('string', data.response[1]);
                assert.equal('string', data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Permissions', 'getPermissionsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const identityProvidersAddIdentityProviderUsingPOSTBodyParam = {
      enabled: true,
      id: 'string',
      idpEntityId: 'string',
      idpMultiCertificate: [
        'string'
      ],
      idpSSOUrl: 'string',
      idpSigningCertificate: 'string',
      metadata: 'string',
      metadataFileName: 'metadata.xml',
      name: 'OKTA',
      serviceMetadata: 'string',
      tenantId: 1,
      type: null
    };
    describe('#addIdentityProviderUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addIdentityProviderUsingPOST(identityProvidersAddIdentityProviderUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProviders', 'addIdentityProviderUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityProvidersUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIdentityProvidersUsingGET((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProviders', 'getIdentityProvidersUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const identityProvidersIdentityProviderId = 'fakedata';
    const identityProvidersUpdateIdentityProviderUsingPUTBodyParam = {
      enabled: true,
      id: 'string',
      idpEntityId: 'string',
      idpMultiCertificate: [
        'string'
      ],
      idpSSOUrl: 'string',
      idpSigningCertificate: 'string',
      metadata: 'string',
      metadataFileName: 'metadata.xml',
      name: 'OKTA',
      serviceMetadata: 'string',
      tenantId: 1,
      type: null
    };
    describe('#updateIdentityProviderUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateIdentityProviderUsingPUT(identityProvidersIdentityProviderId, identityProvidersUpdateIdentityProviderUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProviders', 'updateIdentityProviderUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityProviderUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getIdentityProviderUsingGET(identityProvidersIdentityProviderId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProviders', 'getIdentityProviderUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const baseConnectorsTenantNetworkId = 555;
    describe('#getconnectors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getconnectors(baseConnectorsTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BaseConnectors', 'getconnectors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const baseConnectorsConnectorId = 555;
    describe('#getConnector - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConnector(baseConnectorsTenantNetworkId, baseConnectorsConnectorId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.id);
                assert.equal(1234, data.response.tenantId);
                assert.equal(1234, data.response.tenantNetworkId);
                assert.equal('connector1', data.response.name);
                assert.equal('string', data.response.internalName);
                assert.equal('hadoop clusters', data.response.description);
                assert.equal('5c003a92-77df-11ec-90d6-0242ac120003', data.response.cxpId);
                assert.equal('47709960-77e3-11ec-90d6-0242ac120003', data.response.productId);
                assert.equal('INTERNAL', data.response.group);
                assert.equal(76, data.response.groupId);
                assert.equal(47, data.response.tagId);
                assert.equal(32, data.response.implicitGroupId);
                assert.equal(true, Array.isArray(data.response.segments));
                assert.equal(true, Array.isArray(data.response.segmentIds));
                assert.equal('object', typeof data.response.segmentOptions);
                assert.equal('object', typeof data.response.extraAttributes);
                assert.equal(true, Array.isArray(data.response.instances));
                assert.equal('object', typeof data.response.state);
                assert.equal(65512, data.response.asn);
                assert.equal('object', typeof data.response.docState);
                assert.equal(-1, data.response.deletedAt);
                assert.equal(1657063116, data.response.lastProvisionedAt);
                assert.equal(1657063116, data.response.lastEnabled);
                assert.equal(1657061851242, data.response.lastConfigUpdatedAt);
                assert.equal(1657070000, data.response.configDownloadedAt);
                assert.equal(true, data.response.configAvailable);
                assert.equal(true, Array.isArray(data.response.billingTags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BaseConnectors', 'getConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aWSVPCConnectorsTenantNetworkId = 555;
    const aWSVPCConnectorsCreateAWSVPCConnectorBodyParam = {};
    describe('#createAWSVPCConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createAWSVPCConnector(aWSVPCConnectorsTenantNetworkId, aWSVPCConnectorsCreateAWSVPCConnectorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AWSVPCConnectors', 'createAWSVPCConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getawsvpcconnectors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getawsvpcconnectors(aWSVPCConnectorsTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AWSVPCConnectors', 'getawsvpcconnectors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aWSVPCConnectorsAwsvpcconnectorId = 555;
    const aWSVPCConnectorsUpdateAWSVPCConnectorBodyParam = {};
    describe('#updateAWSVPCConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateAWSVPCConnector(aWSVPCConnectorsTenantNetworkId, aWSVPCConnectorsAwsvpcconnectorId, aWSVPCConnectorsUpdateAWSVPCConnectorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AWSVPCConnectors', 'updateAWSVPCConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAWSVPCConnector - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAWSVPCConnector(aWSVPCConnectorsTenantNetworkId, aWSVPCConnectorsAwsvpcconnectorId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.billingTags));
                assert.equal(true, data.response.enabled);
                assert.equal(false, data.response.primary);
                assert.equal(true, Array.isArray(data.response.secondaryCXPs));
                assert.equal('test', data.response.customerName);
                assert.equal('us-west-1', data.response.customerRegion);
                assert.equal('string', data.response.vgwId);
                assert.equal('vpc-123456', data.response.vpcId);
                assert.equal('string', data.response.vpcOwnerId);
                assert.equal('7c42de1c-7281-11ec-90d6-0242ac120003', data.response.credentialId);
                assert.equal(true, Array.isArray(data.response.segments));
                assert.equal(true, data.response.directInterVPCCommunicationEnabled);
                assert.equal('object', typeof data.response.vpcRouting);
                assert.equal(true, Array.isArray(data.response.tgwAttachments));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AWSVPCConnectors', 'getAWSVPCConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const azureVNETConnectorsTenantNetworkId = 555;
    const azureVNETConnectorsCreateAzureVNETConnectorBodyParam = {};
    describe('#createAzureVNETConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createAzureVNETConnector(azureVNETConnectorsTenantNetworkId, azureVNETConnectorsCreateAzureVNETConnectorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AzureVNETConnectors', 'createAzureVNETConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getazurevnetconnectors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getazurevnetconnectors(azureVNETConnectorsTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AzureVNETConnectors', 'getazurevnetconnectors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const azureVNETConnectorsAzurevnetconnectorId = 555;
    const azureVNETConnectorsUpdateAzureVNETConnectorBodyParam = {};
    describe('#updateAzureVNETConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateAzureVNETConnector(azureVNETConnectorsTenantNetworkId, azureVNETConnectorsAzurevnetconnectorId, azureVNETConnectorsUpdateAzureVNETConnectorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AzureVNETConnectors', 'updateAzureVNETConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAzureVNETConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAzureVNETConnector(azureVNETConnectorsTenantNetworkId, azureVNETConnectorsAzurevnetconnectorId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AzureVNETConnectors', 'getAzureVNETConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gCPConnectorsTenantNetworkId = 555;
    const gCPConnectorsCreateGCPConnectorBodyParam = {};
    describe('#createGCPConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createGCPConnector(gCPConnectorsTenantNetworkId, gCPConnectorsCreateGCPConnectorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GCPConnectors', 'createGCPConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgcpconnectors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getgcpconnectors(gCPConnectorsTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GCPConnectors', 'getgcpconnectors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gCPConnectorsGcpconnectorId = 555;
    const gCPConnectorsUpdateGCPConnectorBodyParam = {};
    describe('#updateGCPConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateGCPConnector(gCPConnectorsTenantNetworkId, gCPConnectorsGcpconnectorId, gCPConnectorsUpdateGCPConnectorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GCPConnectors', 'updateGCPConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGCPConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getGCPConnector(gCPConnectorsTenantNetworkId, gCPConnectorsGcpconnectorId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GCPConnectors', 'getGCPConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oCIVCNConnectorsTenantNetworkId = 555;
    const oCIVCNConnectorsCreateOCIVCNConnectorBodyParam = {};
    describe('#createOCIVCNConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createOCIVCNConnector(oCIVCNConnectorsTenantNetworkId, oCIVCNConnectorsCreateOCIVCNConnectorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OCIVCNConnectors', 'createOCIVCNConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOCIVCNConnectors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOCIVCNConnectors(oCIVCNConnectorsTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OCIVCNConnectors', 'getOCIVCNConnectors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oCIVCNConnectorsOCIVCNConnectorId = 555;
    const oCIVCNConnectorsUpdateOCIVCNConnectorBodyParam = {};
    describe('#updateOCIVCNConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateOCIVCNConnector(oCIVCNConnectorsTenantNetworkId, oCIVCNConnectorsOCIVCNConnectorId, oCIVCNConnectorsUpdateOCIVCNConnectorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OCIVCNConnectors', 'updateOCIVCNConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOCIVCNConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getOCIVCNConnector(oCIVCNConnectorsTenantNetworkId, oCIVCNConnectorsOCIVCNConnectorId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OCIVCNConnectors', 'getOCIVCNConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iPSecConnectorsTenantNetworkId = 555;
    const iPSecConnectorsCreateIPSecConnectorBodyParam = {};
    describe('#createIPSecConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createIPSecConnector(iPSecConnectorsTenantNetworkId, iPSecConnectorsCreateIPSecConnectorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPSecConnectors', 'createIPSecConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getipsecconnectors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getipsecconnectors(iPSecConnectorsTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPSecConnectors', 'getipsecconnectors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iPSecConnectorsIpsecconnectorId = 555;
    const iPSecConnectorsUpdateIPSecConnectorBodyParam = {};
    describe('#updateIPSecConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateIPSecConnector(iPSecConnectorsTenantNetworkId, iPSecConnectorsIpsecconnectorId, iPSecConnectorsUpdateIPSecConnectorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPSecConnectors', 'updateIPSecConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPSecConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getIPSecConnector(iPSecConnectorsTenantNetworkId, iPSecConnectorsIpsecconnectorId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPSecConnectors', 'getIPSecConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iPSecConnectorsSiteId = 555;
    describe('#getIPSecConnectorSiteConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getIPSecConnectorSiteConfiguration(iPSecConnectorsTenantNetworkId, iPSecConnectorsIpsecconnectorId, iPSecConnectorsSiteId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPSecConnectors', 'getIPSecConnectorSiteConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ciscoSDWANConnectorsTenantNetworkId = 555;
    const ciscoSDWANConnectorsCreateCiscoSDWANIngressBodyParam = {};
    describe('#createCiscoSDWANIngress - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createCiscoSDWANIngress(ciscoSDWANConnectorsTenantNetworkId, ciscoSDWANConnectorsCreateCiscoSDWANIngressBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CiscoSDWANConnectors', 'createCiscoSDWANIngress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getciscosdwaningresses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getciscosdwaningresses(ciscoSDWANConnectorsTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CiscoSDWANConnectors', 'getciscosdwaningresses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ciscoSDWANConnectorsCiscosdwaningressId = 555;
    const ciscoSDWANConnectorsUpdateCiscoSDWANIngressBodyParam = {};
    describe('#updateCiscoSDWANIngress - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateCiscoSDWANIngress(ciscoSDWANConnectorsTenantNetworkId, ciscoSDWANConnectorsCiscosdwaningressId, ciscoSDWANConnectorsUpdateCiscoSDWANIngressBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CiscoSDWANConnectors', 'updateCiscoSDWANIngress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCiscoSDWANIngress - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCiscoSDWANIngress(ciscoSDWANConnectorsTenantNetworkId, ciscoSDWANConnectorsCiscosdwaningressId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CiscoSDWANConnectors', 'getCiscoSDWANIngress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ciscoSDWANConnectorsInstanceId = 555;
    describe('#getCiscoSDWANConnectorInstanceConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCiscoSDWANConnectorInstanceConfiguration(ciscoSDWANConnectorsTenantNetworkId, ciscoSDWANConnectorsCiscosdwaningressId, ciscoSDWANConnectorsInstanceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CiscoSDWANConnectors', 'getCiscoSDWANConnectorInstanceConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const internetConnectorsTenantNetworkId = 555;
    const internetConnectorsCreateInternetConnectorBodyParam = {};
    describe('#createInternetConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createInternetConnector(internetConnectorsTenantNetworkId, internetConnectorsCreateInternetConnectorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InternetConnectors', 'createInternetConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getinternetconnectors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getinternetconnectors(internetConnectorsTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InternetConnectors', 'getinternetconnectors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const internetConnectorsInternetconnectorId = 555;
    const internetConnectorsUpdateInternetConnectorBodyParam = {};
    describe('#updateInternetConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateInternetConnector(internetConnectorsTenantNetworkId, internetConnectorsInternetconnectorId, internetConnectorsUpdateInternetConnectorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InternetConnectors', 'updateInternetConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInternetConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getInternetConnector(internetConnectorsTenantNetworkId, internetConnectorsInternetconnectorId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InternetConnectors', 'getInternetConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInternetConnectorConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInternetConnectorConfiguration(internetConnectorsTenantNetworkId, internetConnectorsInternetconnectorId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InternetConnectors', 'getInternetConnectorConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const directConnectConnectorsTenantNetworkId = 555;
    const directConnectConnectorsCreateDirectConnectConnectorBodyParam = {};
    describe('#createDirectConnectConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createDirectConnectConnector(directConnectConnectorsTenantNetworkId, directConnectConnectorsCreateDirectConnectConnectorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DirectConnectConnectors', 'createDirectConnectConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getdirectconnectconnectors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getdirectconnectconnectors(directConnectConnectorsTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DirectConnectConnectors', 'getdirectconnectconnectors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const directConnectConnectorsDirectconnectconnectorId = 555;
    const directConnectConnectorsUpdateDirectConnectConnectorBodyParam = {};
    describe('#updateDirectConnectConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateDirectConnectConnector(directConnectConnectorsTenantNetworkId, directConnectConnectorsDirectconnectconnectorId, directConnectConnectorsUpdateDirectConnectConnectorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DirectConnectConnectors', 'updateDirectConnectConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDirectConnectConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDirectConnectConnector(directConnectConnectorsTenantNetworkId, directConnectConnectorsDirectconnectconnectorId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DirectConnectConnectors', 'getDirectConnectConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const directConnectConnectorsInstanceId = 555;
    describe('#getDirectConnectConnectorInstanceConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDirectConnectConnectorInstanceConfiguration(directConnectConnectorsTenantNetworkId, directConnectConnectorsDirectconnectconnectorId, directConnectConnectorsInstanceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DirectConnectConnectors', 'getDirectConnectConnectorInstanceConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const azureErConnectorsTenantNetworkId = 555;
    const azureErConnectorsCreateAzureErConnectorBodyParam = {};
    describe('#createAzureErConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createAzureErConnector(azureErConnectorsTenantNetworkId, azureErConnectorsCreateAzureErConnectorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AzureErConnectors', 'createAzureErConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAzureErConnectors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAzureErConnectors(azureErConnectorsTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AzureErConnectors', 'getAzureErConnectors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const azureErConnectorsAzureErConnectorId = 555;
    const azureErConnectorsUpdateAzureErConnectorBodyParam = {};
    describe('#updateAzureErConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateAzureErConnector(azureErConnectorsTenantNetworkId, azureErConnectorsAzureErConnectorId, azureErConnectorsUpdateAzureErConnectorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AzureErConnectors', 'updateAzureErConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAzureErConnector - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAzureErConnector(azureErConnectorsTenantNetworkId, azureErConnectorsAzureErConnectorId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.billingTags));
                assert.equal(true, data.response.enabled);
                assert.equal('10.10.10.0/23', data.response.vhubPrefix);
                assert.equal(true, Array.isArray(data.response.instances));
                assert.equal(true, Array.isArray(data.response.segmentOptions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AzureErConnectors', 'getAzureErConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const azureErConnectorsInstanceId = 555;
    describe('#getAzureErConnectorInstanceConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAzureErConnectorInstanceConfiguration(azureErConnectorsTenantNetworkId, azureErConnectorsAzureErConnectorId, azureErConnectorsInstanceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AzureErConnectors', 'getAzureErConnectorInstanceConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuditLogTags - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAuditLogTags(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuditLogs', 'getAuditLogTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuditLogs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAuditLogs(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuditLogs', 'getAuditLogs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantNetworksCreateTenantNetworkBodyParam = {};
    describe('#createTenantNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createTenantNetwork(tenantNetworksCreateTenantNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantNetworks', 'createTenantNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantNetworksTenantNetworkId = 555;
    describe('#provisionTenantNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.provisionTenantNetwork(tenantNetworksTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantNetworks', 'provisionTenantNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenantNetworks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTenantNetworks((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantNetworks', 'getTenantNetworks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantNetworksUpdateTenantNetworkBodyParam = {};
    describe('#updateTenantNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateTenantNetwork(tenantNetworksTenantNetworkId, tenantNetworksUpdateTenantNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantNetworks', 'updateTenantNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenantNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTenantNetwork(tenantNetworksTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantNetworks', 'getTenantNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const trafficTenantNetworkId = 555;
    const trafficConnectorId = 555;
    describe('#retrievestrafficstatsforconnectorsinthistenant - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrievestrafficstatsforconnectorsinthistenant(trafficTenantNetworkId, null, null, null, null, trafficConnectorId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Traffic', 'retrievestrafficstatsforconnectorsinthistenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrievestheInterCXPtraffic - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrievestheInterCXPtraffic(trafficTenantNetworkId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Traffic', 'retrievestheInterCXPtraffic', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrievestheoveralltrafficgoingtointernetfromCXP - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrievestheoveralltrafficgoingtointernetfromCXP(trafficTenantNetworkId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Traffic', 'retrievestheoveralltrafficgoingtointernetfromCXP', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const trafficServiceId = 555;
    describe('#retrievestrafficstatsforservicesinthistenant - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrievestrafficstatsforservicesinthistenant(trafficTenantNetworkId, null, null, null, null, trafficServiceId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Traffic', 'retrievestrafficstatsforservicesinthistenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrievesactivesessionscountforservicesinthistenant - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrievesactivesessionscountforservicesinthistenant(trafficTenantNetworkId, null, null, null, null, trafficServiceId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Traffic', 'retrievesactivesessionscountforservicesinthistenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrievesinstancecountforservicesinthistenant - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrievesinstancecountforservicesinthistenant(trafficTenantNetworkId, null, null, null, null, trafficServiceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Traffic', 'retrievesinstancecountforservicesinthistenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const firewallZonesTenantNetworkId = 555;
    describe('#getfirewallzones - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getfirewallzones(firewallZonesTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FirewallZones', 'getfirewallzones', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const firewallZonesFirewallzoneId = 555;
    describe('#getFirewallZone - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getFirewallZone(firewallZonesTenantNetworkId, firewallZonesFirewallzoneId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FirewallZones', 'getFirewallZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const internetApplicationsTenantNetworkId = 555;
    const internetApplicationsCreateInternetApplicationBodyParam = {};
    describe('#createInternetApplication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createInternetApplication(internetApplicationsTenantNetworkId, internetApplicationsCreateInternetApplicationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InternetApplications', 'createInternetApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getinternetapplications - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getinternetapplications(internetApplicationsTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InternetApplications', 'getinternetapplications', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const internetApplicationsInternetapplicationId = 555;
    const internetApplicationsUpdateInternetApplicationBodyParam = {};
    describe('#updateInternetApplication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateInternetApplication(internetApplicationsTenantNetworkId, internetApplicationsInternetapplicationId, internetApplicationsUpdateInternetApplicationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InternetApplications', 'updateInternetApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInternetApplication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getInternetApplication(internetApplicationsTenantNetworkId, internetApplicationsInternetapplicationId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InternetApplications', 'getInternetApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pANFWServicesTenantNetworkId = 555;
    const pANFWServicesCreatePANFWServiceBodyParam = {};
    describe('#createPANFWService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createPANFWService(pANFWServicesTenantNetworkId, pANFWServicesCreatePANFWServiceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PANFWServices', 'createPANFWService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getpanfwservices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getpanfwservices(pANFWServicesTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PANFWServices', 'getpanfwservices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pANFWServicesServiceId = 555;
    const pANFWServicesUpdatePANFWServiceBodyParam = {};
    describe('#updatePANFWService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePANFWService(pANFWServicesTenantNetworkId, pANFWServicesServiceId, pANFWServicesUpdatePANFWServiceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PANFWServices', 'updatePANFWService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPANFWService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPANFWService(pANFWServicesTenantNetworkId, pANFWServicesServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PANFWServices', 'getPANFWService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pANFWServicesUpdatePANAutoscaleOptionsUsingPUTBodyParam = {};
    describe('#updatePANAutoscaleOptionsUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePANAutoscaleOptionsUsingPUT(pANFWServicesServiceId, pANFWServicesTenantNetworkId, pANFWServicesUpdatePANAutoscaleOptionsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PANFWServices', 'updatePANAutoscaleOptionsUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPANAutoscaleOptionsUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPANAutoscaleOptionsUsingGET(pANFWServicesServiceId, pANFWServicesTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PANFWServices', 'getPANAutoscaleOptionsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pANFWServicesInstanceId = 555;
    describe('#getPANFWServiceInstanceConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPANFWServiceInstanceConfiguration(pANFWServicesTenantNetworkId, pANFWServicesServiceId, pANFWServicesInstanceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PANFWServices', 'getPANFWServiceInstanceConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const baseServicesTenantNetworkId = 555;
    describe('#getservices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getservices(baseServicesTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BaseServices', 'getservices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const baseServicesServiceId = 555;
    describe('#getService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getService(baseServicesTenantNetworkId, baseServicesServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BaseServices', 'getService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listofallproducts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listofallproducts(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Products', 'listofallproducts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenantResourceLimits - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTenantResourceLimits((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceLimits', 'getTenantResourceLimits', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getResourceUsage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getResourceUsage(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceUsage', 'getResourceUsage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCXPs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCXPs(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CXPs', 'getCXPs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getbillinginformation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getbillinginformation((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BillingInfo', 'getbillinginformation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getinvoicesummaries - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getinvoicesummaries(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InvoiceSummaries', 'getinvoicesummaries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const invoiceSummariesInvoiceId = 555;
    describe('#getInvoiceSummary - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getInvoiceSummary(invoiceSummariesInvoiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InvoiceSummaries', 'getInvoiceSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pingTenantNetworkId = 555;
    const pingConnectorId = 555;
    const pingInstanceId = 555;
    const pingPingFromConnectorInstanceBodyParam = {
      requestId: '1f58cec4-36af-4a0b-aff2-868a4fddb9a8',
      responseMessageType: null,
      tenantId: 8,
      tenantNetworkId: 1,
      connectorId: 1,
      instanceId: 2,
      segment: 'Seg1',
      type: 'UNDERLAY',
      probe: 'TCP',
      srcPort: 3,
      destIp: '123.20.10.12',
      destPort: 4,
      count: 5,
      hops: 2,
      ipTos: 9,
      dontFragment: false
    };
    describe('#pingFromConnectorInstance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pingFromConnectorInstance(pingTenantNetworkId, pingConnectorId, pingInstanceId, pingPingFromConnectorInstanceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ping', 'pingFromConnectorInstance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pingPingFromConnectorBodyParam = {
      requestId: '1f58cec4-36af-4a0b-aff2-868a4fddb9a8',
      responseMessageType: null,
      tenantId: 2,
      tenantNetworkId: 9,
      connectorId: 2,
      instanceId: 1,
      segment: 'Seg1',
      type: 'OVERLAY',
      probe: 'TCP',
      srcPort: 3,
      destIp: '123.20.10.12',
      destPort: 1,
      count: 5,
      hops: 1,
      ipTos: 6,
      dontFragment: true
    };
    describe('#pingFromConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.pingFromConnector(pingTenantNetworkId, pingConnectorId, pingPingFromConnectorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ping', 'pingFromConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getJobs(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'getJobs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobsJobId = 'fakedata';
    describe('#getJob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getJob(jobsJobId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'getJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertTags - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAlertTags(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'getAlertTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlerts - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAlerts(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'getAlerts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsId = 'fakedata';
    describe('#resolved - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.resolved(alertsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'resolved', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const traceRouteTenantNetworkId = 555;
    const traceRouteConnectorId = 555;
    const traceRouteInstanceId = 555;
    const traceRouteTraceRouteFromConnectorInstanceBodyParam = {
      requestId: '1f58cec4-36af-4a0b-aff2-868a4fddb9a8',
      responseMessageType: null,
      tenantId: 9,
      tenantNetworkId: 7,
      connectorId: 2,
      instanceId: 10,
      segment: 'seg1',
      type: 'OVERLAY',
      destIp: '123.20.10.12'
    };
    describe('#traceRouteFromConnectorInstance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.traceRouteFromConnectorInstance(traceRouteTenantNetworkId, traceRouteConnectorId, traceRouteInstanceId, traceRouteTraceRouteFromConnectorInstanceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TraceRoute', 'traceRouteFromConnectorInstance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const traceRouteTraceRouteFromConnectorBodyParam = {
      requestId: '1f58cec4-36af-4a0b-aff2-868a4fddb9a8',
      responseMessageType: null,
      tenantId: 10,
      tenantNetworkId: 9,
      connectorId: 6,
      instanceId: 5,
      segment: 'seg1',
      type: 'OVERLAY',
      destIp: '123.20.10.12'
    };
    describe('#traceRouteFromConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.traceRouteFromConnector(traceRouteTenantNetworkId, traceRouteConnectorId, traceRouteTraceRouteFromConnectorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TraceRoute', 'traceRouteFromConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const logsTenantNetworkId = 555;
    const logsConnectorId = 555;
    const logsInstanceId = 555;
    const logsLogsFromConnectorInstanceBodyParam = {
      requestId: '1f58cec4-36af-4a0b-aff2-868a4fddb9a8',
      responseMessageType: null,
      tenantId: 9,
      tenantNetworkId: 3,
      connectorId: 4,
      instanceId: 6,
      tunnelId: 'tunnel-1f58cec4-36af-4a0b-aff2-868a4fddb9a8',
      type: null,
      since: 6
    };
    describe('#logsFromConnectorInstance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.logsFromConnectorInstance(logsTenantNetworkId, logsConnectorId, logsInstanceId, logsLogsFromConnectorInstanceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Logs', 'logsFromConnectorInstance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const logsLogsFromConnectorBodyParam = {
      requestId: '1f58cec4-36af-4a0b-aff2-868a4fddb9a8',
      responseMessageType: null,
      tenantId: 1,
      tenantNetworkId: 4,
      connectorId: 4,
      instanceId: 5,
      tunnelId: 'tunnel-1f58cec4-36af-4a0b-aff2-868a4fddb9a8',
      type: null,
      since: 6
    };
    describe('#logsFromConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.logsFromConnector(logsTenantNetworkId, logsConnectorId, logsLogsFromConnectorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Logs', 'logsFromConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const getMessagesRequestId = 'fakedata';
    describe('#getMessages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMessages(getMessagesRequestId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GetMessages', 'getMessages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const flowsTenantNetworkId = 555;
    const flowsAppName = 'fakedata';
    describe('#getSessionCountUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSessionCountUsingGET(flowsTenantNetworkId, null, null, null, null, flowsAppName, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Flows', 'getSessionCountUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppNameTimeSeriesUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAppNameTimeSeriesUsingGET(flowsTenantNetworkId, null, null, null, null, flowsAppName, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Flows', 'getAppNameTimeSeriesUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRuleNameTimeSeriesUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRuleNameTimeSeriesUsingGET(flowsTenantNetworkId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Flows', 'getRuleNameTimeSeriesUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopApplicationsUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopApplicationsUsingGET(flowsTenantNetworkId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Flows', 'getTopApplicationsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const flowsConnectorId = 555;
    describe('#getTopApplicationsbyConnectorUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopApplicationsbyConnectorUsingGET(flowsTenantNetworkId, null, null, null, flowsConnectorId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Flows', 'getTopApplicationsbyConnectorUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopPolicyHitsUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopPolicyHitsUsingGET(flowsTenantNetworkId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Flows', 'getTopPolicyHitsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopPolicyHitsbyConnectorUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopPolicyHitsbyConnectorUsingGET(flowsTenantNetworkId, null, null, null, flowsConnectorId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Flows', 'getTopPolicyHitsbyConnectorUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const remoteAccessConnectorsTenantNetworkId = 555;
    describe('#getRemoteAccessConnectors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRemoteAccessConnectors(remoteAccessConnectorsTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RemoteAccessConnectors', 'getRemoteAccessConnectors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const remoteAccessConnectorsConnectorId = 555;
    describe('#getRemoteAccesssConnector - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRemoteAccesssConnector(remoteAccessConnectorsConnectorId, remoteAccessConnectorsTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.id);
                assert.equal(1, data.response.templateId);
                assert.equal('remote-access-connector-US-WEST-1-RemoteUserAccess', data.response.name);
                assert.equal('con-remote_a-4f8a090f-048f-4c59-ac0b-2d1b0d66', data.response.internalName);
                assert.equal(1234, data.response.tagId);
                assert.equal(true, Array.isArray(data.response.billingTags));
                assert.equal(true, Array.isArray(data.response.segments));
                assert.equal(true, Array.isArray(data.response.segmentIds));
                assert.equal(true, Array.isArray(data.response.segmentOptions));
                assert.equal('object', typeof data.response.authenticationOptions);
                assert.equal('object', typeof data.response.advancedOptions);
                assert.equal('object', typeof data.response.docState);
                assert.equal('object', typeof data.response.state);
                assert.equal(1234, data.response.dhParamKeysId);
                assert.equal(100, data.response.maxActiveUsers);
                assert.equal(true, Array.isArray(data.response.serverCertificates));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RemoteAccessConnectors', 'getRemoteAccesssConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRemoteAccesssConnectorConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRemoteAccesssConnectorConfiguration(remoteAccessConnectorsConnectorId, remoteAccessConnectorsTenantNetworkId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RemoteAccessConnectors', 'getRemoteAccesssConnectorConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const remoteAccessConnectorTemplatesTenantNetworkId = 555;
    const remoteAccessConnectorTemplatesCreateConnectorTemplateBodyParam = {};
    describe('#createConnectorTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createConnectorTemplate(remoteAccessConnectorTemplatesTenantNetworkId, remoteAccessConnectorTemplatesCreateConnectorTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.advancedOptions);
                assert.equal(true, Array.isArray(data.response.arguments));
                assert.equal('metadata', data.response.samlIDPMetadata);
                assert.equal('object', typeof data.response.authenticationOptions);
                assert.equal('object', typeof data.response.docState);
                assert.equal(121, data.response.id);
                assert.equal('con-remote_a-364beec4-171f-401e-9bb1-4724e87a', data.response.internalName);
                assert.equal('remote-access-connector-template-1', data.response.name);
                assert.equal(true, Array.isArray(data.response.segmentIds));
                assert.equal(true, Array.isArray(data.response.segmentOptions));
                assert.equal(true, Array.isArray(data.response.segments));
                assert.equal('object', typeof data.response.state);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RemoteAccessConnectorTemplates', 'createConnectorTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConnectorTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConnectorTemplates(remoteAccessConnectorTemplatesTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RemoteAccessConnectorTemplates', 'getConnectorTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const remoteAccessConnectorTemplatesConnectorTemplateId = 555;
    const remoteAccessConnectorTemplatesUpdateConnectorTemplateBodyParam = {};
    describe('#updateConnectorTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateConnectorTemplate(remoteAccessConnectorTemplatesConnectorTemplateId, remoteAccessConnectorTemplatesTenantNetworkId, remoteAccessConnectorTemplatesUpdateConnectorTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RemoteAccessConnectorTemplates', 'updateConnectorTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConnectorTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConnectorTemplate(remoteAccessConnectorTemplatesConnectorTemplateId, remoteAccessConnectorTemplatesTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.advancedOptions);
                assert.equal(true, Array.isArray(data.response.arguments));
                assert.equal('metadata', data.response.samlIDPMetadata);
                assert.equal('object', typeof data.response.authenticationOptions);
                assert.equal('object', typeof data.response.docState);
                assert.equal(121, data.response.id);
                assert.equal('con-remote_a-364beec4-171f-401e-9bb1-4724e87a', data.response.internalName);
                assert.equal('remote-access-connector-template-1', data.response.name);
                assert.equal(true, Array.isArray(data.response.segmentIds));
                assert.equal(true, Array.isArray(data.response.segmentOptions));
                assert.equal(true, Array.isArray(data.response.segments));
                assert.equal('object', typeof data.response.state);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RemoteAccessConnectorTemplates', 'getConnectorTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantTenantId = 555;
    const tenantUpdateTenantBodyParam = {};
    describe('#updateTenant - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTenant(tenantTenantId, 'fakedata', tenantUpdateTenantBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'updateTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenant - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTenant(tenantTenantId, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.id);
                assert.equal('Networking Department', data.response.name);
                assert.equal('Alkira', data.response.companyName);
                assert.equal('networkingDepartment@gmail.com', data.response.emailId);
                assert.equal(true, Array.isArray(data.response.regions));
                assert.equal('networkingDepartment', data.response.subDomain);
                assert.equal('LARGE', data.response.size);
                assert.equal(1600000000000, data.response.tokenCreatedAt);
                assert.equal(1200, data.response.sessionTimeout);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantPreferencesTenantId = 555;
    const tenantPreferencesUpdateTenantPreferencesBodyParam = {};
    describe('#updateTenantPreferences - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateTenantPreferences(tenantPreferencesTenantId, tenantPreferencesUpdateTenantPreferencesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantPreferences', 'updateTenantPreferences', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenantPreferences - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTenantPreferences(tenantPreferencesTenantId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.allowList);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantPreferences', 'getTenantPreferences', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const integrationId = 555;
    let integrationSubType = 'fakedata';
    let integrationType = 'fakedata';
    const integrationCreateIntegrationBodyParam = {};
    describe('#createIntegration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIntegration(integrationId, integrationCreateIntegrationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('My Alert', data.response.name);
                assert.equal('NOTIFICATION', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              integrationSubType = data.response.subType;
              integrationType = data.response.type;
              saveMockData('Integration', 'createIntegration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntegrationsByTypeAndSubType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        integrationSubType = 'fakedata';
        try {
          a.getIntegrationsByTypeAndSubType(integrationId, integrationSubType, integrationType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Integration', 'getIntegrationsByTypeAndSubType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntegrations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIntegrations(integrationId, integrationType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Integration', 'getIntegrations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const integrationIntegrationId = 'fakedata';
    const integrationUpdateIntegrationBodyParam = {};
    describe('#updateIntegration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIntegration(integrationId, integrationIntegrationId, integrationUpdateIntegrationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Integration', 'updateIntegration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudProviderAccountCreateCloudProviderAccountBodyParam = {};
    describe('#createCloudProviderAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createCloudProviderAccount(cloudProviderAccountCreateCloudProviderAccountBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudProviderAccount', 'createCloudProviderAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudProviderAccounts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCloudProviderAccounts((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudProviderAccount', 'getCloudProviderAccounts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudProviderAccountCloudProviderAccountId = 'fakedata';
    const cloudProviderAccountUpdateCloudProviderAccountBodyParam = {};
    describe('#updateCloudProviderAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateCloudProviderAccount(cloudProviderAccountCloudProviderAccountId, cloudProviderAccountUpdateCloudProviderAccountBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudProviderAccount', 'updateCloudProviderAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudProviderAccount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCloudProviderAccount(cloudProviderAccountCloudProviderAccountId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('c0faeeec-3df1-11ec-9bbc-0242ac130002', data.response.id);
                assert.equal(1, data.response.tenantId);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.credentialId);
                assert.equal('3789001020101', data.response.nativeId);
                assert.equal('TestAccount', data.response.nativeName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudProviderAccount', 'getCloudProviderAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudProviderObjectCounts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCloudProviderObjectCounts((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudProviderObject', 'getCloudProviderObjectCounts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudProviderObjectCountByTimes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCloudProviderObjectCountByTimes((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudProviderObject', 'getCloudProviderObjectCountByTimes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudProviderObjectCloudProvider = 'fakedata';
    const cloudProviderObjectCloudProviderObjectType = 'fakedata';
    describe('#getCloudProviderObjects - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCloudProviderObjects(null, null, cloudProviderObjectCloudProvider, null, null, cloudProviderObjectCloudProviderObjectType, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudProviderObject', 'getCloudProviderObjects', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudProviderObjectTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCloudProviderObjectTags(cloudProviderObjectCloudProvider, cloudProviderObjectCloudProviderObjectType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudProviderObject', 'getCloudProviderObjectTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudProviderObjectCloudProviderObjectId = 'fakedata';
    describe('#getCloudProviderObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCloudProviderObject(cloudProviderObjectCloudProviderObjectId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('VPC', data.response.type);
                assert.equal('vpc-123456', data.response.nativeId);
                assert.equal('AZURE', data.response.cloudProvider);
                assert.equal('string', data.response.cloudProviderAccountId);
                assert.equal('string', data.response.cloudProviderAccountNativeId);
                assert.equal('string', data.response.version);
                assert.equal(7, data.response.timeStamp);
                assert.equal('object', typeof data.response.attributes);
                assert.equal(true, Array.isArray(data.response.linkedObjects));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudProviderObject', 'getCloudProviderObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudProviderObjectSyncRequestCreateCloudProviderObjectSyncRequestBodyParam = {};
    describe('#createCloudProviderObjectSyncRequest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createCloudProviderObjectSyncRequest(cloudProviderObjectSyncRequestCreateCloudProviderObjectSyncRequestBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudProviderObjectSyncRequest', 'createCloudProviderObjectSyncRequest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudProviderObjectSyncRequests - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCloudProviderObjectSyncRequests(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudProviderObjectSyncRequest', 'getCloudProviderObjectSyncRequests', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudProviderObjectSyncRequestCloudProviderObjectSyncRequestId = 'fakedata';
    describe('#getCloudProviderObjectSyncRequest - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCloudProviderObjectSyncRequest(cloudProviderObjectSyncRequestCloudProviderObjectSyncRequestId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('9880d082-3e62-11ec-9bbc-0242ac130002', data.response.id);
                assert.equal('thall', data.response.user);
                assert.equal(5, data.response.timeStamp);
                assert.equal('Could not connect to AWS', data.response.failureReason);
                assert.equal('AWS', data.response.cloudProvider);
                assert.equal('7beef156-3e62-11ec-9bbc-0242ac130002', data.response.cloudProviderAccountId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudProviderObjectSyncRequest', 'getCloudProviderObjectSyncRequest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudInsights - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCloudInsights(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudInsight', 'getCloudInsights', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudInsightCategorySummaries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCloudInsightCategorySummaries(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudInsight', 'getCloudInsightCategorySummaries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudInsightCloudInsightId = 'fakedata';
    const cloudInsightUpdateCloudInsightBodyParam = {};
    describe('#updateCloudInsight - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateCloudInsight(cloudInsightCloudInsightId, cloudInsightUpdateCloudInsightBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudInsight', 'updateCloudInsight', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudInsight - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCloudInsight(cloudInsightCloudInsightId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('d50bd876-3e44-11ec-9bbc-0242ac130002', data.response.id);
                assert.equal('e356ec72-3e44-11ec-9bbc-0242ac130002', data.response.definitionId);
                assert.equal(8, data.response.lastRefreshedAt);
                assert.equal('OPERATIONS_EXCELLENCE', data.response.category);
                assert.equal('e236ed6e-3e45-11ec-9bbc-0242ac130002', data.response.cloudProviderAccountId);
                assert.equal(true, data.response.enabled);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.matches));
                assert.equal(true, Array.isArray(data.response.matchedObjectIds));
                assert.equal(true, Array.isArray(data.response.ignoredObjectIds));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudInsight', 'getCloudInsight', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudInsightRefreshRequestCreateCloudInsightRefreshRequestBodyParam = {};
    describe('#createCloudInsightRefreshRequest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createCloudInsightRefreshRequest(cloudInsightRefreshRequestCreateCloudInsightRefreshRequestBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudInsightRefreshRequest', 'createCloudInsightRefreshRequest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudInsightRefreshRequests - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCloudInsightRefreshRequests(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudInsightRefreshRequest', 'getCloudInsightRefreshRequests', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudInsightRefreshRequestCloudInsightRefreshRequestId = 'fakedata';
    describe('#getCloudInsightRefreshRequest - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCloudInsightRefreshRequest(cloudInsightRefreshRequestCloudInsightRefreshRequestId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('5fb1ffa8-3e60-11ec-9bbc-0242ac130002', data.response.id);
                assert.equal(6, data.response.createdAt);
                assert.equal(1, data.response.completedAt);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudInsightRefreshRequest', 'getCloudInsightRefreshRequest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudInsightDefinitions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCloudInsightDefinitions(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudInsightDefinition', 'getCloudInsightDefinitions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudInsightDefinitionSummaries - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCloudInsightDefinitionSummaries(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudInsightDefinition', 'getCloudInsightDefinitionSummaries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudInsightDefinitionCloudInsightDefinitionId = 'fakedata';
    const cloudInsightDefinitionUpdateCloudInsightDefinitionBodyParam = {};
    describe('#updateCloudInsightDefinition - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateCloudInsightDefinition(cloudInsightDefinitionCloudInsightDefinitionId, cloudInsightDefinitionUpdateCloudInsightDefinitionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudInsightDefinition', 'updateCloudInsightDefinition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudInsightDefinition - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCloudInsightDefinition(cloudInsightDefinitionCloudInsightDefinitionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudInsightDefinition', 'getCloudInsightDefinition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudInsightReportConfigurationCreateCloudInsightReportConfigurationBodyParam = {};
    describe('#createCloudInsightReportConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createCloudInsightReportConfiguration(cloudInsightReportConfigurationCreateCloudInsightReportConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudInsightReportConfiguration', 'createCloudInsightReportConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudInsightReportConfigurations - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCloudInsightReportConfigurations(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudInsightReportConfiguration', 'getCloudInsightReportConfigurations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudInsightReportConfigurationCloudInsightReportConfigurationId = 'fakedata';
    describe('#getCloudInsightReportConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCloudInsightReportConfiguration(cloudInsightReportConfigurationCloudInsightReportConfigurationId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('ec53d7c8-3e6b-11ec-9bbc-0242ac130002', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(10, data.response.reportCount);
                assert.equal('object', typeof data.response.scope);
                assert.equal('object', typeof data.response.params);
                assert.equal(1642688235834, data.response.nextScheduledReportGenerationTime);
                assert.equal('John.Doe@alkira.net', data.response.updatedBy);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudInsightReportConfiguration', 'getCloudInsightReportConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudInsightReports - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCloudInsightReports(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudInsightReport', 'getCloudInsightReports', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudInsightReportCloudInsightReportId = 'fakedata';
    describe('#getCloudInsightReport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCloudInsightReport(cloudInsightReportCloudInsightReportId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('ec53d7c8-3e6b-11ec-9bbc-0242ac130002', data.response.id);
                assert.equal(10, data.response.tenantId);
                assert.equal('3ca265a0-79f4-11ec-90d6-0242ac120003', data.response.configurationId);
                assert.equal(1642683700723, data.response.generatedAt);
                assert.equal(true, Array.isArray(data.response.accounts));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudInsightReport', 'getCloudInsightReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const billingTagsCreateBillingTagBodyParam = {};
    describe('#createBillingTag - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createBillingTag(billingTagsCreateBillingTagBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BillingTags', 'createBillingTag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getbillingtags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getbillingtags((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BillingTags', 'getbillingtags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const billingTagsBillingTagId = 'fakedata';
    const billingTagsUpdateBillingTagBodyParam = {};
    describe('#updateBillingTag - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateBillingTag(billingTagsBillingTagId, billingTagsUpdateBillingTagBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BillingTags', 'updateBillingTag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBillingTag - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBillingTag(billingTagsBillingTagId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BillingTags', 'getBillingTag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const arubaEdgeConnectConnectorsTenantNetworkId = 555;
    const arubaEdgeConnectConnectorsCreateArubaEdgeConnectConnectorBodyParam = {};
    describe('#createArubaEdgeConnectConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createArubaEdgeConnectConnector(arubaEdgeConnectConnectorsTenantNetworkId, arubaEdgeConnectConnectorsCreateArubaEdgeConnectConnectorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ArubaEdgeConnectConnectors', 'createArubaEdgeConnectConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getArubaEdgeconnectconnectors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getArubaEdgeconnectconnectors(arubaEdgeConnectConnectorsTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ArubaEdgeConnectConnectors', 'getArubaEdgeconnectconnectors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const arubaEdgeConnectConnectorsArubaEdgeconnectConnectorId = 555;
    const arubaEdgeConnectConnectorsUpdateArubaEdgeConnectConnectorBodyParam = {};
    describe('#updateArubaEdgeConnectConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateArubaEdgeConnectConnector(arubaEdgeConnectConnectorsTenantNetworkId, arubaEdgeConnectConnectorsArubaEdgeconnectConnectorId, arubaEdgeConnectConnectorsUpdateArubaEdgeConnectConnectorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ArubaEdgeConnectConnectors', 'updateArubaEdgeConnectConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getArubaEdgeConnectConnector - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getArubaEdgeConnectConnector(arubaEdgeConnectConnectorsTenantNetworkId, arubaEdgeConnectConnectorsArubaEdgeconnectConnectorId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.billingTags));
                assert.equal(false, data.response.boostMode);
                assert.equal(true, Array.isArray(data.response.arubaEdgeVRFMapping));
                assert.equal(64900, data.response.gatewayBgpAsn);
                assert.equal(true, Array.isArray(data.response.instances));
                assert.equal('8.0.0.3', data.response.version);
                assert.equal(true, Array.isArray(data.response.segments));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ArubaEdgeConnectConnectors', 'getArubaEdgeConnectConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const arubaEdgeConnectConnectorsArubaEdgeconnectconnectorId = 555;
    const arubaEdgeConnectConnectorsInstanceId = 555;
    describe('#getArubaEdgeConnectConnectorInstanceConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getArubaEdgeConnectConnectorInstanceConfiguration(arubaEdgeConnectConnectorsTenantNetworkId, arubaEdgeConnectConnectorsArubaEdgeconnectconnectorId, arubaEdgeConnectConnectorsInstanceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ArubaEdgeConnectConnectors', 'getArubaEdgeConnectConnectorInstanceConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const akamaiProlexicConnectorsTenantNetworkId = 555;
    const akamaiProlexicConnectorsCreateAkamaiProlexicConnectorBodyParam = {};
    describe('#createAkamaiProlexicConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createAkamaiProlexicConnector(akamaiProlexicConnectorsTenantNetworkId, akamaiProlexicConnectorsCreateAkamaiProlexicConnectorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AkamaiProlexicConnectors', 'createAkamaiProlexicConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAkamaiProlexicconnectors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAkamaiProlexicconnectors(akamaiProlexicConnectorsTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AkamaiProlexicConnectors', 'getAkamaiProlexicconnectors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const akamaiProlexicConnectorsId = 555;
    const akamaiProlexicConnectorsUpdateAkamaiProlexicConnectorBodyParam = {};
    describe('#updateAkamaiProlexicConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateAkamaiProlexicConnector(akamaiProlexicConnectorsTenantNetworkId, akamaiProlexicConnectorsId, akamaiProlexicConnectorsUpdateAkamaiProlexicConnectorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AkamaiProlexicConnectors', 'updateAkamaiProlexicConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAkamaiProlexicConnector - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAkamaiProlexicConnector(akamaiProlexicConnectorsTenantNetworkId, akamaiProlexicConnectorsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.billingTags));
                assert.equal(true, data.response.enabled);
                assert.equal('string', data.response.credentialId);
                assert.equal(true, Array.isArray(data.response.segments));
                assert.equal(true, Array.isArray(data.response.overlayConfiguration));
                assert.equal(9, data.response.akamaiBgpAsn);
                assert.equal(true, Array.isArray(data.response.byoipOptions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AkamaiProlexicConnectors', 'getAkamaiProlexicConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudNativeServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCloudNativeServices(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudNativeServices', 'getCloudNativeServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const remoteAccessTenantNetworkId = 555;
    const remoteAccessTemplateId = 555;
    describe('#getAuthenticationAttemptsUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAuthenticationAttemptsUsingGET(remoteAccessTenantNetworkId, null, null, null, null, null, remoteAccessTemplateId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RemoteAccess', 'getAuthenticationAttemptsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRemoteAccessSessionsUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRemoteAccessSessionsUsingGET(remoteAccessTenantNetworkId, null, null, null, null, null, remoteAccessTemplateId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RemoteAccess', 'getRemoteAccessSessionsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRemoteAccessBandwidthUtilizationUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRemoteAccessBandwidthUtilizationUsingGET(remoteAccessTenantNetworkId, remoteAccessTemplateId, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RemoteAccess', 'getRemoteAccessBandwidthUtilizationUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCountUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCountUsingGET(remoteAccessTenantNetworkId, null, null, null, null, null, null, remoteAccessTemplateId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RemoteAccess', 'getCountUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRemoteAccessDataUtilizationUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRemoteAccessDataUtilizationUsingGET(remoteAccessTenantNetworkId, remoteAccessTemplateId, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RemoteAccess', 'getRemoteAccessDataUtilizationUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRemoteAccessTopApplications - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRemoteAccessTopApplications(remoteAccessTenantNetworkId, null, null, null, null, null, null, remoteAccessTemplateId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RemoteAccess', 'getRemoteAccessTopApplications', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRemoteAccessTopPolicyHits - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRemoteAccessTopPolicyHits(remoteAccessTenantNetworkId, null, null, null, null, null, null, remoteAccessTemplateId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RemoteAccess', 'getRemoteAccessTopPolicyHits', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRemoteAccessConnectorDataUtilizationUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRemoteAccessConnectorDataUtilizationUsingGET(remoteAccessTenantNetworkId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RemoteAccess', 'getRemoteAccessConnectorDataUtilizationUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nATPoliciesTenantNetworkId = 555;
    const nATPoliciesCreateNATPolicyBodyParam = {};
    describe('#createNATPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNATPolicy(nATPoliciesTenantNetworkId, nATPoliciesCreateNATPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NATPolicies', 'createNATPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNATPolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNATPolicies(nATPoliciesTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NATPolicies', 'getNATPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nATPoliciesNATPolicyId = 555;
    const nATPoliciesUpdateNATPolicyBodyParam = {};
    describe('#updateNATPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNATPolicy(nATPoliciesTenantNetworkId, nATPoliciesNATPolicyId, nATPoliciesUpdateNATPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NATPolicies', 'updateNATPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNATPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNATPolicy(nATPoliciesTenantNetworkId, nATPoliciesNATPolicyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NATPolicies', 'getNATPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nATRulesTenantNetworkId = 555;
    const nATRulesCreateNATRuleBodyParam = {};
    describe('#createNATRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNATRule(nATRulesTenantNetworkId, nATRulesCreateNATRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NATRules', 'createNATRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNATRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNATRules(nATRulesTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NATRules', 'getNATRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nATRulesNATRuleId = 555;
    const nATRulesUpdateNATRuleBodyParam = {};
    describe('#updateNATRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNATRule(nATRulesTenantNetworkId, nATRulesNATRuleId, nATRulesUpdateNATRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NATRules', 'updateNATRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNATRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNATRule(nATRulesTenantNetworkId, nATRulesNATRuleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NATRules', 'getNATRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const globalCidrListsTenantNetworkId = 555;
    const globalCidrListsCreateGlobalCidrListBodyParam = {};
    describe('#createGlobalCidrList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createGlobalCidrList(globalCidrListsTenantNetworkId, globalCidrListsCreateGlobalCidrListBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(107, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.values));
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GlobalCidrLists', 'createGlobalCidrList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGlobalCidrLists - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGlobalCidrLists(globalCidrListsTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GlobalCidrLists', 'getGlobalCidrLists', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const globalCidrListsGlobalCidrListId = 555;
    const globalCidrListsUpdateGlobalCidrListBodyParam = {};
    describe('#updateGlobalCidrList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateGlobalCidrList(globalCidrListsTenantNetworkId, globalCidrListsGlobalCidrListId, globalCidrListsUpdateGlobalCidrListBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GlobalCidrLists', 'updateGlobalCidrList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGlobalCidrList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGlobalCidrList(globalCidrListsTenantNetworkId, globalCidrListsGlobalCidrListId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(107, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.values));
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GlobalCidrLists', 'getGlobalCidrList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const segmentResourcesTenantNetworkId = 555;
    const segmentResourcesCreateSegmentResourceBodyParam = {};
    describe('#createSegmentResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createSegmentResource(segmentResourcesTenantNetworkId, segmentResourcesCreateSegmentResourceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SegmentResources', 'createSegmentResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getsegmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getsegmentResources(segmentResourcesTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SegmentResources', 'getsegmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const segmentResourcesSegmentResourceId = 555;
    const segmentResourcesUpdateSegmentResourceBodyParam = {};
    describe('#updateSegmentResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSegmentResource(segmentResourcesTenantNetworkId, segmentResourcesSegmentResourceId, segmentResourcesUpdateSegmentResourceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SegmentResources', 'updateSegmentResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSegmentResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSegmentResource(segmentResourcesTenantNetworkId, segmentResourcesSegmentResourceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SegmentResources', 'getSegmentResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const segmentResourceSharesTenantNetworkId = 555;
    const segmentResourceSharesCreateSegmentResourceShareBodyParam = {};
    describe('#createSegmentResourceShare - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createSegmentResourceShare(segmentResourceSharesTenantNetworkId, segmentResourceSharesCreateSegmentResourceShareBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SegmentResourceShares', 'createSegmentResourceShare', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSegmentResourceShares - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSegmentResourceShares(segmentResourceSharesTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SegmentResourceShares', 'getSegmentResourceShares', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const segmentResourceSharesSegmentResourceShareId = 555;
    const segmentResourceSharesUpdateSegmentResourceShareBodyParam = {};
    describe('#updateSegmentResourceShare - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSegmentResourceShare(segmentResourceSharesTenantNetworkId, segmentResourceSharesSegmentResourceShareId, segmentResourceSharesUpdateSegmentResourceShareBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SegmentResourceShares', 'updateSegmentResourceShare', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSegmentResourceShare - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSegmentResourceShare(segmentResourceSharesTenantNetworkId, segmentResourceSharesSegmentResourceShareId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SegmentResourceShares', 'getSegmentResourceShare', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const checkPointFWServicesTenantNetworkId = 555;
    const checkPointFWServicesCreateCheckPointFWServiceBodyParam = {};
    describe('#createCheckPointFWService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createCheckPointFWService(checkPointFWServicesTenantNetworkId, checkPointFWServicesCreateCheckPointFWServiceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CheckPointFWServices', 'createCheckPointFWService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCheckPointFWServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCheckPointFWServices(checkPointFWServicesTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CheckPointFWServices', 'getCheckPointFWServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const checkPointFWServicesServiceId = 555;
    const checkPointFWServicesUpdateCheckPointFWServiceBodyParam = {};
    describe('#updateCheckPointFWService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateCheckPointFWService(checkPointFWServicesTenantNetworkId, checkPointFWServicesServiceId, checkPointFWServicesUpdateCheckPointFWServiceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CheckPointFWServices', 'updateCheckPointFWService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCheckPointFWService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCheckPointFWService(checkPointFWServicesTenantNetworkId, checkPointFWServicesServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CheckPointFWServices', 'getCheckPointFWService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const checkPointFWServicesInstanceId = 555;
    describe('#getCheckPointFWServiceInstanceConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCheckPointFWServiceInstanceConfiguration(checkPointFWServicesTenantNetworkId, checkPointFWServicesServiceId, checkPointFWServicesInstanceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CheckPointFWServices', 'getCheckPointFWServiceInstanceConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fortinetFWServicesTenantNetworkId = 555;
    let fortinetFWServicesServiceId = 555;
    let fortinetFWServicesInstanceId = 555;
    const fortinetFWServicesCreateFortinetFWServiceBodyParam = {};
    describe('#createFortinetFWService - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFortinetFWService(fortinetFWServicesTenantNetworkId, fortinetFWServicesCreateFortinetFWServiceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.id);
                assert.equal('svc-ftntfw-089f0f9e-8740-11eb-8dcd-0242ac130003', data.response.internalName);
                assert.equal('object', typeof data.response.state);
                assert.equal('string', data.response.name);
                assert.equal('7.0.1', data.response.version);
                assert.equal('34e47b10-7af2-11ec-90d6-0242ac120003', data.response.credentialId);
                assert.equal('object', typeof data.response.managementServer);
                assert.equal('PAY_AS_YOU_GO', data.response.licenseType);
                assert.equal(true, Array.isArray(data.response.instances));
                assert.equal(9, data.response.minInstanceCount);
                assert.equal(6, data.response.maxInstanceCount);
                assert.equal(true, Array.isArray(data.response.segments));
                assert.equal('object', typeof data.response.segmentOptions);
                assert.equal(true, Array.isArray(data.response.billingTags));
                assert.equal('OFF', data.response.autoScale);
              } else {
                runCommonAsserts(data, error);
              }
              fortinetFWServicesServiceId = data.response.id;
              fortinetFWServicesInstanceId = data.response.id;
              saveMockData('FortinetFWServices', 'createFortinetFWService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFortinetFWServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFortinetFWServices(fortinetFWServicesTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FortinetFWServices', 'getFortinetFWServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fortinetFWServicesUpdateFortinetFWServiceBodyParam = {};
    describe('#updateFortinetFWService - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFortinetFWService(fortinetFWServicesTenantNetworkId, fortinetFWServicesServiceId, fortinetFWServicesUpdateFortinetFWServiceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FortinetFWServices', 'updateFortinetFWService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFortinetFWService - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFortinetFWService(fortinetFWServicesTenantNetworkId, fortinetFWServicesServiceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.id);
                assert.equal('svc-ftntfw-089f0f9e-8740-11eb-8dcd-0242ac130003', data.response.internalName);
                assert.equal('object', typeof data.response.state);
                assert.equal('string', data.response.name);
                assert.equal('7.0.1', data.response.version);
                assert.equal('34e47b10-7af2-11ec-90d6-0242ac120003', data.response.credentialId);
                assert.equal('object', typeof data.response.managementServer);
                assert.equal('PAY_AS_YOU_GO', data.response.licenseType);
                assert.equal(true, Array.isArray(data.response.instances));
                assert.equal(4, data.response.minInstanceCount);
                assert.equal(9, data.response.maxInstanceCount);
                assert.equal(true, Array.isArray(data.response.segments));
                assert.equal('object', typeof data.response.segmentOptions);
                assert.equal(true, Array.isArray(data.response.billingTags));
                assert.equal('OFF', data.response.autoScale);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FortinetFWServices', 'getFortinetFWService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFortinetFWServiceInstanceConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getFortinetFWServiceInstanceConfiguration(fortinetFWServicesTenantNetworkId, fortinetFWServicesServiceId, fortinetFWServicesInstanceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FortinetFWServices', 'getFortinetFWServiceInstanceConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ciscoFTDvFWServicesTenantNetworkId = 555;
    let ciscoFTDvFWServicesServiceId = 555;
    let ciscoFTDvFWServicesInstanceId = 555;
    const ciscoFTDvFWServicesCreateCiscoFTDvFWServiceBodyParam = {};
    describe('#createCiscoFTDvFWService - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createCiscoFTDvFWService(ciscoFTDvFWServicesTenantNetworkId, ciscoFTDvFWServicesCreateCiscoFTDvFWServiceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
                assert.equal('svc-cscoftdv-089f0f9e-8740-11eb-8dcd-0242ac130003', data.response.internalName);
                assert.equal('My Firewall', data.response.name);
                assert.equal(10, data.response.globalCidrListId);
                assert.equal('02064342-be34-4108-b68a-c67032c46924', data.response.credentialId);
                assert.equal('object', typeof data.response.managementServer);
                assert.equal(true, Array.isArray(data.response.servicesIpAllowList));
                assert.equal(true, Array.isArray(data.response.instances));
                assert.equal(4, data.response.minInstanceCount);
                assert.equal(1, data.response.maxInstanceCount);
                assert.equal(true, Array.isArray(data.response.segments));
                assert.equal('object', typeof data.response.segmentOptions);
                assert.equal(true, Array.isArray(data.response.billingTags));
                assert.equal('IPSEC', data.response.tunnelProtocol);
              } else {
                runCommonAsserts(data, error);
              }
              ciscoFTDvFWServicesServiceId = data.response.id;
              ciscoFTDvFWServicesInstanceId = data.response.id;
              saveMockData('CiscoFTDvFWServices', 'createCiscoFTDvFWService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCiscoFTDvFWServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCiscoFTDvFWServices(ciscoFTDvFWServicesTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CiscoFTDvFWServices', 'getCiscoFTDvFWServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ciscoFTDvFWServicesUpdateCiscoFTDvFWServiceBodyParam = {};
    describe('#updateCiscoFTDvFWService - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateCiscoFTDvFWService(ciscoFTDvFWServicesTenantNetworkId, ciscoFTDvFWServicesServiceId, ciscoFTDvFWServicesUpdateCiscoFTDvFWServiceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CiscoFTDvFWServices', 'updateCiscoFTDvFWService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCiscoFTDvFWService - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCiscoFTDvFWService(ciscoFTDvFWServicesTenantNetworkId, ciscoFTDvFWServicesServiceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.id);
                assert.equal('svc-cscoftdv-089f0f9e-8740-11eb-8dcd-0242ac130003', data.response.internalName);
                assert.equal('My Firewall', data.response.name);
                assert.equal(10, data.response.globalCidrListId);
                assert.equal('02064342-be34-4108-b68a-c67032c46924', data.response.credentialId);
                assert.equal('object', typeof data.response.managementServer);
                assert.equal(true, Array.isArray(data.response.servicesIpAllowList));
                assert.equal(true, Array.isArray(data.response.instances));
                assert.equal(4, data.response.minInstanceCount);
                assert.equal(10, data.response.maxInstanceCount);
                assert.equal(true, Array.isArray(data.response.segments));
                assert.equal('object', typeof data.response.segmentOptions);
                assert.equal(true, Array.isArray(data.response.billingTags));
                assert.equal('IPSEC', data.response.tunnelProtocol);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CiscoFTDvFWServices', 'getCiscoFTDvFWService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCiscoFTDvFWServiceInstanceConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCiscoFTDvFWServiceInstanceConfiguration(ciscoFTDvFWServicesTenantNetworkId, ciscoFTDvFWServicesServiceId, ciscoFTDvFWServicesInstanceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CiscoFTDvFWServices', 'getCiscoFTDvFWServiceInstanceConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const zscalerInternetAccessServicesTenantNetworkId = 555;
    const zscalerInternetAccessServicesCreateZscalerInternetAccessServiceBodyParam = {};
    describe('#createZscalerInternetAccessService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createZscalerInternetAccessService(zscalerInternetAccessServicesTenantNetworkId, zscalerInternetAccessServicesCreateZscalerInternetAccessServiceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ZscalerInternetAccessServices', 'createZscalerInternetAccessService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZscalerInternetAccessServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getZscalerInternetAccessServices(zscalerInternetAccessServicesTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ZscalerInternetAccessServices', 'getZscalerInternetAccessServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const zscalerInternetAccessServicesServiceId = 555;
    const zscalerInternetAccessServicesUpdateZscalerInternetAccessServiceBodyParam = {};
    describe('#updateZscalerInternetAccessService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateZscalerInternetAccessService(zscalerInternetAccessServicesTenantNetworkId, zscalerInternetAccessServicesServiceId, zscalerInternetAccessServicesUpdateZscalerInternetAccessServiceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ZscalerInternetAccessServices', 'updateZscalerInternetAccessService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZscalerInternetAccessService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getZscalerInternetAccessService(zscalerInternetAccessServicesTenantNetworkId, zscalerInternetAccessServicesServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ZscalerInternetAccessServices', 'getZscalerInternetAccessService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const infoBloxServicesTenantNetworkId = 555;
    const infoBloxServicesCreateInfoBloxServiceBodyParam = {};
    describe('#createInfoBloxService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createInfoBloxService(infoBloxServicesTenantNetworkId, infoBloxServicesCreateInfoBloxServiceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InfoBloxServices', 'createInfoBloxService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfoBloxServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInfoBloxServices(infoBloxServicesTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InfoBloxServices', 'getInfoBloxServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const infoBloxServicesServiceId = 555;
    const infoBloxServicesUpdateInfoBloxServiceBodyParam = {};
    describe('#updateInfoBloxService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateInfoBloxService(infoBloxServicesTenantNetworkId, infoBloxServicesServiceId, infoBloxServicesUpdateInfoBloxServiceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InfoBloxServices', 'updateInfoBloxService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfoBloxService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getInfoBloxService(infoBloxServicesTenantNetworkId, infoBloxServicesServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InfoBloxServices', 'getInfoBloxService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenantEnabledFeatures - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTenantEnabledFeatures((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FeatureFlags', 'getTenantEnabledFeatures', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const featureFlagsFeature = 'fakedata';
    describe('#getFeatureFlag - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getFeatureFlag(featureFlagsFeature, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FeatureFlags', 'getFeatureFlag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const packetCaptureTenantNetworkId = 555;
    const packetCaptureConnectorId = 555;
    const packetCaptureCapturePacketsFromConnectorBodyParam = {
      requestId: '1f58cec4-36af-4a0b-aff2-868a4fddb9a8',
      responseMessagetype: 'PACKET_CAPTURE_RESPONSE',
      tenantId: 2,
      tenantNetworkId: 9,
      segment: 'Executive',
      destConnector: 402,
      sourceIp: '123.20.10.11',
      sourcePort: 6000,
      destIp: '123.20.10.12',
      destPort: 3000,
      protocol: null,
      cxp: 'US-WEST',
      action: null,
      captureTimeInSeconds: 600
    };
    describe('#capturePacketsFromConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.capturePacketsFromConnector(packetCaptureTenantNetworkId, packetCaptureConnectorId, packetCaptureCapturePacketsFromConnectorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PacketCapture', 'capturePacketsFromConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const packetCapturePathParam = 'fakedata';
    describe('#downloadPacketCapture - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadPacketCapture(packetCapturePathParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PacketCapture', 'downloadPacketCapture', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPacketCaptureResponses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPacketCaptureResponses((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PacketCapture', 'getPacketCaptureResponses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bYOIPTenantNetworkId = 555;
    let bYOIPByoipId = 555;
    const bYOIPCreateBYOIPBodyParam = {};
    describe('#createBYOIP - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createBYOIP(bYOIPTenantNetworkId, bYOIPCreateBYOIPBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('127.0.0.0/24', data.response.prefix);
                assert.equal(1, data.response.id);
                assert.equal('Example', data.response.description);
                assert.equal(3, data.response.usageCount);
                assert.equal(1636134597000, data.response.createdAt);
                assert.equal('ipv4pool-ec2-1234e123f87654321', data.response.poolId);
                assert.equal('False', data.response.doNotAdvertise);
                assert.equal('None', data.response.errorMessage);
              } else {
                runCommonAsserts(data, error);
              }
              bYOIPByoipId = data.response.id;
              saveMockData('BYOIP', 'createBYOIP', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllBYOIP - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllBYOIP(bYOIPTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BYOIP', 'getAllBYOIP', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bYOIPUpdateBYOIPBodyParam = {};
    describe('#updateBYOIP - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateBYOIP(bYOIPTenantNetworkId, bYOIPByoipId, bYOIPUpdateBYOIPBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BYOIP', 'updateBYOIP', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBYOIP - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBYOIP(bYOIPTenantNetworkId, bYOIPByoipId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BYOIP', 'getBYOIP', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBYOIPReferences - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBYOIPReferences(bYOIPTenantNetworkId, bYOIPByoipId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BYOIP', 'getBYOIPReferences', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const credentialsTenantNetworkId = 555;
    describe('#getallcredentials - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getallcredentials(credentialsTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'getallcredentials', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aWSVPCCredentialsTenantNetworkId = 555;
    const aWSVPCCredentialsAddAWSVPCCredentialsUsingPOSTBodyParam = {};
    describe('#addAWSVPCCredentialsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addAWSVPCCredentialsUsingPOST(aWSVPCCredentialsTenantNetworkId, aWSVPCCredentialsAddAWSVPCCredentialsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AWSVPCCredentials', 'addAWSVPCCredentialsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aWSVPCCredentialsCredentialId = 'fakedata';
    const aWSVPCCredentialsUpdateAWSVPCCredentialsUsingPUTBodyParam = {};
    describe('#updateAWSVPCCredentialsUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateAWSVPCCredentialsUsingPUT(aWSVPCCredentialsTenantNetworkId, aWSVPCCredentialsCredentialId, aWSVPCCredentialsUpdateAWSVPCCredentialsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AWSVPCCredentials', 'updateAWSVPCCredentialsUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const azureCredentialsTenantNetworkId = 555;
    const azureCredentialsAddAzureCredentialsUsingPOSTBodyParam = {};
    describe('#addAzureCredentialsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addAzureCredentialsUsingPOST(azureCredentialsTenantNetworkId, azureCredentialsAddAzureCredentialsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AzureCredentials', 'addAzureCredentialsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const azureCredentialsCredentialId = 'fakedata';
    const azureCredentialsUpdateAzureCredentialsUsingPUTBodyParam = {};
    describe('#updateAzureCredentialsUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateAzureCredentialsUsingPUT(azureCredentialsTenantNetworkId, azureCredentialsCredentialId, azureCredentialsUpdateAzureCredentialsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AzureCredentials', 'updateAzureCredentialsUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oCIVCNCredentialsTenantNetworkId = 555;
    const oCIVCNCredentialsAddOCIVCNCredentialsUsingPOSTBodyParam = {};
    describe('#addOCIVCNCredentialsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addOCIVCNCredentialsUsingPOST(oCIVCNCredentialsTenantNetworkId, oCIVCNCredentialsAddOCIVCNCredentialsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OCIVCNCredentials', 'addOCIVCNCredentialsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oCIVCNCredentialsCredentialId = 'fakedata';
    const oCIVCNCredentialsUpdateOCIVCNCredentialsUsingPUTBodyParam = {};
    describe('#updateOCIVCNCredentialsUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateOCIVCNCredentialsUsingPUT(oCIVCNCredentialsTenantNetworkId, oCIVCNCredentialsCredentialId, oCIVCNCredentialsUpdateOCIVCNCredentialsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OCIVCNCredentials', 'updateOCIVCNCredentialsUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ciscoSDWANInstanceCredentialsTenantNetworkId = 555;
    const ciscoSDWANInstanceCredentialsAddCiscoSDWANInstanceCredentialsUsingPOSTBodyParam = {};
    describe('#addCiscoSDWANInstanceCredentialsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addCiscoSDWANInstanceCredentialsUsingPOST(ciscoSDWANInstanceCredentialsTenantNetworkId, ciscoSDWANInstanceCredentialsAddCiscoSDWANInstanceCredentialsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CiscoSDWANInstanceCredentials', 'addCiscoSDWANInstanceCredentialsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ciscoSDWANInstanceCredentialsCredentialId = 'fakedata';
    const ciscoSDWANInstanceCredentialsUpdateCiscoSDWANInstanceCredentialsUsingPUTBodyParam = {};
    describe('#updateCiscoSDWANInstanceCredentialsUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateCiscoSDWANInstanceCredentialsUsingPUT(ciscoSDWANInstanceCredentialsTenantNetworkId, ciscoSDWANInstanceCredentialsCredentialId, ciscoSDWANInstanceCredentialsUpdateCiscoSDWANInstanceCredentialsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CiscoSDWANInstanceCredentials', 'updateCiscoSDWANInstanceCredentialsUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gCPCredentialsTenantNetworkId = 555;
    const gCPCredentialsAddGCPCredentialsUsingPOSTBodyParam = {};
    describe('#addGCPCredentialsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addGCPCredentialsUsingPOST(gCPCredentialsTenantNetworkId, gCPCredentialsAddGCPCredentialsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GCPCredentials', 'addGCPCredentialsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gCPCredentialsCredentialId = 'fakedata';
    const gCPCredentialsUpdateGCPCredentialsUsingPUTBodyParam = {};
    describe('#updateGCPCredentialsUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateGCPCredentialsUsingPUT(gCPCredentialsTenantNetworkId, gCPCredentialsCredentialId, gCPCredentialsUpdateGCPCredentialsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GCPCredentials', 'updateGCPCredentialsUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lDAPCredentialsTenantNetworkId = 555;
    const lDAPCredentialsAddLDAPCredentialsUsingPOSTBodyParam = {};
    describe('#addLDAPCredentialsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addLDAPCredentialsUsingPOST(lDAPCredentialsTenantNetworkId, lDAPCredentialsAddLDAPCredentialsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPCredentials', 'addLDAPCredentialsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lDAPCredentialsCredentialId = 'fakedata';
    const lDAPCredentialsUpdateLDAPCredentialsUsingPUTBodyParam = {};
    describe('#updateLDAPCredentialsUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateLDAPCredentialsUsingPUT(lDAPCredentialsTenantNetworkId, lDAPCredentialsCredentialId, lDAPCredentialsUpdateLDAPCredentialsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPCredentials', 'updateLDAPCredentialsUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pANCredentialsTenantNetworkId = 555;
    const pANCredentialsAddPANCredentialsUsingPOSTBodyParam = {};
    describe('#addPANCredentialsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addPANCredentialsUsingPOST(pANCredentialsTenantNetworkId, pANCredentialsAddPANCredentialsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PANCredentials', 'addPANCredentialsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pANCredentialsCredentialId = 'fakedata';
    const pANCredentialsUpdatePANCredentialsUsingPUTBodyParam = {};
    describe('#updatePANCredentialsUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePANCredentialsUsingPUT(pANCredentialsTenantNetworkId, pANCredentialsCredentialId, pANCredentialsUpdatePANCredentialsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PANCredentials', 'updatePANCredentialsUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pANInstanceCredentialsTenantNetworkId = 555;
    const pANInstanceCredentialsAddPANInstanceCredentialsUsingPOSTBodyParam = {};
    describe('#addPANInstanceCredentialsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addPANInstanceCredentialsUsingPOST(pANInstanceCredentialsTenantNetworkId, pANInstanceCredentialsAddPANInstanceCredentialsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PANInstanceCredentials', 'addPANInstanceCredentialsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pANInstanceCredentialsCredentialId = 'fakedata';
    const pANInstanceCredentialsUpdatePANInstanceCredentialsUsingPUTBodyParam = {};
    describe('#updatePANInstanceCredentialsUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePANInstanceCredentialsUsingPUT(pANInstanceCredentialsTenantNetworkId, pANInstanceCredentialsCredentialId, pANInstanceCredentialsUpdatePANInstanceCredentialsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PANInstanceCredentials', 'updatePANInstanceCredentialsUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pANMasterKeyCredentialsTenantNetworkId = 555;
    const pANMasterKeyCredentialsAddPANMasterKeyCredentialsUsingPOSTBodyParam = {};
    describe('#addPANMasterKeyCredentialsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addPANMasterKeyCredentialsUsingPOST(pANMasterKeyCredentialsTenantNetworkId, pANMasterKeyCredentialsAddPANMasterKeyCredentialsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PANMasterKeyCredentials', 'addPANMasterKeyCredentialsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pANMasterKeyCredentialsCredentialId = 'fakedata';
    const pANMasterKeyCredentialsUpdatePANMasterKeyCredentialsUsingPUTBodyParam = {};
    describe('#updatePANMasterKeyCredentialsUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePANMasterKeyCredentialsUsingPUT(pANMasterKeyCredentialsTenantNetworkId, pANMasterKeyCredentialsCredentialId, pANMasterKeyCredentialsUpdatePANMasterKeyCredentialsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PANMasterKeyCredentials', 'updatePANMasterKeyCredentialsUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pANRegistrationCredentialsTenantNetworkId = 555;
    const pANRegistrationCredentialsAddPANRegistrationCredentialsUsingPOSTBodyParam = {};
    describe('#addPANRegistrationCredentialsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addPANRegistrationCredentialsUsingPOST(pANRegistrationCredentialsTenantNetworkId, pANRegistrationCredentialsAddPANRegistrationCredentialsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PANRegistrationCredentials', 'addPANRegistrationCredentialsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pANRegistrationCredentialsCredentialId = 'fakedata';
    const pANRegistrationCredentialsUpdatePANRegistrationCredentialsUsingPUTBodyParam = {};
    describe('#updatePANRegistrationCredentialsUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePANRegistrationCredentialsUsingPUT(pANRegistrationCredentialsTenantNetworkId, pANRegistrationCredentialsCredentialId, pANRegistrationCredentialsUpdatePANRegistrationCredentialsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PANRegistrationCredentials', 'updatePANRegistrationCredentialsUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const keyPairCredentialsTenantNetworkId = 555;
    const keyPairCredentialsAddKeyPairCredentialsUsingPOSTBodyParam = {};
    describe('#addKeyPairCredentialsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addKeyPairCredentialsUsingPOST(keyPairCredentialsTenantNetworkId, keyPairCredentialsAddKeyPairCredentialsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('KeyPairCredentials', 'addKeyPairCredentialsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const arubaEdgeConnectCredentialsTenantNetworkId = 555;
    const arubaEdgeConnectCredentialsAddArubaEdgeConnectCredentialsUsingPOSTBodyParam = {};
    describe('#addArubaEdgeConnectCredentialsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addArubaEdgeConnectCredentialsUsingPOST(arubaEdgeConnectCredentialsTenantNetworkId, arubaEdgeConnectCredentialsAddArubaEdgeConnectCredentialsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ArubaEdgeConnectCredentials', 'addArubaEdgeConnectCredentialsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const akamaiProlexicCredentialsTenantNetworkId = 555;
    const akamaiProlexicCredentialsAddAkamaiProlexicCredentialsUsingPOSTBodyParam = {};
    describe('#addAkamaiProlexicCredentialsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addAkamaiProlexicCredentialsUsingPOST(akamaiProlexicCredentialsTenantNetworkId, akamaiProlexicCredentialsAddAkamaiProlexicCredentialsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AkamaiProlexicCredentials', 'addAkamaiProlexicCredentialsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const checkPointFWServiceCredentialsTenantNetworkId = 555;
    const checkPointFWServiceCredentialsAddCheckPointFWServiceCredentialsUsingPOSTBodyParam = {};
    describe('#addCheckPointFWServiceCredentialsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addCheckPointFWServiceCredentialsUsingPOST(checkPointFWServiceCredentialsTenantNetworkId, checkPointFWServiceCredentialsAddCheckPointFWServiceCredentialsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CheckPointFWServiceCredentials', 'addCheckPointFWServiceCredentialsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const checkPointFWServiceCredentialsCredentialId = 'fakedata';
    const checkPointFWServiceCredentialsUpdateCheckPointFWServiceCredentialsUsingPUTBodyParam = {};
    describe('#updateCheckPointFWServiceCredentialsUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateCheckPointFWServiceCredentialsUsingPUT(checkPointFWServiceCredentialsTenantNetworkId, checkPointFWServiceCredentialsCredentialId, checkPointFWServiceCredentialsUpdateCheckPointFWServiceCredentialsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CheckPointFWServiceCredentials', 'updateCheckPointFWServiceCredentialsUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const checkPointFWServiceInstanceCredentialsTenantNetworkId = 555;
    const checkPointFWServiceInstanceCredentialsAddCheckPointFWServiceInstanceCredentialsUsingPOSTBodyParam = {};
    describe('#addCheckPointFWServiceInstanceCredentialsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addCheckPointFWServiceInstanceCredentialsUsingPOST(checkPointFWServiceInstanceCredentialsTenantNetworkId, checkPointFWServiceInstanceCredentialsAddCheckPointFWServiceInstanceCredentialsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CheckPointFWServiceInstanceCredentials', 'addCheckPointFWServiceInstanceCredentialsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const checkPointFWServiceInstanceCredentialsCredentialId = 'fakedata';
    const checkPointFWServiceInstanceCredentialsUpdateCheckPointFWServiceInstanceCredentialsUsingPUTBodyParam = {};
    describe('#updateCheckPointFWServiceInstanceCredentialsUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateCheckPointFWServiceInstanceCredentialsUsingPUT(checkPointFWServiceInstanceCredentialsTenantNetworkId, checkPointFWServiceInstanceCredentialsCredentialId, checkPointFWServiceInstanceCredentialsUpdateCheckPointFWServiceInstanceCredentialsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CheckPointFWServiceInstanceCredentials', 'updateCheckPointFWServiceInstanceCredentialsUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const checkPointFWManagementServerCredentialsTenantNetworkId = 555;
    const checkPointFWManagementServerCredentialsAddCheckPointFWManagementServerCredentialsUsingPOSTBodyParam = {};
    describe('#addCheckPointFWManagementServerCredentialsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addCheckPointFWManagementServerCredentialsUsingPOST(checkPointFWManagementServerCredentialsTenantNetworkId, checkPointFWManagementServerCredentialsAddCheckPointFWManagementServerCredentialsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CheckPointFWManagementServerCredentials', 'addCheckPointFWManagementServerCredentialsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const checkPointFWManagementServerCredentialsCredentialId = 'fakedata';
    const checkPointFWManagementServerCredentialsUpdateCheckPointFWManagementServerCredentialsUsingPUTBodyParam = {};
    describe('#updateCheckPointFWManagementServerCredentialsUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateCheckPointFWManagementServerCredentialsUsingPUT(checkPointFWManagementServerCredentialsTenantNetworkId, checkPointFWManagementServerCredentialsCredentialId, checkPointFWManagementServerCredentialsUpdateCheckPointFWManagementServerCredentialsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CheckPointFWManagementServerCredentials', 'updateCheckPointFWManagementServerCredentialsUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usernamePasswordCredentialsTenantNetworkId = 555;
    const usernamePasswordCredentialsAddUsernamePasswordCredentialsUsingPOSTBodyParam = {};
    describe('#addUsernamePasswordCredentialsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addUsernamePasswordCredentialsUsingPOST(usernamePasswordCredentialsTenantNetworkId, usernamePasswordCredentialsAddUsernamePasswordCredentialsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UsernamePasswordCredentials', 'addUsernamePasswordCredentialsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usernamePasswordCredentialsCredentialId = 'fakedata';
    const usernamePasswordCredentialsUpdateUsernamePasswordCredentialsUsingPUTBodyParam = {};
    describe('#updateUsernamePasswordCredentialsUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateUsernamePasswordCredentialsUsingPUT(usernamePasswordCredentialsTenantNetworkId, usernamePasswordCredentialsCredentialId, usernamePasswordCredentialsUpdateUsernamePasswordCredentialsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UsernamePasswordCredentials', 'updateUsernamePasswordCredentialsUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const splunkHECTokenCredentialsTenantNetworkId = 555;
    const splunkHECTokenCredentialsAddSplunkHECTokenCredentialsUsingPOSTBodyParam = {};
    describe('#addSplunkHECTokenCredentialsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addSplunkHECTokenCredentialsUsingPOST(splunkHECTokenCredentialsTenantNetworkId, splunkHECTokenCredentialsAddSplunkHECTokenCredentialsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SplunkHECTokenCredentials', 'addSplunkHECTokenCredentialsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const splunkHECTokenCredentialsCredentialId = 'fakedata';
    const splunkHECTokenCredentialsUpdateSplunkHECTokenCredentialsUsingPUTBodyParam = {};
    describe('#updateSplunkHECTokenCredentialsUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSplunkHECTokenCredentialsUsingPUT(splunkHECTokenCredentialsTenantNetworkId, splunkHECTokenCredentialsCredentialId, splunkHECTokenCredentialsUpdateSplunkHECTokenCredentialsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SplunkHECTokenCredentials', 'updateSplunkHECTokenCredentialsUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fortinetFWServiceCredentialsTenantNetworkId = 555;
    const fortinetFWServiceCredentialsAddFtntFWServiceCredentialsUsingPOSTBodyParam = {};
    describe('#addFtntFWServiceCredentialsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addFtntFWServiceCredentialsUsingPOST(fortinetFWServiceCredentialsTenantNetworkId, fortinetFWServiceCredentialsAddFtntFWServiceCredentialsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FortinetFWServiceCredentials', 'addFtntFWServiceCredentialsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fortinetFWServiceCredentialsCredentialId = 'fakedata';
    const fortinetFWServiceCredentialsUpdateFtntFWServiceCredentialsUsingPUTBodyParam = {};
    describe('#updateFtntFWServiceCredentialsUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateFtntFWServiceCredentialsUsingPUT(fortinetFWServiceCredentialsTenantNetworkId, fortinetFWServiceCredentialsCredentialId, fortinetFWServiceCredentialsUpdateFtntFWServiceCredentialsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FortinetFWServiceCredentials', 'updateFtntFWServiceCredentialsUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fortinetFWServiceInstanceCredentialsTenantNetworkId = 555;
    const fortinetFWServiceInstanceCredentialsAddFtntFWServiceInstanceCredentialsUsingPOSTBodyParam = {};
    describe('#addFtntFWServiceInstanceCredentialsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addFtntFWServiceInstanceCredentialsUsingPOST(fortinetFWServiceInstanceCredentialsTenantNetworkId, fortinetFWServiceInstanceCredentialsAddFtntFWServiceInstanceCredentialsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FortinetFWServiceInstanceCredentials', 'addFtntFWServiceInstanceCredentialsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fortinetFWServiceInstanceCredentialsCredentialId = 'fakedata';
    const fortinetFWServiceInstanceCredentialsUpdateFtntFWServiceInstanceCredentialsUsingPUTBodyParam = {};
    describe('#updateFtntFWServiceInstanceCredentialsUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateFtntFWServiceInstanceCredentialsUsingPUT(fortinetFWServiceInstanceCredentialsTenantNetworkId, fortinetFWServiceInstanceCredentialsCredentialId, fortinetFWServiceInstanceCredentialsUpdateFtntFWServiceInstanceCredentialsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FortinetFWServiceInstanceCredentials', 'updateFtntFWServiceInstanceCredentialsUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ciscoFTDvFWCredentialsTenantNetworkId = 555;
    const ciscoFTDvFWCredentialsAddCiscoFTDvFWCredentialsUsingPOSTBodyParam = {};
    describe('#addCiscoFTDvFWCredentialsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addCiscoFTDvFWCredentialsUsingPOST(ciscoFTDvFWCredentialsTenantNetworkId, ciscoFTDvFWCredentialsAddCiscoFTDvFWCredentialsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CiscoFTDvFWCredentials', 'addCiscoFTDvFWCredentialsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ciscoFTDvFWCredentialsCredentialId = 'fakedata';
    const ciscoFTDvFWCredentialsUpdateCiscoFTDvFWCredentialsUsingPUTBodyParam = {};
    describe('#updateCiscoFTDvFWCredentialsUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateCiscoFTDvFWCredentialsUsingPUT(ciscoFTDvFWCredentialsTenantNetworkId, ciscoFTDvFWCredentialsCredentialId, ciscoFTDvFWCredentialsUpdateCiscoFTDvFWCredentialsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CiscoFTDvFWCredentials', 'updateCiscoFTDvFWCredentialsUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ciscoFTDvFWInstanceCredentialsTenantNetworkId = 555;
    const ciscoFTDvFWInstanceCredentialsAddCiscoFTDvFWInstanceCredentialsUsingPOSTBodyParam = {};
    describe('#addCiscoFTDvFWInstanceCredentialsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addCiscoFTDvFWInstanceCredentialsUsingPOST(ciscoFTDvFWInstanceCredentialsTenantNetworkId, ciscoFTDvFWInstanceCredentialsAddCiscoFTDvFWInstanceCredentialsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CiscoFTDvFWInstanceCredentials', 'addCiscoFTDvFWInstanceCredentialsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ciscoFTDvFWInstanceCredentialsCredentialId = 'fakedata';
    const ciscoFTDvFWInstanceCredentialsUpdateCiscoFTDvFWInstanceCredentialsUsingPUTBodyParam = {};
    describe('#updateCiscoFTDvFWInstanceCredentialsUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateCiscoFTDvFWInstanceCredentialsUsingPUT(ciscoFTDvFWInstanceCredentialsTenantNetworkId, ciscoFTDvFWInstanceCredentialsCredentialId, ciscoFTDvFWInstanceCredentialsUpdateCiscoFTDvFWInstanceCredentialsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CiscoFTDvFWInstanceCredentials', 'updateCiscoFTDvFWInstanceCredentialsUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const infoBloxCredentialsTenantNetworkId = 555;
    const infoBloxCredentialsAddInfoBloxCredentialsUsingPOSTBodyParam = {};
    describe('#addInfoBloxCredentialsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addInfoBloxCredentialsUsingPOST(infoBloxCredentialsTenantNetworkId, infoBloxCredentialsAddInfoBloxCredentialsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InfoBloxCredentials', 'addInfoBloxCredentialsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const infoBloxInstanceCredentialsTenantNetworkId = 555;
    const infoBloxInstanceCredentialsAddInfoBloxInstanceCredentialsUsingPOSTBodyParam = {};
    describe('#addInfoBloxInstanceCredentialsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addInfoBloxInstanceCredentialsUsingPOST(infoBloxInstanceCredentialsTenantNetworkId, infoBloxInstanceCredentialsAddInfoBloxInstanceCredentialsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InfoBloxInstanceCredentials', 'addInfoBloxInstanceCredentialsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const infoBloxGridMasterCredentialsTenantNetworkId = 555;
    const infoBloxGridMasterCredentialsAddInfoBloxGridMasterCredentialsUsingPOSTBodyParam = {};
    describe('#addInfoBloxGridMasterCredentialsUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addInfoBloxGridMasterCredentialsUsingPOST(infoBloxGridMasterCredentialsTenantNetworkId, infoBloxGridMasterCredentialsAddInfoBloxGridMasterCredentialsUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InfoBloxGridMasterCredentials', 'addInfoBloxGridMasterCredentialsUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let oneTimePasswordOtpId = 'fakedata';
    const oneTimePasswordCreateOneTimePasswordBodyParam = {
      purpose: 'Need help provisioning connector',
      createdAt: 1650488051207,
      duration: 900000
    };
    describe('#createOneTimePassword - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createOneTimePassword(oneTimePasswordCreateOneTimePasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Need help provisioning connector', data.response.purpose);
                assert.equal(1650488051207, data.response.createdAt);
                assert.equal(900000, data.response.duration);
                assert.equal('78772b5e-ed20-11ea-adc1-0242ac120777', data.response.id);
                assert.equal('1', data.response.tenantId);
                assert.equal(1650497051207, data.response.expiresAt);
                assert.equal('78772b5e-ed20-11ea-adc1-0242ac120777', data.response.requesterId);
                assert.equal(true, data.response.used);
              } else {
                runCommonAsserts(data, error);
              }
              oneTimePasswordOtpId = data.response.id;
              saveMockData('OneTimePassword', 'createOneTimePassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllUnexpiredOTP - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllUnexpiredOTP((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OneTimePassword', 'getAllUnexpiredOTP', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const loginSpecificationCreateOrUpdateLoginSpecificationBodyParam = {
      id: '78772b5e-ed20-11ea-adc1-0242ac120777',
      tenantId: '1',
      maxFailedLoginCount: 5,
      lockDurationSeconds: 3600
    };
    describe('#createOrUpdateLoginSpecification - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createOrUpdateLoginSpecification(loginSpecificationCreateOrUpdateLoginSpecificationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('78772b5e-ed20-11ea-adc1-0242ac120777', data.response.id);
                assert.equal('1', data.response.tenantId);
                assert.equal(5, data.response.maxFailedLoginCount);
                assert.equal(3600, data.response.lockDurationSeconds);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoginSpecification', 'createOrUpdateLoginSpecification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoginSpecification - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLoginSpecification((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('78772b5e-ed20-11ea-adc1-0242ac120777', data.response.id);
                assert.equal('1', data.response.tenantId);
                assert.equal(5, data.response.maxFailedLoginCount);
                assert.equal(3600, data.response.lockDurationSeconds);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoginSpecification', 'getLoginSpecification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const passwordSpecificationCreateOrUpdatePasswordSpecificationBodyParam = {
      id: '78772b5e-ed20-11ea-adc1-0242ac120777',
      tenantId: '1',
      minimumPasswordLength: 8,
      minimumDigits: 1,
      minimumUpperCaseLetters: 8,
      minimumSpecialChar: 1,
      nonRepeatingPasswordsCount: 5
    };
    describe('#createOrUpdatePasswordSpecification - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createOrUpdatePasswordSpecification(passwordSpecificationCreateOrUpdatePasswordSpecificationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('78772b5e-ed20-11ea-adc1-0242ac120777', data.response.id);
                assert.equal('1', data.response.tenantId);
                assert.equal(8, data.response.minimumPasswordLength);
                assert.equal(1, data.response.minimumDigits);
                assert.equal(8, data.response.minimumUpperCaseLetters);
                assert.equal(1, data.response.minimumSpecialChar);
                assert.equal(5, data.response.nonRepeatingPasswordsCount);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PasswordSpecification', 'createOrUpdatePasswordSpecification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPasswordSpecification - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPasswordSpecification((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('78772b5e-ed20-11ea-adc1-0242ac120777', data.response.id);
                assert.equal('1', data.response.tenantId);
                assert.equal(8, data.response.minimumPasswordLength);
                assert.equal(1, data.response.minimumDigits);
                assert.equal(8, data.response.minimumUpperCaseLetters);
                assert.equal(1, data.response.minimumSpecialChar);
                assert.equal(5, data.response.nonRepeatingPasswordsCount);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PasswordSpecification', 'getPasswordSpecification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routePoliciesTenantNetworkId = 555;
    const routePoliciesCreateRoutePoliciesBodyParam = {
      name: 'Allow-Default-Route',
      enabled: true,
      direction: null,
      segment: 'Seg1',
      includedGroups: [
        3
      ]
    };
    describe('#createRoutePolicies - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createRoutePolicies(routePoliciesTenantNetworkId, routePoliciesCreateRoutePoliciesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutePolicies', 'createRoutePolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRoutePolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllRoutePolicies(routePoliciesTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutePolicies', 'getAllRoutePolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routePoliciesRoutePolicyId = 555;
    const routePoliciesUpdateRoutePolicyBodyParam = {
      name: 'Allow-Default-Route',
      enabled: true,
      direction: null,
      segment: 'Seg1',
      includedGroups: [
        2
      ]
    };
    describe('#updateRoutePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateRoutePolicy(routePoliciesTenantNetworkId, routePoliciesRoutePolicyId, routePoliciesUpdateRoutePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutePolicies', 'updateRoutePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoutePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRoutePolicy(routePoliciesTenantNetworkId, routePoliciesRoutePolicyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutePolicies', 'getRoutePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInvoiceDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getInvoiceDetails(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'getInvoiceDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const invoicesInvoiceId = 555;
    describe('#getInvoiceDetail - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getInvoiceDetail(invoicesInvoiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'getInvoiceDetail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMeters - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMeters(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Meters', 'getMeters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metersMeterId = 555;
    describe('#getMeterItems - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMeterItems(metersMeterId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Meters', 'getMeterItems', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routesTenantNetworkId = 555;
    describe('#getRoutes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRoutes(routesTenantNetworkId, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Routes', 'getRoutes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesTenantNetworkId = 555;
    const policiesCreatePolicyBodyParam = {};
    describe('#createPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createPolicy(policiesTenantNetworkId, policiesCreatePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'createPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPolicies(policiesTenantNetworkId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'getPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesId = 555;
    const policiesUpdatePolicyBodyParam = {};
    describe('#updatePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePolicy(policiesTenantNetworkId, policiesId, policiesUpdatePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'updatePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPolicy(policiesTenantNetworkId, policiesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'getPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rulesTenantNetworkId = 555;
    const rulesCreateRuleBodyParam = {};
    describe('#createRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createRule(rulesTenantNetworkId, rulesCreateRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rules', 'createRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRules(rulesTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rules', 'getRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rulesId = 555;
    const rulesUpdateRuleBodyParam = {};
    describe('#updateRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateRule(rulesTenantNetworkId, rulesId, rulesUpdateRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rules', 'updateRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRule(rulesTenantNetworkId, rulesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rules', 'getRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rulelistsTenantNetworkId = 555;
    const rulelistsCreateRulelistBodyParam = {};
    describe('#createRulelist - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createRulelist(rulelistsTenantNetworkId, rulelistsCreateRulelistBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rulelists', 'createRulelist', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRulelists - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRulelists(rulelistsTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rulelists', 'getRulelists', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rulelistsId = 555;
    const rulelistsUpdateRulelistBodyParam = {};
    describe('#updateRulelist - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateRulelist(rulelistsTenantNetworkId, rulelistsId, rulelistsUpdateRulelistBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rulelists', 'updateRulelist', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRulelist - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRulelist(rulelistsTenantNetworkId, rulelistsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rulelists', 'getRulelist', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const prefixlistsTenantNetworkId = 555;
    const prefixlistsCreatePrefixlistBodyParam = {};
    describe('#createPrefixlist - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createPrefixlist(prefixlistsTenantNetworkId, prefixlistsCreatePrefixlistBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Prefixlists', 'createPrefixlist', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPrefixlists - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPrefixlists(prefixlistsTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Prefixlists', 'getPrefixlists', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const prefixlistsId = 555;
    const prefixlistsUpdatePrefixlistBodyParam = {};
    describe('#updatePrefixlist - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePrefixlist(prefixlistsTenantNetworkId, prefixlistsId, prefixlistsUpdatePrefixlistBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Prefixlists', 'updatePrefixlist', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPrefixlist - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPrefixlist(prefixlistsTenantNetworkId, prefixlistsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Prefixlists', 'getPrefixlist', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const healthTenantNetworkId = 555;
    describe('#retrieveshealthforallconnectorsandservicesinthistenant - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrieveshealthforallconnectorsandservicesinthistenant(healthTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Health', 'retrieveshealthforallconnectorsandservicesinthistenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const healthConnectorId = 555;
    describe('#retrieveshealthforaspecificconnectorinthistenant - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrieveshealthforaspecificconnectorinthistenant(healthTenantNetworkId, healthConnectorId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Health', 'retrieveshealthforaspecificconnectorinthistenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const healthInstanceId = 555;
    describe('#retrieveshealthforaspecificinstanceofaconnectorinthistenant - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrieveshealthforaspecificinstanceofaconnectorinthistenant(healthTenantNetworkId, healthConnectorId, healthInstanceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Health', 'retrieveshealthforaspecificinstanceofaconnectorinthistenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const healthZoneId = 555;
    describe('#retrieveshealthforaspecificfirewalzoneinthistenant - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrieveshealthforaspecificfirewalzoneinthistenant(healthTenantNetworkId, healthZoneId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Health', 'retrieveshealthforaspecificfirewalzoneinthistenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveshealthforallinternetapplicationsinthistenant - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrieveshealthforallinternetapplicationsinthistenant(healthTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Health', 'retrieveshealthforallinternetapplicationsinthistenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const healthServiceId = 555;
    describe('#retrieveshealthforaspecificserviceinthistenant - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrieveshealthforaspecificserviceinthistenant(healthTenantNetworkId, healthServiceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Health', 'retrieveshealthforaspecificserviceinthistenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveshealthforaspecificinstanceofaserviceinthistenant - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrieveshealthforaspecificinstanceofaserviceinthistenant(healthTenantNetworkId, healthServiceId, healthInstanceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Health', 'retrieveshealthforaspecificinstanceofaserviceinthistenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const healthv3TenantNetworkId = 555;
    describe('#getRetrieveshealthforallconnectorsandservicesinthistenant - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRetrieveshealthforallconnectorsandservicesinthistenant(healthv3TenantNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Healthv3', 'getRetrieveshealthforallconnectorsandservicesinthistenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const flowCaptureTenantNetworkId = 555;
    const flowCaptureConnectorId = 555;
    const flowCaptureCaptureFlowsFromConnectorBodyParam = {
      requestId: '1f58cec4-36af-4a0b-aff2-868a4fddb9a8',
      responseMessagetype: 'FLOWCAPTURE_RESPONSE',
      tenantId: 7,
      tenantNetworkId: 2,
      segment: 'Executive',
      destConnector: 402,
      sourceIp: '123.20.10.11',
      sourcePort: 6000,
      destIp: '123.20.10.12',
      destPort: 3000,
      protocol: null,
      applicationId: 100,
      inetApplicationId: 100,
      userGroupId: 100,
      segmentResourceShareId: 12345,
      cxp: 'US-WEST'
    };
    describe('#captureFlowsFromConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.captureFlowsFromConnector(flowCaptureTenantNetworkId, flowCaptureConnectorId, flowCaptureCaptureFlowsFromConnectorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FlowCapture', 'captureFlowsFromConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const flowCaptureSegmentResourceShareId = 555;
    const flowCaptureCaptureFlowsFromResourceShareBodyParam = {
      requestId: '1f58cec4-36af-4a0b-aff2-868a4fddb9a8',
      responseMessagetype: 'FLOWCAPTURE_RESPONSE',
      tenantId: 3,
      tenantNetworkId: 1,
      segment: 'Executive',
      destConnector: 402,
      sourceIp: '123.20.10.11',
      sourcePort: 6000,
      destIp: '123.20.10.12',
      destPort: 3000,
      protocol: null,
      applicationId: 100,
      inetApplicationId: 100,
      userGroupId: 100,
      segmentResourceShareId: 12345,
      cxp: 'US-WEST'
    };
    describe('#captureFlowsFromResourceShare - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.captureFlowsFromResourceShare(flowCaptureTenantNetworkId, flowCaptureSegmentResourceShareId, flowCaptureCaptureFlowsFromResourceShareBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FlowCapture', 'captureFlowsFromResourceShare', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const activeNATTranslationTenantNetworkId = 555;
    const activeNATTranslationConnectorId = 555;
    const activeNATTranslationGetActiveNatTranslationBodyParam = {
      requestId: '1f58cec4-36af-4a0b-aff2-868a4fddb9a8',
      responseMessagetype: 'ACTIVE_NAT_TRANSLATION_RESPONSE',
      tenantId: 2,
      tenantNetworkId: 4,
      segment: 'Executive',
      originalIp: '123.20.10.11',
      originalPort: 6000,
      translatedIp: '123.20.10.12',
      translatedPort: 3000,
      segmentResourceShareId: 12345,
      cxp: 'US-WEST'
    };
    describe('#getActiveNatTranslation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getActiveNatTranslation(activeNATTranslationTenantNetworkId, activeNATTranslationConnectorId, activeNATTranslationGetActiveNatTranslationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActiveNATTranslation', 'getActiveNatTranslation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const activeNATTranslationSegmentResourceShareId = 555;
    const activeNATTranslationGetActiveNatTranslationForResourceShareBodyParam = {
      requestId: '1f58cec4-36af-4a0b-aff2-868a4fddb9a8',
      responseMessagetype: 'ACTIVE_NAT_TRANSLATION_RESPONSE',
      tenantId: 10,
      tenantNetworkId: 9,
      segment: 'Executive',
      originalIp: '123.20.10.11',
      originalPort: 6000,
      translatedIp: '123.20.10.12',
      translatedPort: 3000,
      segmentResourceShareId: 12345,
      cxp: 'US-WEST'
    };
    describe('#getActiveNatTranslationForResourceShare - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getActiveNatTranslationForResourceShare(activeNATTranslationTenantNetworkId, activeNATTranslationSegmentResourceShareId, activeNATTranslationGetActiveNatTranslationForResourceShareBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActiveNATTranslation', 'getActiveNatTranslationForResourceShare', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aSPathListsTenantNetworkId = 555;
    const aSPathListsCreateAsPathListsBodyParam = {
      id: 10,
      name: 'string',
      description: 'string',
      values: [
        'string'
      ],
      lastConfigUpdatedAt: 2
    };
    describe('#createAsPathLists - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createAsPathLists(aSPathListsTenantNetworkId, aSPathListsCreateAsPathListsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ASPathLists', 'createAsPathLists', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllAsPathLists - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllAsPathLists(aSPathListsTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ASPathLists', 'getAllAsPathLists', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aSPathListsAsPathListId = 555;
    const aSPathListsUpdateAsPathListBodyParam = {
      id: 8,
      name: 'string',
      description: 'string',
      values: [
        'string'
      ],
      lastConfigUpdatedAt: 10
    };
    describe('#updateAsPathList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateAsPathList(aSPathListsTenantNetworkId, aSPathListsAsPathListId, aSPathListsUpdateAsPathListBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ASPathLists', 'updateAsPathList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAsPathList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAsPathList(aSPathListsTenantNetworkId, aSPathListsAsPathListId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ASPathLists', 'getAsPathList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const communityListsTenantNetworkId = 555;
    const communityListsCreateCommunityListsBodyParam = {
      id: 2,
      name: 'string',
      description: 'string',
      values: [
        'string'
      ],
      lastConfigUpdatedAt: 5
    };
    describe('#createCommunityLists - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createCommunityLists(communityListsTenantNetworkId, communityListsCreateCommunityListsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CommunityLists', 'createCommunityLists', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllCommunityLists - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllCommunityLists(communityListsTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CommunityLists', 'getAllCommunityLists', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const communityListsCommunityListId = 555;
    const communityListsUpdateCommunityListBodyParam = {
      id: 7,
      name: 'string',
      description: 'string',
      values: [
        'string'
      ],
      lastConfigUpdatedAt: 7
    };
    describe('#updateCommunityList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateCommunityList(communityListsTenantNetworkId, communityListsCommunityListId, communityListsUpdateCommunityListBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CommunityLists', 'updateCommunityList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCommunityList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCommunityList(communityListsTenantNetworkId, communityListsCommunityListId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CommunityLists', 'getCommunityList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extendedCommunityListsTenantNetworkId = 555;
    const extendedCommunityListsCreateExtendedCommunityListsBodyParam = {
      id: 2,
      name: 'string',
      description: 'string',
      values: [
        'string'
      ],
      lastConfigUpdatedAt: 7
    };
    describe('#createExtendedCommunityLists - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createExtendedCommunityLists(extendedCommunityListsTenantNetworkId, extendedCommunityListsCreateExtendedCommunityListsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExtendedCommunityLists', 'createExtendedCommunityLists', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllExtendedCommunityLists - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllExtendedCommunityLists(extendedCommunityListsTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExtendedCommunityLists', 'getAllExtendedCommunityLists', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extendedCommunityListsExtendedCommunityListId = 555;
    const extendedCommunityListsUpdateExtendedCommunityListBodyParam = {
      id: 9,
      name: 'string',
      description: 'string',
      values: [
        'string'
      ],
      lastConfigUpdatedAt: 10
    };
    describe('#updateExtendedCommunityList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateExtendedCommunityList(extendedCommunityListsTenantNetworkId, extendedCommunityListsExtendedCommunityListId, extendedCommunityListsUpdateExtendedCommunityListBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExtendedCommunityLists', 'updateExtendedCommunityList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtendedCommunityList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getExtendedCommunityList(extendedCommunityListsTenantNetworkId, extendedCommunityListsExtendedCommunityListId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExtendedCommunityLists', 'getExtendedCommunityList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSession - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSession((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'deleteSession', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSegment - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSegment(segmentsTenantNetworkId, segmentsSegmentId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Segments', 'deleteSegment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletegroups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletegroups(groupsTenantNetworkId, groupsGroupId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'deletegroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsersByIdUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUsersByIdUsingDELETE(null, usersUserId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'deleteUsersByIdUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUserGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUserGroup(userGroupsUsergroupId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserGroups', 'deleteUserGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUserFromUserGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUserFromUserGroup(userGroupsUsergroupId, userGroupsUserId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserGroups', 'deleteUserFromUserGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRoleByIdUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRoleByIdUsingDELETE(rolesRoleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'deleteRoleByIdUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIdentityProviderUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIdentityProviderUsingDELETE(identityProvidersIdentityProviderId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProviders', 'deleteIdentityProviderUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAWSVPCConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAWSVPCConnector(aWSVPCConnectorsTenantNetworkId, aWSVPCConnectorsAwsvpcconnectorId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AWSVPCConnectors', 'deleteAWSVPCConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAzureVNETConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAzureVNETConnector(azureVNETConnectorsTenantNetworkId, azureVNETConnectorsAzurevnetconnectorId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AzureVNETConnectors', 'deleteAzureVNETConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGCPConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteGCPConnector(gCPConnectorsTenantNetworkId, gCPConnectorsGcpconnectorId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GCPConnectors', 'deleteGCPConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOCIVCNConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOCIVCNConnector(oCIVCNConnectorsTenantNetworkId, oCIVCNConnectorsOCIVCNConnectorId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OCIVCNConnectors', 'deleteOCIVCNConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIPSecConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIPSecConnector(iPSecConnectorsTenantNetworkId, iPSecConnectorsIpsecconnectorId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPSecConnectors', 'deleteIPSecConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCiscoSDWANIngress - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCiscoSDWANIngress(ciscoSDWANConnectorsTenantNetworkId, ciscoSDWANConnectorsCiscosdwaningressId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CiscoSDWANConnectors', 'deleteCiscoSDWANIngress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInternetConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteInternetConnector(internetConnectorsTenantNetworkId, internetConnectorsInternetconnectorId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InternetConnectors', 'deleteInternetConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDirectConnectConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDirectConnectConnector(directConnectConnectorsTenantNetworkId, directConnectConnectorsDirectconnectconnectorId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DirectConnectConnectors', 'deleteDirectConnectConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAzureErConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAzureErConnector(azureErConnectorsTenantNetworkId, azureErConnectorsAzureErConnectorId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AzureErConnectors', 'deleteAzureErConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTenantNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTenantNetwork(tenantNetworksTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TenantNetworks', 'deleteTenantNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInternetApplication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteInternetApplication(internetApplicationsTenantNetworkId, internetApplicationsInternetapplicationId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InternetApplications', 'deleteInternetApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePANFWService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePANFWService(pANFWServicesTenantNetworkId, pANFWServicesServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PANFWServices', 'deletePANFWService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePANAutoscaleOptionsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePANAutoscaleOptionsUsingDELETE(pANFWServicesServiceId, pANFWServicesTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PANFWServices', 'deletePANAutoscaleOptionsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const remoteAccessConnectorsConnectorTemplateId = 555;
    describe('#deleteAllRemoteAccessSessions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAllRemoteAccessSessions(remoteAccessConnectorsConnectorTemplateId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RemoteAccessConnectors', 'deleteAllRemoteAccessSessions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const remoteAccessConnectorsSessionId = 555;
    describe('#deleteRemoteAccessSession - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRemoteAccessSession(remoteAccessConnectorsConnectorTemplateId, remoteAccessConnectorsSessionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RemoteAccessConnectors', 'deleteRemoteAccessSession', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteConnectorTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteConnectorTemplate(remoteAccessConnectorTemplatesConnectorTemplateId, remoteAccessConnectorTemplatesTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RemoteAccessConnectorTemplates', 'deleteConnectorTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIntegrations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteIntegrations(integrationId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Integration', 'deleteIntegrations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIntegration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteIntegration(integrationId, integrationIntegrationId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Integration', 'deleteIntegration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteallCloudProviderAccounts - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteallCloudProviderAccounts((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudProviderAccount', 'deleteallCloudProviderAccounts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCloudProviderAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCloudProviderAccount(cloudProviderAccountCloudProviderAccountId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudProviderAccount', 'deleteCloudProviderAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteallCloudProviderObjectSyncRequests - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteallCloudProviderObjectSyncRequests((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudProviderObjectSyncRequest', 'deleteallCloudProviderObjectSyncRequests', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteallCloudInsights - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteallCloudInsights((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudInsight', 'deleteallCloudInsights', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteallCloudInsightRefreshRequests - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteallCloudInsightRefreshRequests((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudInsightRefreshRequest', 'deleteallCloudInsightRefreshRequests', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteallCloudInsightReportConfigurations - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteallCloudInsightReportConfigurations((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudInsightReportConfiguration', 'deleteallCloudInsightReportConfigurations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteallCloudInsightReports - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteallCloudInsightReports(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudInsightReport', 'deleteallCloudInsightReports', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCloudInsightReport - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCloudInsightReport(cloudInsightReportCloudInsightReportId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudInsightReport', 'deleteCloudInsightReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBillingTag - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBillingTag(billingTagsBillingTagId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BillingTags', 'deleteBillingTag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteArubaEdgeConnectConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteArubaEdgeConnectConnector(arubaEdgeConnectConnectorsTenantNetworkId, arubaEdgeConnectConnectorsArubaEdgeconnectConnectorId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ArubaEdgeConnectConnectors', 'deleteArubaEdgeConnectConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAkamaiProlexicConnector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAkamaiProlexicConnector(akamaiProlexicConnectorsTenantNetworkId, akamaiProlexicConnectorsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AkamaiProlexicConnectors', 'deleteAkamaiProlexicConnector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNATPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNATPolicy(nATPoliciesTenantNetworkId, nATPoliciesNATPolicyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NATPolicies', 'deleteNATPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNATRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNATRule(nATRulesTenantNetworkId, nATRulesNATRuleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NATRules', 'deleteNATRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGlobalCidrList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteGlobalCidrList(globalCidrListsTenantNetworkId, globalCidrListsGlobalCidrListId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GlobalCidrLists', 'deleteGlobalCidrList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSegmentResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSegmentResource(segmentResourcesTenantNetworkId, segmentResourcesSegmentResourceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SegmentResources', 'deleteSegmentResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSegmentResourceShare - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSegmentResourceShare(segmentResourceSharesTenantNetworkId, segmentResourceSharesSegmentResourceShareId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SegmentResourceShares', 'deleteSegmentResourceShare', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCheckPointFWService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCheckPointFWService(checkPointFWServicesTenantNetworkId, checkPointFWServicesServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CheckPointFWServices', 'deleteCheckPointFWService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFortinetFWService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteFortinetFWService(fortinetFWServicesTenantNetworkId, fortinetFWServicesServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FortinetFWServices', 'deleteFortinetFWService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCiscoFTDvFWService - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteCiscoFTDvFWService(ciscoFTDvFWServicesTenantNetworkId, ciscoFTDvFWServicesServiceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CiscoFTDvFWServices', 'deleteCiscoFTDvFWService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteZscalerInternetAccessService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteZscalerInternetAccessService(zscalerInternetAccessServicesTenantNetworkId, zscalerInternetAccessServicesServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ZscalerInternetAccessServices', 'deleteZscalerInternetAccessService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInfoBloxService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteInfoBloxService(infoBloxServicesTenantNetworkId, infoBloxServicesServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InfoBloxServices', 'deleteInfoBloxService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAllBYOIP - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAllBYOIP(bYOIPTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BYOIP', 'deleteAllBYOIP', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBYOIP - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBYOIP(bYOIPTenantNetworkId, bYOIPByoipId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BYOIP', 'deleteBYOIP', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAwsVpcCredentialsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAwsVpcCredentialsUsingDELETE(aWSVPCCredentialsTenantNetworkId, aWSVPCCredentialsCredentialId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AWSVPCCredentials', 'deleteAwsVpcCredentialsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAzureCredentialsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAzureCredentialsUsingDELETE(azureCredentialsTenantNetworkId, azureCredentialsCredentialId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AzureCredentials', 'deleteAzureCredentialsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePOCIVCNCredentialsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePOCIVCNCredentialsUsingDELETE(oCIVCNCredentialsTenantNetworkId, oCIVCNCredentialsCredentialId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OCIVCNCredentials', 'deletePOCIVCNCredentialsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCiscosdwanCredentialsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCiscosdwanCredentialsUsingDELETE(ciscoSDWANInstanceCredentialsTenantNetworkId, ciscoSDWANInstanceCredentialsCredentialId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CiscoSDWANInstanceCredentials', 'deleteCiscosdwanCredentialsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGcpvpcCredentialsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteGcpvpcCredentialsUsingDELETE(gCPCredentialsTenantNetworkId, gCPCredentialsCredentialId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GCPCredentials', 'deleteGcpvpcCredentialsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLDAPCredentialsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteLDAPCredentialsUsingDELETE(lDAPCredentialsTenantNetworkId, lDAPCredentialsCredentialId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAPCredentials', 'deleteLDAPCredentialsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePanCredentialsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePanCredentialsUsingDELETE(pANCredentialsTenantNetworkId, pANCredentialsCredentialId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PANCredentials', 'deletePanCredentialsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePanInstanceCredentialsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePanInstanceCredentialsUsingDELETE(pANInstanceCredentialsTenantNetworkId, pANInstanceCredentialsCredentialId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PANInstanceCredentials', 'deletePanInstanceCredentialsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePanMasterKeyCredentialsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePanMasterKeyCredentialsUsingDELETE(pANMasterKeyCredentialsTenantNetworkId, pANMasterKeyCredentialsCredentialId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PANMasterKeyCredentials', 'deletePanMasterKeyCredentialsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePanRegistrationCredentialsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePanRegistrationCredentialsUsingDELETE(pANRegistrationCredentialsTenantNetworkId, pANRegistrationCredentialsCredentialId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PANRegistrationCredentials', 'deletePanRegistrationCredentialsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const keyPairCredentialsCredentialId = 'fakedata';
    describe('#deleteKeyPairCredentialsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteKeyPairCredentialsUsingDELETE(keyPairCredentialsTenantNetworkId, keyPairCredentialsCredentialId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('KeyPairCredentials', 'deleteKeyPairCredentialsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const arubaEdgeConnectCredentialsCredentialId = 'fakedata';
    describe('#deleteArubaEdgeConnectCredentialsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteArubaEdgeConnectCredentialsUsingDELETE(arubaEdgeConnectCredentialsTenantNetworkId, arubaEdgeConnectCredentialsCredentialId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ArubaEdgeConnectCredentials', 'deleteArubaEdgeConnectCredentialsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const akamaiProlexicCredentialsCredentialId = 'fakedata';
    describe('#deleteAkamaiProlexicCredentialsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAkamaiProlexicCredentialsUsingDELETE(akamaiProlexicCredentialsTenantNetworkId, akamaiProlexicCredentialsCredentialId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AkamaiProlexicCredentials', 'deleteAkamaiProlexicCredentialsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePCheckPointFWServiceCredentialsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePCheckPointFWServiceCredentialsUsingDELETE(checkPointFWServiceCredentialsTenantNetworkId, checkPointFWServiceCredentialsCredentialId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CheckPointFWServiceCredentials', 'deletePCheckPointFWServiceCredentialsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePCheckPointFWServiceInstanceCredentialsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePCheckPointFWServiceInstanceCredentialsUsingDELETE(checkPointFWServiceInstanceCredentialsTenantNetworkId, checkPointFWServiceInstanceCredentialsCredentialId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CheckPointFWServiceInstanceCredentials', 'deletePCheckPointFWServiceInstanceCredentialsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePCheckPointFWManagementServerCredentialsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePCheckPointFWManagementServerCredentialsUsingDELETE(checkPointFWManagementServerCredentialsTenantNetworkId, checkPointFWManagementServerCredentialsCredentialId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CheckPointFWManagementServerCredentials', 'deletePCheckPointFWManagementServerCredentialsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsernamePasswordCredentialsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUsernamePasswordCredentialsUsingDELETE(usernamePasswordCredentialsTenantNetworkId, usernamePasswordCredentialsCredentialId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UsernamePasswordCredentials', 'deleteUsernamePasswordCredentialsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSplunkHECTokenCredentialsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSplunkHECTokenCredentialsUsingDELETE(splunkHECTokenCredentialsTenantNetworkId, splunkHECTokenCredentialsCredentialId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SplunkHECTokenCredentials', 'deleteSplunkHECTokenCredentialsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFtntFWServiceCredentialsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteFtntFWServiceCredentialsUsingDELETE(fortinetFWServiceCredentialsTenantNetworkId, fortinetFWServiceCredentialsCredentialId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FortinetFWServiceCredentials', 'deleteFtntFWServiceCredentialsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFtntFWServiceInstanceCredentialsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteFtntFWServiceInstanceCredentialsUsingDELETE(fortinetFWServiceInstanceCredentialsTenantNetworkId, fortinetFWServiceInstanceCredentialsCredentialId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FortinetFWServiceInstanceCredentials', 'deleteFtntFWServiceInstanceCredentialsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCiscoFTDvFWCredentialsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCiscoFTDvFWCredentialsUsingDELETE(ciscoFTDvFWCredentialsTenantNetworkId, ciscoFTDvFWCredentialsCredentialId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CiscoFTDvFWCredentials', 'deleteCiscoFTDvFWCredentialsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCiscoFTDvFWInstanceCredentialsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCiscoFTDvFWInstanceCredentialsUsingDELETE(ciscoFTDvFWInstanceCredentialsTenantNetworkId, ciscoFTDvFWInstanceCredentialsCredentialId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CiscoFTDvFWInstanceCredentials', 'deleteCiscoFTDvFWInstanceCredentialsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteValidOneTimePasswords - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteValidOneTimePasswords((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OneTimePassword', 'deleteValidOneTimePasswords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOneTimePasswordById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOneTimePasswordById(oneTimePasswordOtpId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OneTimePassword', 'deleteOneTimePasswordById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLoginSpecification - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteLoginSpecification((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoginSpecification', 'deleteLoginSpecification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePasswordSpecification - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePasswordSpecification((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PasswordSpecification', 'deletePasswordSpecification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAllRoutePolicies - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAllRoutePolicies(routePoliciesTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutePolicies', 'deleteAllRoutePolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRoutePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRoutePolicy(routePoliciesTenantNetworkId, routePoliciesRoutePolicyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutePolicies', 'deleteRoutePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePolicy(policiesTenantNetworkId, policiesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'deletePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRule(rulesTenantNetworkId, rulesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rules', 'deleteRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRulelist - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRulelist(rulelistsTenantNetworkId, rulelistsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rulelists', 'deleteRulelist', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePrefixlist - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePrefixlist(prefixlistsTenantNetworkId, prefixlistsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Prefixlists', 'deletePrefixlist', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAllAsPathLists - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAllAsPathLists(aSPathListsTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ASPathLists', 'deleteAllAsPathLists', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAsPathList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAsPathList(aSPathListsTenantNetworkId, aSPathListsAsPathListId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ASPathLists', 'deleteAsPathList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAllCommunityLists - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAllCommunityLists(communityListsTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CommunityLists', 'deleteAllCommunityLists', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCommunityList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCommunityList(communityListsTenantNetworkId, communityListsCommunityListId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CommunityLists', 'deleteCommunityList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAllExtendedCommunityLists - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAllExtendedCommunityLists(extendedCommunityListsTenantNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExtendedCommunityLists', 'deleteAllExtendedCommunityLists', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtendedCommunityList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteExtendedCommunityList(extendedCommunityListsTenantNetworkId, extendedCommunityListsExtendedCommunityListId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-alkira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExtendedCommunityLists', 'deleteExtendedCommunityList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
