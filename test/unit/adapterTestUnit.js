/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-alkira',
      type: 'Alkira',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const Alkira = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Alkira Adapter Test', () => {
  describe('Alkira Class Tests', () => {
    const a = new Alkira(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('alkira'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('alkira'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('Alkira', pronghornDotJson.export);
          assert.equal('Alkira', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-alkira', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('alkira'));
          assert.equal('Alkira', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-alkira', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-alkira', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#loginUsersUsingPOST - errors', () => {
      it('should have a loginUsersUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.loginUsersUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.loginUsersUsingPOST(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-loginUsersUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#logoutUsersUsingPOST - errors', () => {
      it('should have a logoutUsersUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.logoutUsersUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.logoutUsersUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-logoutUsersUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLogin - errors', () => {
      it('should have a postLogin function', (done) => {
        try {
          assert.equal(true, typeof a.postLogin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postLogin(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-postLogin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#oauthLogin - errors', () => {
      it('should have a oauthLogin function', (done) => {
        try {
          assert.equal(true, typeof a.oauthLogin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.oauthLogin(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-oauthLogin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogout - errors', () => {
      it('should have a getLogout function', (done) => {
        try {
          assert.equal(true, typeof a.getLogout === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSession - errors', () => {
      it('should have a createSession function', (done) => {
        try {
          assert.equal(true, typeof a.createSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSession(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createSession', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSession - errors', () => {
      it('should have a deleteSession function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#otpLogin - errors', () => {
      it('should have a otpLogin function', (done) => {
        try {
          assert.equal(true, typeof a.otpLogin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing otp', (done) => {
        try {
          a.otpLogin(null, (data, error) => {
            try {
              const displayE = 'otp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-otpLogin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getsegments - errors', () => {
      it('should have a getsegments function', (done) => {
        try {
          assert.equal(true, typeof a.getsegments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getsegments(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getsegments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSegment - errors', () => {
      it('should have a createSegment function', (done) => {
        try {
          assert.equal(true, typeof a.createSegment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createSegment(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createSegment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSegment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createSegment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSegment - errors', () => {
      it('should have a getSegment function', (done) => {
        try {
          assert.equal(true, typeof a.getSegment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getSegment(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getSegment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing segmentId', (done) => {
        try {
          a.getSegment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'segmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getSegment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSegment - errors', () => {
      it('should have a updateSegment function', (done) => {
        try {
          assert.equal(true, typeof a.updateSegment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateSegment(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateSegment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing segmentId', (done) => {
        try {
          a.updateSegment('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'segmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateSegment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSegment('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateSegment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSegment - errors', () => {
      it('should have a deleteSegment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSegment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteSegment(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteSegment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing segmentId', (done) => {
        try {
          a.deleteSegment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'segmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteSegment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgroups - errors', () => {
      it('should have a getgroups function', (done) => {
        try {
          assert.equal(true, typeof a.getgroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getgroups(null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getgroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#creategroups - errors', () => {
      it('should have a creategroups function', (done) => {
        try {
          assert.equal(true, typeof a.creategroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.creategroups(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-creategroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.creategroups('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-creategroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgroup - errors', () => {
      it('should have a getgroup function', (done) => {
        try {
          assert.equal(true, typeof a.getgroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getgroup(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getgroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.getgroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getgroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updategroups - errors', () => {
      it('should have a updategroups function', (done) => {
        try {
          assert.equal(true, typeof a.updategroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updategroups(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updategroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.updategroups('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updategroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updategroups('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updategroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletegroups - errors', () => {
      it('should have a deletegroups function', (done) => {
        try {
          assert.equal(true, typeof a.deletegroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deletegroups(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deletegroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.deletegroups('fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deletegroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllUsersUsingGET - errors', () => {
      it('should have a getAllUsersUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getAllUsersUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createUsersUsingPOST - errors', () => {
      it('should have a createUsersUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.createUsersUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersByIdUsingGET - errors', () => {
      it('should have a getUsersByIdUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getUsersByIdUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.getUsersByIdUsingGET('fakeparam', null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getUsersByIdUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateUsersByIdUsingPUT - errors', () => {
      it('should have a updateUsersByIdUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updateUsersByIdUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.updateUsersByIdUsingPUT(null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateUsersByIdUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsersByIdUsingDELETE - errors', () => {
      it('should have a deleteUsersByIdUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUsersByIdUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.deleteUsersByIdUsingDELETE('fakeparam', null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteUsersByIdUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserProfileUsingGET - errors', () => {
      it('should have a getUserProfileUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getUserProfileUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateUserProfileUsingPUT - errors', () => {
      it('should have a updateUserProfileUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updateUserProfileUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateUserProfileUsingPUT('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateUserProfileUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getusergroups - errors', () => {
      it('should have a getusergroups function', (done) => {
        try {
          assert.equal(true, typeof a.getusergroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createUserGroup - errors', () => {
      it('should have a createUserGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createUserGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createUserGroup(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createUserGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserGroup - errors', () => {
      it('should have a getUserGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getUserGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing usergroupId', (done) => {
        try {
          a.getUserGroup(null, (data, error) => {
            try {
              const displayE = 'usergroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getUserGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateUserGroup - errors', () => {
      it('should have a updateUserGroup function', (done) => {
        try {
          assert.equal(true, typeof a.updateUserGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing usergroupId', (done) => {
        try {
          a.updateUserGroup(null, null, (data, error) => {
            try {
              const displayE = 'usergroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateUserGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateUserGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateUserGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUserGroup - errors', () => {
      it('should have a deleteUserGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUserGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing usergroupId', (done) => {
        try {
          a.deleteUserGroup(null, (data, error) => {
            try {
              const displayE = 'usergroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteUserGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersInUserGroup - errors', () => {
      it('should have a getUsersInUserGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getUsersInUserGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing usergroupId', (done) => {
        try {
          a.getUsersInUserGroup(null, (data, error) => {
            try {
              const displayE = 'usergroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getUsersInUserGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addUsersToUserGroup - errors', () => {
      it('should have a addUsersToUserGroup function', (done) => {
        try {
          assert.equal(true, typeof a.addUsersToUserGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing usergroupId', (done) => {
        try {
          a.addUsersToUserGroup(null, null, (data, error) => {
            try {
              const displayE = 'usergroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addUsersToUserGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addUsersToUserGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addUsersToUserGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceUsersInUserGroup - errors', () => {
      it('should have a replaceUsersInUserGroup function', (done) => {
        try {
          assert.equal(true, typeof a.replaceUsersInUserGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing usergroupId', (done) => {
        try {
          a.replaceUsersInUserGroup(null, null, (data, error) => {
            try {
              const displayE = 'usergroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-replaceUsersInUserGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceUsersInUserGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-replaceUsersInUserGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserFromUserGroup - errors', () => {
      it('should have a getUserFromUserGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getUserFromUserGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing usergroupId', (done) => {
        try {
          a.getUserFromUserGroup(null, null, (data, error) => {
            try {
              const displayE = 'usergroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getUserFromUserGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.getUserFromUserGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getUserFromUserGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUserFromUserGroup - errors', () => {
      it('should have a deleteUserFromUserGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUserFromUserGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing usergroupId', (done) => {
        try {
          a.deleteUserFromUserGroup(null, null, (data, error) => {
            try {
              const displayE = 'usergroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteUserFromUserGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.deleteUserFromUserGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteUserFromUserGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRolesUsingGET - errors', () => {
      it('should have a getRolesUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getRolesUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRoleUsingPOST - errors', () => {
      it('should have a createRoleUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.createRoleUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoleByIdUsingGET - errors', () => {
      it('should have a getRoleByIdUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getRoleByIdUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.getRoleByIdUsingGET(null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getRoleByIdUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRoleUsingPUT1 - errors', () => {
      it('should have a updateRoleUsingPUT1 function', (done) => {
        try {
          assert.equal(true, typeof a.updateRoleUsingPUT1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.updateRoleUsingPUT1(null, null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateRoleUsingPUT1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRoleByIdUsingDELETE - errors', () => {
      it('should have a deleteRoleByIdUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRoleByIdUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.deleteRoleByIdUsingDELETE(null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteRoleByIdUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionsUsingGET - errors', () => {
      it('should have a getPermissionsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getPermissionsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityProvidersUsingGET - errors', () => {
      it('should have a getIdentityProvidersUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityProvidersUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addIdentityProviderUsingPOST - errors', () => {
      it('should have a addIdentityProviderUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addIdentityProviderUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityProviderUsingGET - errors', () => {
      it('should have a getIdentityProviderUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityProviderUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing identityProviderId', (done) => {
        try {
          a.getIdentityProviderUsingGET(null, (data, error) => {
            try {
              const displayE = 'identityProviderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getIdentityProviderUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIdentityProviderUsingPUT - errors', () => {
      it('should have a updateIdentityProviderUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updateIdentityProviderUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing identityProviderId', (done) => {
        try {
          a.updateIdentityProviderUsingPUT(null, null, (data, error) => {
            try {
              const displayE = 'identityProviderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateIdentityProviderUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIdentityProviderUsingDELETE - errors', () => {
      it('should have a deleteIdentityProviderUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIdentityProviderUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing identityProviderId', (done) => {
        try {
          a.deleteIdentityProviderUsingDELETE(null, (data, error) => {
            try {
              const displayE = 'identityProviderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteIdentityProviderUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getconnectors - errors', () => {
      it('should have a getconnectors function', (done) => {
        try {
          assert.equal(true, typeof a.getconnectors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getconnectors(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getconnectors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConnector - errors', () => {
      it('should have a getConnector function', (done) => {
        try {
          assert.equal(true, typeof a.getConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorId', (done) => {
        try {
          a.getConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getawsvpcconnectors - errors', () => {
      it('should have a getawsvpcconnectors function', (done) => {
        try {
          assert.equal(true, typeof a.getawsvpcconnectors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getawsvpcconnectors(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getawsvpcconnectors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAWSVPCConnector - errors', () => {
      it('should have a createAWSVPCConnector function', (done) => {
        try {
          assert.equal(true, typeof a.createAWSVPCConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createAWSVPCConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createAWSVPCConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAWSVPCConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createAWSVPCConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAWSVPCConnector - errors', () => {
      it('should have a getAWSVPCConnector function', (done) => {
        try {
          assert.equal(true, typeof a.getAWSVPCConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getAWSVPCConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getAWSVPCConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing awsvpcconnectorId', (done) => {
        try {
          a.getAWSVPCConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'awsvpcconnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getAWSVPCConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAWSVPCConnector - errors', () => {
      it('should have a updateAWSVPCConnector function', (done) => {
        try {
          assert.equal(true, typeof a.updateAWSVPCConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateAWSVPCConnector(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateAWSVPCConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing awsvpcconnectorId', (done) => {
        try {
          a.updateAWSVPCConnector('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'awsvpcconnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateAWSVPCConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAWSVPCConnector('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateAWSVPCConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAWSVPCConnector - errors', () => {
      it('should have a deleteAWSVPCConnector function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAWSVPCConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteAWSVPCConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteAWSVPCConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing awsvpcconnectorId', (done) => {
        try {
          a.deleteAWSVPCConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'awsvpcconnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteAWSVPCConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getazurevnetconnectors - errors', () => {
      it('should have a getazurevnetconnectors function', (done) => {
        try {
          assert.equal(true, typeof a.getazurevnetconnectors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getazurevnetconnectors(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getazurevnetconnectors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAzureVNETConnector - errors', () => {
      it('should have a createAzureVNETConnector function', (done) => {
        try {
          assert.equal(true, typeof a.createAzureVNETConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createAzureVNETConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createAzureVNETConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAzureVNETConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createAzureVNETConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAzureVNETConnector - errors', () => {
      it('should have a getAzureVNETConnector function', (done) => {
        try {
          assert.equal(true, typeof a.getAzureVNETConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getAzureVNETConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getAzureVNETConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing azurevnetconnectorId', (done) => {
        try {
          a.getAzureVNETConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'azurevnetconnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getAzureVNETConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAzureVNETConnector - errors', () => {
      it('should have a updateAzureVNETConnector function', (done) => {
        try {
          assert.equal(true, typeof a.updateAzureVNETConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateAzureVNETConnector(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateAzureVNETConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing azurevnetconnectorId', (done) => {
        try {
          a.updateAzureVNETConnector('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'azurevnetconnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateAzureVNETConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAzureVNETConnector('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateAzureVNETConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAzureVNETConnector - errors', () => {
      it('should have a deleteAzureVNETConnector function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAzureVNETConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteAzureVNETConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteAzureVNETConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing azurevnetconnectorId', (done) => {
        try {
          a.deleteAzureVNETConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'azurevnetconnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteAzureVNETConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgcpconnectors - errors', () => {
      it('should have a getgcpconnectors function', (done) => {
        try {
          assert.equal(true, typeof a.getgcpconnectors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getgcpconnectors(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getgcpconnectors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createGCPConnector - errors', () => {
      it('should have a createGCPConnector function', (done) => {
        try {
          assert.equal(true, typeof a.createGCPConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createGCPConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createGCPConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createGCPConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createGCPConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGCPConnector - errors', () => {
      it('should have a getGCPConnector function', (done) => {
        try {
          assert.equal(true, typeof a.getGCPConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getGCPConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getGCPConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gcpconnectorId', (done) => {
        try {
          a.getGCPConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'gcpconnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getGCPConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateGCPConnector - errors', () => {
      it('should have a updateGCPConnector function', (done) => {
        try {
          assert.equal(true, typeof a.updateGCPConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateGCPConnector(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateGCPConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gcpconnectorId', (done) => {
        try {
          a.updateGCPConnector('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'gcpconnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateGCPConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateGCPConnector('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateGCPConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGCPConnector - errors', () => {
      it('should have a deleteGCPConnector function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGCPConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteGCPConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteGCPConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gcpconnectorId', (done) => {
        try {
          a.deleteGCPConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'gcpconnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteGCPConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOCIVCNConnectors - errors', () => {
      it('should have a getOCIVCNConnectors function', (done) => {
        try {
          assert.equal(true, typeof a.getOCIVCNConnectors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getOCIVCNConnectors(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getOCIVCNConnectors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOCIVCNConnector - errors', () => {
      it('should have a createOCIVCNConnector function', (done) => {
        try {
          assert.equal(true, typeof a.createOCIVCNConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createOCIVCNConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createOCIVCNConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createOCIVCNConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createOCIVCNConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOCIVCNConnector - errors', () => {
      it('should have a getOCIVCNConnector function', (done) => {
        try {
          assert.equal(true, typeof a.getOCIVCNConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getOCIVCNConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getOCIVCNConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing oCIVCNConnectorId', (done) => {
        try {
          a.getOCIVCNConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'oCIVCNConnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getOCIVCNConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOCIVCNConnector - errors', () => {
      it('should have a updateOCIVCNConnector function', (done) => {
        try {
          assert.equal(true, typeof a.updateOCIVCNConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateOCIVCNConnector(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateOCIVCNConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing oCIVCNConnectorId', (done) => {
        try {
          a.updateOCIVCNConnector('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'oCIVCNConnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateOCIVCNConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateOCIVCNConnector('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateOCIVCNConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOCIVCNConnector - errors', () => {
      it('should have a deleteOCIVCNConnector function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOCIVCNConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteOCIVCNConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteOCIVCNConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing oCIVCNConnectorId', (done) => {
        try {
          a.deleteOCIVCNConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'oCIVCNConnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteOCIVCNConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getipsecconnectors - errors', () => {
      it('should have a getipsecconnectors function', (done) => {
        try {
          assert.equal(true, typeof a.getipsecconnectors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getipsecconnectors(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getipsecconnectors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIPSecConnector - errors', () => {
      it('should have a createIPSecConnector function', (done) => {
        try {
          assert.equal(true, typeof a.createIPSecConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createIPSecConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createIPSecConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIPSecConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createIPSecConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPSecConnector - errors', () => {
      it('should have a getIPSecConnector function', (done) => {
        try {
          assert.equal(true, typeof a.getIPSecConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getIPSecConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getIPSecConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipsecconnectorId', (done) => {
        try {
          a.getIPSecConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ipsecconnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getIPSecConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIPSecConnector - errors', () => {
      it('should have a updateIPSecConnector function', (done) => {
        try {
          assert.equal(true, typeof a.updateIPSecConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateIPSecConnector(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateIPSecConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipsecconnectorId', (done) => {
        try {
          a.updateIPSecConnector('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ipsecconnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateIPSecConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIPSecConnector('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateIPSecConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIPSecConnector - errors', () => {
      it('should have a deleteIPSecConnector function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIPSecConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteIPSecConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteIPSecConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipsecconnectorId', (done) => {
        try {
          a.deleteIPSecConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ipsecconnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteIPSecConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPSecConnectorSiteConfiguration - errors', () => {
      it('should have a getIPSecConnectorSiteConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getIPSecConnectorSiteConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getIPSecConnectorSiteConfiguration(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getIPSecConnectorSiteConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipsecconnectorId', (done) => {
        try {
          a.getIPSecConnectorSiteConfiguration('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ipsecconnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getIPSecConnectorSiteConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteId', (done) => {
        try {
          a.getIPSecConnectorSiteConfiguration('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getIPSecConnectorSiteConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getciscosdwaningresses - errors', () => {
      it('should have a getciscosdwaningresses function', (done) => {
        try {
          assert.equal(true, typeof a.getciscosdwaningresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getciscosdwaningresses(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getciscosdwaningresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCiscoSDWANIngress - errors', () => {
      it('should have a createCiscoSDWANIngress function', (done) => {
        try {
          assert.equal(true, typeof a.createCiscoSDWANIngress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createCiscoSDWANIngress(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createCiscoSDWANIngress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCiscoSDWANIngress('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createCiscoSDWANIngress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCiscoSDWANIngress - errors', () => {
      it('should have a getCiscoSDWANIngress function', (done) => {
        try {
          assert.equal(true, typeof a.getCiscoSDWANIngress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getCiscoSDWANIngress(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCiscoSDWANIngress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ciscosdwaningressId', (done) => {
        try {
          a.getCiscoSDWANIngress('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ciscosdwaningressId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCiscoSDWANIngress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCiscoSDWANIngress - errors', () => {
      it('should have a updateCiscoSDWANIngress function', (done) => {
        try {
          assert.equal(true, typeof a.updateCiscoSDWANIngress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateCiscoSDWANIngress(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCiscoSDWANIngress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ciscosdwaningressId', (done) => {
        try {
          a.updateCiscoSDWANIngress('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ciscosdwaningressId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCiscoSDWANIngress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateCiscoSDWANIngress('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCiscoSDWANIngress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCiscoSDWANIngress - errors', () => {
      it('should have a deleteCiscoSDWANIngress function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCiscoSDWANIngress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteCiscoSDWANIngress(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteCiscoSDWANIngress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ciscosdwaningressId', (done) => {
        try {
          a.deleteCiscoSDWANIngress('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ciscosdwaningressId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteCiscoSDWANIngress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCiscoSDWANConnectorInstanceConfiguration - errors', () => {
      it('should have a getCiscoSDWANConnectorInstanceConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getCiscoSDWANConnectorInstanceConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getCiscoSDWANConnectorInstanceConfiguration(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCiscoSDWANConnectorInstanceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ciscosdwaningressId', (done) => {
        try {
          a.getCiscoSDWANConnectorInstanceConfiguration('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ciscosdwaningressId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCiscoSDWANConnectorInstanceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.getCiscoSDWANConnectorInstanceConfiguration('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCiscoSDWANConnectorInstanceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getinternetconnectors - errors', () => {
      it('should have a getinternetconnectors function', (done) => {
        try {
          assert.equal(true, typeof a.getinternetconnectors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getinternetconnectors(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getinternetconnectors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createInternetConnector - errors', () => {
      it('should have a createInternetConnector function', (done) => {
        try {
          assert.equal(true, typeof a.createInternetConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createInternetConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createInternetConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createInternetConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createInternetConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInternetConnector - errors', () => {
      it('should have a getInternetConnector function', (done) => {
        try {
          assert.equal(true, typeof a.getInternetConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getInternetConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getInternetConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing internetconnectorId', (done) => {
        try {
          a.getInternetConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'internetconnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getInternetConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateInternetConnector - errors', () => {
      it('should have a updateInternetConnector function', (done) => {
        try {
          assert.equal(true, typeof a.updateInternetConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateInternetConnector(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateInternetConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing internetconnectorId', (done) => {
        try {
          a.updateInternetConnector('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'internetconnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateInternetConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateInternetConnector('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateInternetConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInternetConnector - errors', () => {
      it('should have a deleteInternetConnector function', (done) => {
        try {
          assert.equal(true, typeof a.deleteInternetConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteInternetConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteInternetConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing internetconnectorId', (done) => {
        try {
          a.deleteInternetConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'internetconnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteInternetConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInternetConnectorConfiguration - errors', () => {
      it('should have a getInternetConnectorConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getInternetConnectorConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getInternetConnectorConfiguration(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getInternetConnectorConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing internetconnectorId', (done) => {
        try {
          a.getInternetConnectorConfiguration('fakeparam', null, (data, error) => {
            try {
              const displayE = 'internetconnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getInternetConnectorConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getdirectconnectconnectors - errors', () => {
      it('should have a getdirectconnectconnectors function', (done) => {
        try {
          assert.equal(true, typeof a.getdirectconnectconnectors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getdirectconnectconnectors(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getdirectconnectconnectors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDirectConnectConnector - errors', () => {
      it('should have a createDirectConnectConnector function', (done) => {
        try {
          assert.equal(true, typeof a.createDirectConnectConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createDirectConnectConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createDirectConnectConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDirectConnectConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createDirectConnectConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDirectConnectConnector - errors', () => {
      it('should have a getDirectConnectConnector function', (done) => {
        try {
          assert.equal(true, typeof a.getDirectConnectConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getDirectConnectConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getDirectConnectConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing directconnectconnectorId', (done) => {
        try {
          a.getDirectConnectConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'directconnectconnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getDirectConnectConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDirectConnectConnector - errors', () => {
      it('should have a updateDirectConnectConnector function', (done) => {
        try {
          assert.equal(true, typeof a.updateDirectConnectConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateDirectConnectConnector(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateDirectConnectConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing directconnectconnectorId', (done) => {
        try {
          a.updateDirectConnectConnector('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'directconnectconnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateDirectConnectConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateDirectConnectConnector('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateDirectConnectConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDirectConnectConnector - errors', () => {
      it('should have a deleteDirectConnectConnector function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDirectConnectConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteDirectConnectConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteDirectConnectConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing directconnectconnectorId', (done) => {
        try {
          a.deleteDirectConnectConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'directconnectconnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteDirectConnectConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDirectConnectConnectorInstanceConfiguration - errors', () => {
      it('should have a getDirectConnectConnectorInstanceConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getDirectConnectConnectorInstanceConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getDirectConnectConnectorInstanceConfiguration(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getDirectConnectConnectorInstanceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing directconnectconnectorId', (done) => {
        try {
          a.getDirectConnectConnectorInstanceConfiguration('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'directconnectconnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getDirectConnectConnectorInstanceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.getDirectConnectConnectorInstanceConfiguration('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getDirectConnectConnectorInstanceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAzureErConnectors - errors', () => {
      it('should have a getAzureErConnectors function', (done) => {
        try {
          assert.equal(true, typeof a.getAzureErConnectors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getAzureErConnectors(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getAzureErConnectors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAzureErConnector - errors', () => {
      it('should have a createAzureErConnector function', (done) => {
        try {
          assert.equal(true, typeof a.createAzureErConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createAzureErConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createAzureErConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAzureErConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createAzureErConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAzureErConnector - errors', () => {
      it('should have a getAzureErConnector function', (done) => {
        try {
          assert.equal(true, typeof a.getAzureErConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getAzureErConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getAzureErConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing azureErConnectorId', (done) => {
        try {
          a.getAzureErConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'azureErConnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getAzureErConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAzureErConnector - errors', () => {
      it('should have a updateAzureErConnector function', (done) => {
        try {
          assert.equal(true, typeof a.updateAzureErConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateAzureErConnector(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateAzureErConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing azureErConnectorId', (done) => {
        try {
          a.updateAzureErConnector('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'azureErConnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateAzureErConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAzureErConnector('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateAzureErConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAzureErConnector - errors', () => {
      it('should have a deleteAzureErConnector function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAzureErConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteAzureErConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteAzureErConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing azureErConnectorId', (done) => {
        try {
          a.deleteAzureErConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'azureErConnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteAzureErConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAzureErConnectorInstanceConfiguration - errors', () => {
      it('should have a getAzureErConnectorInstanceConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getAzureErConnectorInstanceConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getAzureErConnectorInstanceConfiguration(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getAzureErConnectorInstanceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing azureErConnectorId', (done) => {
        try {
          a.getAzureErConnectorInstanceConfiguration('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'azureErConnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getAzureErConnectorInstanceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.getAzureErConnectorInstanceConfiguration('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getAzureErConnectorInstanceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuditLogs - errors', () => {
      it('should have a getAuditLogs function', (done) => {
        try {
          assert.equal(true, typeof a.getAuditLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuditLogTags - errors', () => {
      it('should have a getAuditLogTags function', (done) => {
        try {
          assert.equal(true, typeof a.getAuditLogTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenantNetworks - errors', () => {
      it('should have a getTenantNetworks function', (done) => {
        try {
          assert.equal(true, typeof a.getTenantNetworks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTenantNetwork - errors', () => {
      it('should have a createTenantNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.createTenantNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createTenantNetwork(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createTenantNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenantNetwork - errors', () => {
      it('should have a getTenantNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.getTenantNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getTenantNetwork(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getTenantNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTenantNetwork - errors', () => {
      it('should have a updateTenantNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.updateTenantNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateTenantNetwork(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateTenantNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTenantNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateTenantNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTenantNetwork - errors', () => {
      it('should have a deleteTenantNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTenantNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteTenantNetwork(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteTenantNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#provisionTenantNetwork - errors', () => {
      it('should have a provisionTenantNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.provisionTenantNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.provisionTenantNetwork(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-provisionTenantNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrievestrafficstatsforconnectorsinthistenant - errors', () => {
      it('should have a retrievestrafficstatsforconnectorsinthistenant function', (done) => {
        try {
          assert.equal(true, typeof a.retrievestrafficstatsforconnectorsinthistenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.retrievestrafficstatsforconnectorsinthistenant(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-retrievestrafficstatsforconnectorsinthistenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorId', (done) => {
        try {
          a.retrievestrafficstatsforconnectorsinthistenant('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'connectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-retrievestrafficstatsforconnectorsinthistenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrievestheInterCXPtraffic - errors', () => {
      it('should have a retrievestheInterCXPtraffic function', (done) => {
        try {
          assert.equal(true, typeof a.retrievestheInterCXPtraffic === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.retrievestheInterCXPtraffic(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-retrievestheInterCXPtraffic', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrievestheoveralltrafficgoingtointernetfromCXP - errors', () => {
      it('should have a retrievestheoveralltrafficgoingtointernetfromCXP function', (done) => {
        try {
          assert.equal(true, typeof a.retrievestheoveralltrafficgoingtointernetfromCXP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.retrievestheoveralltrafficgoingtointernetfromCXP(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-retrievestheoveralltrafficgoingtointernetfromCXP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrievestrafficstatsforservicesinthistenant - errors', () => {
      it('should have a retrievestrafficstatsforservicesinthistenant function', (done) => {
        try {
          assert.equal(true, typeof a.retrievestrafficstatsforservicesinthistenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.retrievestrafficstatsforservicesinthistenant(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-retrievestrafficstatsforservicesinthistenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.retrievestrafficstatsforservicesinthistenant('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-retrievestrafficstatsforservicesinthistenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrievesactivesessionscountforservicesinthistenant - errors', () => {
      it('should have a retrievesactivesessionscountforservicesinthistenant function', (done) => {
        try {
          assert.equal(true, typeof a.retrievesactivesessionscountforservicesinthistenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.retrievesactivesessionscountforservicesinthistenant(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-retrievesactivesessionscountforservicesinthistenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.retrievesactivesessionscountforservicesinthistenant('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-retrievesactivesessionscountforservicesinthistenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrievesinstancecountforservicesinthistenant - errors', () => {
      it('should have a retrievesinstancecountforservicesinthistenant function', (done) => {
        try {
          assert.equal(true, typeof a.retrievesinstancecountforservicesinthistenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.retrievesinstancecountforservicesinthistenant(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-retrievesinstancecountforservicesinthistenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.retrievesinstancecountforservicesinthistenant('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-retrievesinstancecountforservicesinthistenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getfirewallzones - errors', () => {
      it('should have a getfirewallzones function', (done) => {
        try {
          assert.equal(true, typeof a.getfirewallzones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getfirewallzones(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getfirewallzones', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFirewallZone - errors', () => {
      it('should have a getFirewallZone function', (done) => {
        try {
          assert.equal(true, typeof a.getFirewallZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getFirewallZone(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getFirewallZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing firewallzoneId', (done) => {
        try {
          a.getFirewallZone('fakeparam', null, (data, error) => {
            try {
              const displayE = 'firewallzoneId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getFirewallZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getinternetapplications - errors', () => {
      it('should have a getinternetapplications function', (done) => {
        try {
          assert.equal(true, typeof a.getinternetapplications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getinternetapplications(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getinternetapplications', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createInternetApplication - errors', () => {
      it('should have a createInternetApplication function', (done) => {
        try {
          assert.equal(true, typeof a.createInternetApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createInternetApplication(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createInternetApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createInternetApplication('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createInternetApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInternetApplication - errors', () => {
      it('should have a getInternetApplication function', (done) => {
        try {
          assert.equal(true, typeof a.getInternetApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getInternetApplication(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getInternetApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing internetapplicationId', (done) => {
        try {
          a.getInternetApplication('fakeparam', null, (data, error) => {
            try {
              const displayE = 'internetapplicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getInternetApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateInternetApplication - errors', () => {
      it('should have a updateInternetApplication function', (done) => {
        try {
          assert.equal(true, typeof a.updateInternetApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateInternetApplication(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateInternetApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing internetapplicationId', (done) => {
        try {
          a.updateInternetApplication('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'internetapplicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateInternetApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateInternetApplication('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateInternetApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInternetApplication - errors', () => {
      it('should have a deleteInternetApplication function', (done) => {
        try {
          assert.equal(true, typeof a.deleteInternetApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteInternetApplication(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteInternetApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing internetapplicationId', (done) => {
        try {
          a.deleteInternetApplication('fakeparam', null, (data, error) => {
            try {
              const displayE = 'internetapplicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteInternetApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getpanfwservices - errors', () => {
      it('should have a getpanfwservices function', (done) => {
        try {
          assert.equal(true, typeof a.getpanfwservices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getpanfwservices(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getpanfwservices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPANFWService - errors', () => {
      it('should have a createPANFWService function', (done) => {
        try {
          assert.equal(true, typeof a.createPANFWService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createPANFWService(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createPANFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createPANFWService('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createPANFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPANFWService - errors', () => {
      it('should have a getPANFWService function', (done) => {
        try {
          assert.equal(true, typeof a.getPANFWService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getPANFWService(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getPANFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.getPANFWService('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getPANFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePANFWService - errors', () => {
      it('should have a updatePANFWService function', (done) => {
        try {
          assert.equal(true, typeof a.updatePANFWService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updatePANFWService(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updatePANFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.updatePANFWService('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updatePANFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePANFWService('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updatePANFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePANFWService - errors', () => {
      it('should have a deletePANFWService function', (done) => {
        try {
          assert.equal(true, typeof a.deletePANFWService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deletePANFWService(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deletePANFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.deletePANFWService('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deletePANFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPANFWServiceInstanceConfiguration - errors', () => {
      it('should have a getPANFWServiceInstanceConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getPANFWServiceInstanceConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getPANFWServiceInstanceConfiguration(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getPANFWServiceInstanceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.getPANFWServiceInstanceConfiguration('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getPANFWServiceInstanceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.getPANFWServiceInstanceConfiguration('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getPANFWServiceInstanceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPANAutoscaleOptionsUsingGET - errors', () => {
      it('should have a getPANAutoscaleOptionsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getPANAutoscaleOptionsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.getPANAutoscaleOptionsUsingGET(null, null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getPANAutoscaleOptionsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getPANAutoscaleOptionsUsingGET('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getPANAutoscaleOptionsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePANAutoscaleOptionsUsingPUT - errors', () => {
      it('should have a updatePANAutoscaleOptionsUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updatePANAutoscaleOptionsUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.updatePANAutoscaleOptionsUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updatePANAutoscaleOptionsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updatePANAutoscaleOptionsUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updatePANAutoscaleOptionsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePANAutoscaleOptionsUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updatePANAutoscaleOptionsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePANAutoscaleOptionsUsingDELETE - errors', () => {
      it('should have a deletePANAutoscaleOptionsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deletePANAutoscaleOptionsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.deletePANAutoscaleOptionsUsingDELETE(null, null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deletePANAutoscaleOptionsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deletePANAutoscaleOptionsUsingDELETE('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deletePANAutoscaleOptionsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getservices - errors', () => {
      it('should have a getservices function', (done) => {
        try {
          assert.equal(true, typeof a.getservices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getservices(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getservices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getService - errors', () => {
      it('should have a getService function', (done) => {
        try {
          assert.equal(true, typeof a.getService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getService(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.getService('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listofallproducts - errors', () => {
      it('should have a listofallproducts function', (done) => {
        try {
          assert.equal(true, typeof a.listofallproducts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenantResourceLimits - errors', () => {
      it('should have a getTenantResourceLimits function', (done) => {
        try {
          assert.equal(true, typeof a.getTenantResourceLimits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getResourceUsage - errors', () => {
      it('should have a getResourceUsage function', (done) => {
        try {
          assert.equal(true, typeof a.getResourceUsage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCXPs - errors', () => {
      it('should have a getCXPs function', (done) => {
        try {
          assert.equal(true, typeof a.getCXPs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getbillinginformation - errors', () => {
      it('should have a getbillinginformation function', (done) => {
        try {
          assert.equal(true, typeof a.getbillinginformation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getinvoicesummaries - errors', () => {
      it('should have a getinvoicesummaries function', (done) => {
        try {
          assert.equal(true, typeof a.getinvoicesummaries === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInvoiceSummary - errors', () => {
      it('should have a getInvoiceSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getInvoiceSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing invoiceId', (done) => {
        try {
          a.getInvoiceSummary(null, (data, error) => {
            try {
              const displayE = 'invoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getInvoiceSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pingFromConnector - errors', () => {
      it('should have a pingFromConnector function', (done) => {
        try {
          assert.equal(true, typeof a.pingFromConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.pingFromConnector(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-pingFromConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorId', (done) => {
        try {
          a.pingFromConnector('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'connectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-pingFromConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.pingFromConnector('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-pingFromConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pingFromConnectorInstance - errors', () => {
      it('should have a pingFromConnectorInstance function', (done) => {
        try {
          assert.equal(true, typeof a.pingFromConnectorInstance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.pingFromConnectorInstance(null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-pingFromConnectorInstance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorId', (done) => {
        try {
          a.pingFromConnectorInstance('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'connectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-pingFromConnectorInstance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.pingFromConnectorInstance('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-pingFromConnectorInstance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.pingFromConnectorInstance('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-pingFromConnectorInstance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobs - errors', () => {
      it('should have a getJobs function', (done) => {
        try {
          assert.equal(true, typeof a.getJobs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJob - errors', () => {
      it('should have a getJob function', (done) => {
        try {
          assert.equal(true, typeof a.getJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.getJob(null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlerts - errors', () => {
      it('should have a getAlerts function', (done) => {
        try {
          assert.equal(true, typeof a.getAlerts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertTags - errors', () => {
      it('should have a getAlertTags function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resolved - errors', () => {
      it('should have a resolved function', (done) => {
        try {
          assert.equal(true, typeof a.resolved === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.resolved(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-resolved', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#traceRouteFromConnector - errors', () => {
      it('should have a traceRouteFromConnector function', (done) => {
        try {
          assert.equal(true, typeof a.traceRouteFromConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.traceRouteFromConnector(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-traceRouteFromConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorId', (done) => {
        try {
          a.traceRouteFromConnector('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'connectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-traceRouteFromConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.traceRouteFromConnector('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-traceRouteFromConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#traceRouteFromConnectorInstance - errors', () => {
      it('should have a traceRouteFromConnectorInstance function', (done) => {
        try {
          assert.equal(true, typeof a.traceRouteFromConnectorInstance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.traceRouteFromConnectorInstance(null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-traceRouteFromConnectorInstance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorId', (done) => {
        try {
          a.traceRouteFromConnectorInstance('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'connectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-traceRouteFromConnectorInstance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.traceRouteFromConnectorInstance('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-traceRouteFromConnectorInstance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.traceRouteFromConnectorInstance('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-traceRouteFromConnectorInstance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#logsFromConnector - errors', () => {
      it('should have a logsFromConnector function', (done) => {
        try {
          assert.equal(true, typeof a.logsFromConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.logsFromConnector(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-logsFromConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorId', (done) => {
        try {
          a.logsFromConnector('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'connectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-logsFromConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.logsFromConnector('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-logsFromConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#logsFromConnectorInstance - errors', () => {
      it('should have a logsFromConnectorInstance function', (done) => {
        try {
          assert.equal(true, typeof a.logsFromConnectorInstance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.logsFromConnectorInstance(null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-logsFromConnectorInstance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorId', (done) => {
        try {
          a.logsFromConnectorInstance('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'connectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-logsFromConnectorInstance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.logsFromConnectorInstance('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-logsFromConnectorInstance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.logsFromConnectorInstance('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-logsFromConnectorInstance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMessages - errors', () => {
      it('should have a getMessages function', (done) => {
        try {
          assert.equal(true, typeof a.getMessages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestId', (done) => {
        try {
          a.getMessages(null, null, (data, error) => {
            try {
              const displayE = 'requestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getMessages', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSessionCountUsingGET - errors', () => {
      it('should have a getSessionCountUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getSessionCountUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getSessionCountUsingGET(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getSessionCountUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appName', (done) => {
        try {
          a.getSessionCountUsingGET('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'appName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getSessionCountUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppNameTimeSeriesUsingGET - errors', () => {
      it('should have a getAppNameTimeSeriesUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getAppNameTimeSeriesUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getAppNameTimeSeriesUsingGET(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getAppNameTimeSeriesUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appName', (done) => {
        try {
          a.getAppNameTimeSeriesUsingGET('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'appName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getAppNameTimeSeriesUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRuleNameTimeSeriesUsingGET - errors', () => {
      it('should have a getRuleNameTimeSeriesUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getRuleNameTimeSeriesUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getRuleNameTimeSeriesUsingGET(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getRuleNameTimeSeriesUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopApplicationsUsingGET - errors', () => {
      it('should have a getTopApplicationsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getTopApplicationsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getTopApplicationsUsingGET(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getTopApplicationsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopApplicationsbyConnectorUsingGET - errors', () => {
      it('should have a getTopApplicationsbyConnectorUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getTopApplicationsbyConnectorUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getTopApplicationsbyConnectorUsingGET(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getTopApplicationsbyConnectorUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorId', (done) => {
        try {
          a.getTopApplicationsbyConnectorUsingGET('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getTopApplicationsbyConnectorUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopPolicyHitsUsingGET - errors', () => {
      it('should have a getTopPolicyHitsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getTopPolicyHitsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getTopPolicyHitsUsingGET(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getTopPolicyHitsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopPolicyHitsbyConnectorUsingGET - errors', () => {
      it('should have a getTopPolicyHitsbyConnectorUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getTopPolicyHitsbyConnectorUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getTopPolicyHitsbyConnectorUsingGET(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getTopPolicyHitsbyConnectorUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorId', (done) => {
        try {
          a.getTopPolicyHitsbyConnectorUsingGET('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getTopPolicyHitsbyConnectorUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRemoteAccessConnectors - errors', () => {
      it('should have a getRemoteAccessConnectors function', (done) => {
        try {
          assert.equal(true, typeof a.getRemoteAccessConnectors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getRemoteAccessConnectors(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getRemoteAccessConnectors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRemoteAccesssConnector - errors', () => {
      it('should have a getRemoteAccesssConnector function', (done) => {
        try {
          assert.equal(true, typeof a.getRemoteAccesssConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorId', (done) => {
        try {
          a.getRemoteAccesssConnector(null, null, (data, error) => {
            try {
              const displayE = 'connectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getRemoteAccesssConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getRemoteAccesssConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getRemoteAccesssConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRemoteAccesssConnectorConfiguration - errors', () => {
      it('should have a getRemoteAccesssConnectorConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getRemoteAccesssConnectorConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorId', (done) => {
        try {
          a.getRemoteAccesssConnectorConfiguration(null, null, null, null, (data, error) => {
            try {
              const displayE = 'connectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getRemoteAccesssConnectorConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getRemoteAccesssConnectorConfiguration('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getRemoteAccesssConnectorConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAllRemoteAccessSessions - errors', () => {
      it('should have a deleteAllRemoteAccessSessions function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAllRemoteAccessSessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorTemplateId', (done) => {
        try {
          a.deleteAllRemoteAccessSessions(null, null, (data, error) => {
            try {
              const displayE = 'connectorTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteAllRemoteAccessSessions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRemoteAccessSession - errors', () => {
      it('should have a deleteRemoteAccessSession function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRemoteAccessSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorTemplateId', (done) => {
        try {
          a.deleteRemoteAccessSession(null, null, (data, error) => {
            try {
              const displayE = 'connectorTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteRemoteAccessSession', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.deleteRemoteAccessSession('fakeparam', null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteRemoteAccessSession', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConnectorTemplates - errors', () => {
      it('should have a getConnectorTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getConnectorTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getConnectorTemplates(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getConnectorTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createConnectorTemplate - errors', () => {
      it('should have a createConnectorTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createConnectorTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createConnectorTemplate(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createConnectorTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createConnectorTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createConnectorTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConnectorTemplate - errors', () => {
      it('should have a getConnectorTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.getConnectorTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorTemplateId', (done) => {
        try {
          a.getConnectorTemplate(null, null, (data, error) => {
            try {
              const displayE = 'connectorTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getConnectorTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getConnectorTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getConnectorTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateConnectorTemplate - errors', () => {
      it('should have a updateConnectorTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.updateConnectorTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorTemplateId', (done) => {
        try {
          a.updateConnectorTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'connectorTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateConnectorTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateConnectorTemplate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateConnectorTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateConnectorTemplate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateConnectorTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteConnectorTemplate - errors', () => {
      it('should have a deleteConnectorTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteConnectorTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorTemplateId', (done) => {
        try {
          a.deleteConnectorTemplate(null, null, (data, error) => {
            try {
              const displayE = 'connectorTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteConnectorTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteConnectorTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteConnectorTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenant - errors', () => {
      it('should have a getTenant function', (done) => {
        try {
          assert.equal(true, typeof a.getTenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getTenant(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getTenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getTenant('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getTenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTenant - errors', () => {
      it('should have a updateTenant function', (done) => {
        try {
          assert.equal(true, typeof a.updateTenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.updateTenant(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateTenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateTenant('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateTenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTenant('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateTenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenantPreferences - errors', () => {
      it('should have a getTenantPreferences function', (done) => {
        try {
          assert.equal(true, typeof a.getTenantPreferences === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.getTenantPreferences(null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getTenantPreferences', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTenantPreferences - errors', () => {
      it('should have a updateTenantPreferences function', (done) => {
        try {
          assert.equal(true, typeof a.updateTenantPreferences === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantId', (done) => {
        try {
          a.updateTenantPreferences(null, null, (data, error) => {
            try {
              const displayE = 'tenantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateTenantPreferences', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTenantPreferences('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateTenantPreferences', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntegrationsByTypeAndSubType - errors', () => {
      it('should have a getIntegrationsByTypeAndSubType function', (done) => {
        try {
          assert.equal(true, typeof a.getIntegrationsByTypeAndSubType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIntegrationsByTypeAndSubType(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getIntegrationsByTypeAndSubType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subType', (done) => {
        try {
          a.getIntegrationsByTypeAndSubType('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'subType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getIntegrationsByTypeAndSubType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getIntegrationsByTypeAndSubType('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getIntegrationsByTypeAndSubType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntegrations - errors', () => {
      it('should have a getIntegrations function', (done) => {
        try {
          assert.equal(true, typeof a.getIntegrations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIntegrations(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getIntegrations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIntegration - errors', () => {
      it('should have a createIntegration function', (done) => {
        try {
          assert.equal(true, typeof a.createIntegration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.createIntegration(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createIntegration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIntegration('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createIntegration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIntegrations - errors', () => {
      it('should have a deleteIntegrations function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIntegrations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIntegrations(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteIntegrations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIntegration - errors', () => {
      it('should have a updateIntegration function', (done) => {
        try {
          assert.equal(true, typeof a.updateIntegration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateIntegration(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateIntegration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing integrationId', (done) => {
        try {
          a.updateIntegration('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'integrationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateIntegration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIntegration('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateIntegration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIntegration - errors', () => {
      it('should have a deleteIntegration function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIntegration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIntegration(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteIntegration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing integrationId', (done) => {
        try {
          a.deleteIntegration('fakeparam', null, (data, error) => {
            try {
              const displayE = 'integrationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteIntegration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudProviderAccounts - errors', () => {
      it('should have a getCloudProviderAccounts function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudProviderAccounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCloudProviderAccount - errors', () => {
      it('should have a createCloudProviderAccount function', (done) => {
        try {
          assert.equal(true, typeof a.createCloudProviderAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCloudProviderAccount(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createCloudProviderAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteallCloudProviderAccounts - errors', () => {
      it('should have a deleteallCloudProviderAccounts function', (done) => {
        try {
          assert.equal(true, typeof a.deleteallCloudProviderAccounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudProviderAccount - errors', () => {
      it('should have a getCloudProviderAccount function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudProviderAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cloudProviderAccountId', (done) => {
        try {
          a.getCloudProviderAccount(null, (data, error) => {
            try {
              const displayE = 'cloudProviderAccountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCloudProviderAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCloudProviderAccount - errors', () => {
      it('should have a updateCloudProviderAccount function', (done) => {
        try {
          assert.equal(true, typeof a.updateCloudProviderAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cloudProviderAccountId', (done) => {
        try {
          a.updateCloudProviderAccount(null, null, (data, error) => {
            try {
              const displayE = 'cloudProviderAccountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCloudProviderAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateCloudProviderAccount('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCloudProviderAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCloudProviderAccount - errors', () => {
      it('should have a deleteCloudProviderAccount function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCloudProviderAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cloudProviderAccountId', (done) => {
        try {
          a.deleteCloudProviderAccount(null, (data, error) => {
            try {
              const displayE = 'cloudProviderAccountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteCloudProviderAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudProviderObjects - errors', () => {
      it('should have a getCloudProviderObjects function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudProviderObjects === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudProviderObject - errors', () => {
      it('should have a getCloudProviderObject function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudProviderObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cloudProviderObjectId', (done) => {
        try {
          a.getCloudProviderObject(null, (data, error) => {
            try {
              const displayE = 'cloudProviderObjectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCloudProviderObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudProviderObjectTags - errors', () => {
      it('should have a getCloudProviderObjectTags function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudProviderObjectTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cloudProvider', (done) => {
        try {
          a.getCloudProviderObjectTags(null, null, (data, error) => {
            try {
              const displayE = 'cloudProvider is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCloudProviderObjectTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cloudProviderObjectType', (done) => {
        try {
          a.getCloudProviderObjectTags('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cloudProviderObjectType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCloudProviderObjectTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudProviderObjectCounts - errors', () => {
      it('should have a getCloudProviderObjectCounts function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudProviderObjectCounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudProviderObjectCountByTimes - errors', () => {
      it('should have a getCloudProviderObjectCountByTimes function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudProviderObjectCountByTimes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudProviderObjectSyncRequests - errors', () => {
      it('should have a getCloudProviderObjectSyncRequests function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudProviderObjectSyncRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCloudProviderObjectSyncRequest - errors', () => {
      it('should have a createCloudProviderObjectSyncRequest function', (done) => {
        try {
          assert.equal(true, typeof a.createCloudProviderObjectSyncRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCloudProviderObjectSyncRequest(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createCloudProviderObjectSyncRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteallCloudProviderObjectSyncRequests - errors', () => {
      it('should have a deleteallCloudProviderObjectSyncRequests function', (done) => {
        try {
          assert.equal(true, typeof a.deleteallCloudProviderObjectSyncRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudProviderObjectSyncRequest - errors', () => {
      it('should have a getCloudProviderObjectSyncRequest function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudProviderObjectSyncRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cloudProviderObjectSyncRequestId', (done) => {
        try {
          a.getCloudProviderObjectSyncRequest(null, (data, error) => {
            try {
              const displayE = 'cloudProviderObjectSyncRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCloudProviderObjectSyncRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudInsights - errors', () => {
      it('should have a getCloudInsights function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudInsights === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteallCloudInsights - errors', () => {
      it('should have a deleteallCloudInsights function', (done) => {
        try {
          assert.equal(true, typeof a.deleteallCloudInsights === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudInsight - errors', () => {
      it('should have a getCloudInsight function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudInsight === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cloudInsightId', (done) => {
        try {
          a.getCloudInsight(null, (data, error) => {
            try {
              const displayE = 'cloudInsightId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCloudInsight', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCloudInsight - errors', () => {
      it('should have a updateCloudInsight function', (done) => {
        try {
          assert.equal(true, typeof a.updateCloudInsight === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cloudInsightId', (done) => {
        try {
          a.updateCloudInsight(null, null, (data, error) => {
            try {
              const displayE = 'cloudInsightId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCloudInsight', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateCloudInsight('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCloudInsight', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudInsightCategorySummaries - errors', () => {
      it('should have a getCloudInsightCategorySummaries function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudInsightCategorySummaries === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudInsightRefreshRequests - errors', () => {
      it('should have a getCloudInsightRefreshRequests function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudInsightRefreshRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCloudInsightRefreshRequest - errors', () => {
      it('should have a createCloudInsightRefreshRequest function', (done) => {
        try {
          assert.equal(true, typeof a.createCloudInsightRefreshRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCloudInsightRefreshRequest(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createCloudInsightRefreshRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteallCloudInsightRefreshRequests - errors', () => {
      it('should have a deleteallCloudInsightRefreshRequests function', (done) => {
        try {
          assert.equal(true, typeof a.deleteallCloudInsightRefreshRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudInsightRefreshRequest - errors', () => {
      it('should have a getCloudInsightRefreshRequest function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudInsightRefreshRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cloudInsightRefreshRequestId', (done) => {
        try {
          a.getCloudInsightRefreshRequest(null, (data, error) => {
            try {
              const displayE = 'cloudInsightRefreshRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCloudInsightRefreshRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudInsightDefinitions - errors', () => {
      it('should have a getCloudInsightDefinitions function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudInsightDefinitions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudInsightDefinition - errors', () => {
      it('should have a getCloudInsightDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudInsightDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cloudInsightDefinitionId', (done) => {
        try {
          a.getCloudInsightDefinition(null, (data, error) => {
            try {
              const displayE = 'cloudInsightDefinitionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCloudInsightDefinition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCloudInsightDefinition - errors', () => {
      it('should have a updateCloudInsightDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.updateCloudInsightDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cloudInsightDefinitionId', (done) => {
        try {
          a.updateCloudInsightDefinition(null, null, (data, error) => {
            try {
              const displayE = 'cloudInsightDefinitionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCloudInsightDefinition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateCloudInsightDefinition('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCloudInsightDefinition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudInsightDefinitionSummaries - errors', () => {
      it('should have a getCloudInsightDefinitionSummaries function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudInsightDefinitionSummaries === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudInsightReportConfigurations - errors', () => {
      it('should have a getCloudInsightReportConfigurations function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudInsightReportConfigurations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCloudInsightReportConfiguration - errors', () => {
      it('should have a createCloudInsightReportConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.createCloudInsightReportConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCloudInsightReportConfiguration(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createCloudInsightReportConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteallCloudInsightReportConfigurations - errors', () => {
      it('should have a deleteallCloudInsightReportConfigurations function', (done) => {
        try {
          assert.equal(true, typeof a.deleteallCloudInsightReportConfigurations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudInsightReportConfiguration - errors', () => {
      it('should have a getCloudInsightReportConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudInsightReportConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cloudInsightReportConfigurationId', (done) => {
        try {
          a.getCloudInsightReportConfiguration(null, (data, error) => {
            try {
              const displayE = 'cloudInsightReportConfigurationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCloudInsightReportConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudInsightReports - errors', () => {
      it('should have a getCloudInsightReports function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudInsightReports === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteallCloudInsightReports - errors', () => {
      it('should have a deleteallCloudInsightReports function', (done) => {
        try {
          assert.equal(true, typeof a.deleteallCloudInsightReports === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudInsightReport - errors', () => {
      it('should have a getCloudInsightReport function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudInsightReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cloudInsightReportId', (done) => {
        try {
          a.getCloudInsightReport(null, (data, error) => {
            try {
              const displayE = 'cloudInsightReportId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCloudInsightReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCloudInsightReport - errors', () => {
      it('should have a deleteCloudInsightReport function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCloudInsightReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cloudInsightReportId', (done) => {
        try {
          a.deleteCloudInsightReport(null, (data, error) => {
            try {
              const displayE = 'cloudInsightReportId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteCloudInsightReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getbillingtags - errors', () => {
      it('should have a getbillingtags function', (done) => {
        try {
          assert.equal(true, typeof a.getbillingtags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createBillingTag - errors', () => {
      it('should have a createBillingTag function', (done) => {
        try {
          assert.equal(true, typeof a.createBillingTag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createBillingTag(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createBillingTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBillingTag - errors', () => {
      it('should have a getBillingTag function', (done) => {
        try {
          assert.equal(true, typeof a.getBillingTag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing billingTagId', (done) => {
        try {
          a.getBillingTag(null, (data, error) => {
            try {
              const displayE = 'billingTagId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getBillingTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateBillingTag - errors', () => {
      it('should have a updateBillingTag function', (done) => {
        try {
          assert.equal(true, typeof a.updateBillingTag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing billingTagId', (done) => {
        try {
          a.updateBillingTag(null, null, (data, error) => {
            try {
              const displayE = 'billingTagId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateBillingTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateBillingTag('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateBillingTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBillingTag - errors', () => {
      it('should have a deleteBillingTag function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBillingTag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing billingTagId', (done) => {
        try {
          a.deleteBillingTag(null, (data, error) => {
            try {
              const displayE = 'billingTagId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteBillingTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getArubaEdgeconnectconnectors - errors', () => {
      it('should have a getArubaEdgeconnectconnectors function', (done) => {
        try {
          assert.equal(true, typeof a.getArubaEdgeconnectconnectors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getArubaEdgeconnectconnectors(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getArubaEdgeconnectconnectors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createArubaEdgeConnectConnector - errors', () => {
      it('should have a createArubaEdgeConnectConnector function', (done) => {
        try {
          assert.equal(true, typeof a.createArubaEdgeConnectConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createArubaEdgeConnectConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createArubaEdgeConnectConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createArubaEdgeConnectConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createArubaEdgeConnectConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getArubaEdgeConnectConnector - errors', () => {
      it('should have a getArubaEdgeConnectConnector function', (done) => {
        try {
          assert.equal(true, typeof a.getArubaEdgeConnectConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getArubaEdgeConnectConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getArubaEdgeConnectConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing arubaEdgeconnectConnectorId', (done) => {
        try {
          a.getArubaEdgeConnectConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'arubaEdgeconnectConnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getArubaEdgeConnectConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateArubaEdgeConnectConnector - errors', () => {
      it('should have a updateArubaEdgeConnectConnector function', (done) => {
        try {
          assert.equal(true, typeof a.updateArubaEdgeConnectConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateArubaEdgeConnectConnector(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateArubaEdgeConnectConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing arubaEdgeconnectConnectorId', (done) => {
        try {
          a.updateArubaEdgeConnectConnector('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'arubaEdgeconnectConnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateArubaEdgeConnectConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateArubaEdgeConnectConnector('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateArubaEdgeConnectConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteArubaEdgeConnectConnector - errors', () => {
      it('should have a deleteArubaEdgeConnectConnector function', (done) => {
        try {
          assert.equal(true, typeof a.deleteArubaEdgeConnectConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteArubaEdgeConnectConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteArubaEdgeConnectConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing arubaEdgeconnectConnectorId', (done) => {
        try {
          a.deleteArubaEdgeConnectConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'arubaEdgeconnectConnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteArubaEdgeConnectConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getArubaEdgeConnectConnectorInstanceConfiguration - errors', () => {
      it('should have a getArubaEdgeConnectConnectorInstanceConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getArubaEdgeConnectConnectorInstanceConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getArubaEdgeConnectConnectorInstanceConfiguration(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getArubaEdgeConnectConnectorInstanceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing arubaEdgeconnectconnectorId', (done) => {
        try {
          a.getArubaEdgeConnectConnectorInstanceConfiguration('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'arubaEdgeconnectconnectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getArubaEdgeConnectConnectorInstanceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.getArubaEdgeConnectConnectorInstanceConfiguration('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getArubaEdgeConnectConnectorInstanceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAkamaiProlexicconnectors - errors', () => {
      it('should have a getAkamaiProlexicconnectors function', (done) => {
        try {
          assert.equal(true, typeof a.getAkamaiProlexicconnectors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getAkamaiProlexicconnectors(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getAkamaiProlexicconnectors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAkamaiProlexicConnector - errors', () => {
      it('should have a createAkamaiProlexicConnector function', (done) => {
        try {
          assert.equal(true, typeof a.createAkamaiProlexicConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createAkamaiProlexicConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createAkamaiProlexicConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAkamaiProlexicConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createAkamaiProlexicConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAkamaiProlexicConnector - errors', () => {
      it('should have a getAkamaiProlexicConnector function', (done) => {
        try {
          assert.equal(true, typeof a.getAkamaiProlexicConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getAkamaiProlexicConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getAkamaiProlexicConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getAkamaiProlexicConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getAkamaiProlexicConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAkamaiProlexicConnector - errors', () => {
      it('should have a updateAkamaiProlexicConnector function', (done) => {
        try {
          assert.equal(true, typeof a.updateAkamaiProlexicConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateAkamaiProlexicConnector(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateAkamaiProlexicConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateAkamaiProlexicConnector('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateAkamaiProlexicConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAkamaiProlexicConnector('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateAkamaiProlexicConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAkamaiProlexicConnector - errors', () => {
      it('should have a deleteAkamaiProlexicConnector function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAkamaiProlexicConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteAkamaiProlexicConnector(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteAkamaiProlexicConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteAkamaiProlexicConnector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteAkamaiProlexicConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCloudNativeServices - errors', () => {
      it('should have a getCloudNativeServices function', (done) => {
        try {
          assert.equal(true, typeof a.getCloudNativeServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCountUsingGET - errors', () => {
      it('should have a getCountUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getCountUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getCountUsingGET(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCountUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRemoteAccessTopApplications - errors', () => {
      it('should have a getRemoteAccessTopApplications function', (done) => {
        try {
          assert.equal(true, typeof a.getRemoteAccessTopApplications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getRemoteAccessTopApplications(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getRemoteAccessTopApplications', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRemoteAccessDataUtilizationUsingGET - errors', () => {
      it('should have a getRemoteAccessDataUtilizationUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getRemoteAccessDataUtilizationUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getRemoteAccessDataUtilizationUsingGET(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getRemoteAccessDataUtilizationUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRemoteAccessConnectorDataUtilizationUsingGET - errors', () => {
      it('should have a getRemoteAccessConnectorDataUtilizationUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getRemoteAccessConnectorDataUtilizationUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getRemoteAccessConnectorDataUtilizationUsingGET(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getRemoteAccessConnectorDataUtilizationUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRemoteAccessTopPolicyHits - errors', () => {
      it('should have a getRemoteAccessTopPolicyHits function', (done) => {
        try {
          assert.equal(true, typeof a.getRemoteAccessTopPolicyHits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getRemoteAccessTopPolicyHits(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getRemoteAccessTopPolicyHits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRemoteAccessBandwidthUtilizationUsingGET - errors', () => {
      it('should have a getRemoteAccessBandwidthUtilizationUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getRemoteAccessBandwidthUtilizationUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getRemoteAccessBandwidthUtilizationUsingGET(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getRemoteAccessBandwidthUtilizationUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthenticationAttemptsUsingGET - errors', () => {
      it('should have a getAuthenticationAttemptsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthenticationAttemptsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getAuthenticationAttemptsUsingGET(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getAuthenticationAttemptsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getAuthenticationAttemptsUsingGET('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getAuthenticationAttemptsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRemoteAccessSessionsUsingGET - errors', () => {
      it('should have a getRemoteAccessSessionsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getRemoteAccessSessionsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getRemoteAccessSessionsUsingGET(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getRemoteAccessSessionsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getRemoteAccessSessionsUsingGET('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getRemoteAccessSessionsUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNATPolicies - errors', () => {
      it('should have a getNATPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.getNATPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getNATPolicies(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getNATPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNATPolicy - errors', () => {
      it('should have a createNATPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.createNATPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createNATPolicy(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createNATPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNATPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createNATPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNATPolicy - errors', () => {
      it('should have a getNATPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getNATPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getNATPolicy(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getNATPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nATPolicyId', (done) => {
        try {
          a.getNATPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nATPolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getNATPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNATPolicy - errors', () => {
      it('should have a updateNATPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.updateNATPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateNATPolicy(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateNATPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nATPolicyId', (done) => {
        try {
          a.updateNATPolicy('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nATPolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateNATPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateNATPolicy('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateNATPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNATPolicy - errors', () => {
      it('should have a deleteNATPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNATPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteNATPolicy(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteNATPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nATPolicyId', (done) => {
        try {
          a.deleteNATPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nATPolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteNATPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNATRules - errors', () => {
      it('should have a getNATRules function', (done) => {
        try {
          assert.equal(true, typeof a.getNATRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getNATRules(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getNATRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNATRule - errors', () => {
      it('should have a createNATRule function', (done) => {
        try {
          assert.equal(true, typeof a.createNATRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createNATRule(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createNATRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNATRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createNATRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNATRule - errors', () => {
      it('should have a getNATRule function', (done) => {
        try {
          assert.equal(true, typeof a.getNATRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getNATRule(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getNATRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nATRuleId', (done) => {
        try {
          a.getNATRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nATRuleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getNATRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNATRule - errors', () => {
      it('should have a updateNATRule function', (done) => {
        try {
          assert.equal(true, typeof a.updateNATRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateNATRule(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateNATRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nATRuleId', (done) => {
        try {
          a.updateNATRule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nATRuleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateNATRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateNATRule('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateNATRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNATRule - errors', () => {
      it('should have a deleteNATRule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNATRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteNATRule(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteNATRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nATRuleId', (done) => {
        try {
          a.deleteNATRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nATRuleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteNATRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGlobalCidrLists - errors', () => {
      it('should have a getGlobalCidrLists function', (done) => {
        try {
          assert.equal(true, typeof a.getGlobalCidrLists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getGlobalCidrLists(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getGlobalCidrLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createGlobalCidrList - errors', () => {
      it('should have a createGlobalCidrList function', (done) => {
        try {
          assert.equal(true, typeof a.createGlobalCidrList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createGlobalCidrList(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createGlobalCidrList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createGlobalCidrList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createGlobalCidrList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGlobalCidrList - errors', () => {
      it('should have a getGlobalCidrList function', (done) => {
        try {
          assert.equal(true, typeof a.getGlobalCidrList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getGlobalCidrList(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getGlobalCidrList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing globalCidrListId', (done) => {
        try {
          a.getGlobalCidrList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'globalCidrListId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getGlobalCidrList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateGlobalCidrList - errors', () => {
      it('should have a updateGlobalCidrList function', (done) => {
        try {
          assert.equal(true, typeof a.updateGlobalCidrList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateGlobalCidrList(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateGlobalCidrList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing globalCidrListId', (done) => {
        try {
          a.updateGlobalCidrList('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'globalCidrListId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateGlobalCidrList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateGlobalCidrList('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateGlobalCidrList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGlobalCidrList - errors', () => {
      it('should have a deleteGlobalCidrList function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGlobalCidrList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteGlobalCidrList(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteGlobalCidrList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing globalCidrListId', (done) => {
        try {
          a.deleteGlobalCidrList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'globalCidrListId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteGlobalCidrList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getsegmentResources - errors', () => {
      it('should have a getsegmentResources function', (done) => {
        try {
          assert.equal(true, typeof a.getsegmentResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getsegmentResources(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getsegmentResources', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSegmentResource - errors', () => {
      it('should have a createSegmentResource function', (done) => {
        try {
          assert.equal(true, typeof a.createSegmentResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createSegmentResource(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createSegmentResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSegmentResource('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createSegmentResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSegmentResource - errors', () => {
      it('should have a getSegmentResource function', (done) => {
        try {
          assert.equal(true, typeof a.getSegmentResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getSegmentResource(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getSegmentResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing segmentResourceId', (done) => {
        try {
          a.getSegmentResource('fakeparam', null, (data, error) => {
            try {
              const displayE = 'segmentResourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getSegmentResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSegmentResource - errors', () => {
      it('should have a updateSegmentResource function', (done) => {
        try {
          assert.equal(true, typeof a.updateSegmentResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateSegmentResource(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateSegmentResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing segmentResourceId', (done) => {
        try {
          a.updateSegmentResource('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'segmentResourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateSegmentResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSegmentResource('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateSegmentResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSegmentResource - errors', () => {
      it('should have a deleteSegmentResource function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSegmentResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteSegmentResource(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteSegmentResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing segmentResourceId', (done) => {
        try {
          a.deleteSegmentResource('fakeparam', null, (data, error) => {
            try {
              const displayE = 'segmentResourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteSegmentResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSegmentResourceShares - errors', () => {
      it('should have a getSegmentResourceShares function', (done) => {
        try {
          assert.equal(true, typeof a.getSegmentResourceShares === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getSegmentResourceShares(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getSegmentResourceShares', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSegmentResourceShare - errors', () => {
      it('should have a createSegmentResourceShare function', (done) => {
        try {
          assert.equal(true, typeof a.createSegmentResourceShare === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createSegmentResourceShare(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createSegmentResourceShare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSegmentResourceShare('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createSegmentResourceShare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSegmentResourceShare - errors', () => {
      it('should have a getSegmentResourceShare function', (done) => {
        try {
          assert.equal(true, typeof a.getSegmentResourceShare === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getSegmentResourceShare(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getSegmentResourceShare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing segmentResourceShareId', (done) => {
        try {
          a.getSegmentResourceShare('fakeparam', null, (data, error) => {
            try {
              const displayE = 'segmentResourceShareId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getSegmentResourceShare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSegmentResourceShare - errors', () => {
      it('should have a updateSegmentResourceShare function', (done) => {
        try {
          assert.equal(true, typeof a.updateSegmentResourceShare === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateSegmentResourceShare(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateSegmentResourceShare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing segmentResourceShareId', (done) => {
        try {
          a.updateSegmentResourceShare('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'segmentResourceShareId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateSegmentResourceShare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSegmentResourceShare('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateSegmentResourceShare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSegmentResourceShare - errors', () => {
      it('should have a deleteSegmentResourceShare function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSegmentResourceShare === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteSegmentResourceShare(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteSegmentResourceShare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing segmentResourceShareId', (done) => {
        try {
          a.deleteSegmentResourceShare('fakeparam', null, (data, error) => {
            try {
              const displayE = 'segmentResourceShareId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteSegmentResourceShare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCheckPointFWServices - errors', () => {
      it('should have a getCheckPointFWServices function', (done) => {
        try {
          assert.equal(true, typeof a.getCheckPointFWServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getCheckPointFWServices(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCheckPointFWServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCheckPointFWService - errors', () => {
      it('should have a createCheckPointFWService function', (done) => {
        try {
          assert.equal(true, typeof a.createCheckPointFWService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createCheckPointFWService(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createCheckPointFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCheckPointFWService('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createCheckPointFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCheckPointFWService - errors', () => {
      it('should have a getCheckPointFWService function', (done) => {
        try {
          assert.equal(true, typeof a.getCheckPointFWService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getCheckPointFWService(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCheckPointFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.getCheckPointFWService('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCheckPointFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCheckPointFWService - errors', () => {
      it('should have a updateCheckPointFWService function', (done) => {
        try {
          assert.equal(true, typeof a.updateCheckPointFWService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateCheckPointFWService(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCheckPointFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.updateCheckPointFWService('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCheckPointFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateCheckPointFWService('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCheckPointFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCheckPointFWService - errors', () => {
      it('should have a deleteCheckPointFWService function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCheckPointFWService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteCheckPointFWService(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteCheckPointFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.deleteCheckPointFWService('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteCheckPointFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCheckPointFWServiceInstanceConfiguration - errors', () => {
      it('should have a getCheckPointFWServiceInstanceConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getCheckPointFWServiceInstanceConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getCheckPointFWServiceInstanceConfiguration(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCheckPointFWServiceInstanceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.getCheckPointFWServiceInstanceConfiguration('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCheckPointFWServiceInstanceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.getCheckPointFWServiceInstanceConfiguration('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCheckPointFWServiceInstanceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFortinetFWServices - errors', () => {
      it('should have a getFortinetFWServices function', (done) => {
        try {
          assert.equal(true, typeof a.getFortinetFWServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getFortinetFWServices(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getFortinetFWServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFortinetFWService - errors', () => {
      it('should have a createFortinetFWService function', (done) => {
        try {
          assert.equal(true, typeof a.createFortinetFWService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createFortinetFWService(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createFortinetFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createFortinetFWService('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createFortinetFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFortinetFWService - errors', () => {
      it('should have a getFortinetFWService function', (done) => {
        try {
          assert.equal(true, typeof a.getFortinetFWService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getFortinetFWService(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getFortinetFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.getFortinetFWService('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getFortinetFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFortinetFWService - errors', () => {
      it('should have a updateFortinetFWService function', (done) => {
        try {
          assert.equal(true, typeof a.updateFortinetFWService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateFortinetFWService(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateFortinetFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.updateFortinetFWService('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateFortinetFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFortinetFWService('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateFortinetFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFortinetFWService - errors', () => {
      it('should have a deleteFortinetFWService function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFortinetFWService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteFortinetFWService(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteFortinetFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.deleteFortinetFWService('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteFortinetFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFortinetFWServiceInstanceConfiguration - errors', () => {
      it('should have a getFortinetFWServiceInstanceConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getFortinetFWServiceInstanceConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getFortinetFWServiceInstanceConfiguration(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getFortinetFWServiceInstanceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.getFortinetFWServiceInstanceConfiguration('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getFortinetFWServiceInstanceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.getFortinetFWServiceInstanceConfiguration('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getFortinetFWServiceInstanceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCiscoFTDvFWServices - errors', () => {
      it('should have a getCiscoFTDvFWServices function', (done) => {
        try {
          assert.equal(true, typeof a.getCiscoFTDvFWServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getCiscoFTDvFWServices(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCiscoFTDvFWServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCiscoFTDvFWService - errors', () => {
      it('should have a createCiscoFTDvFWService function', (done) => {
        try {
          assert.equal(true, typeof a.createCiscoFTDvFWService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createCiscoFTDvFWService(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createCiscoFTDvFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCiscoFTDvFWService('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createCiscoFTDvFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCiscoFTDvFWService - errors', () => {
      it('should have a getCiscoFTDvFWService function', (done) => {
        try {
          assert.equal(true, typeof a.getCiscoFTDvFWService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getCiscoFTDvFWService(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCiscoFTDvFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.getCiscoFTDvFWService('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCiscoFTDvFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCiscoFTDvFWService - errors', () => {
      it('should have a updateCiscoFTDvFWService function', (done) => {
        try {
          assert.equal(true, typeof a.updateCiscoFTDvFWService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateCiscoFTDvFWService(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCiscoFTDvFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.updateCiscoFTDvFWService('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCiscoFTDvFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateCiscoFTDvFWService('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCiscoFTDvFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCiscoFTDvFWService - errors', () => {
      it('should have a deleteCiscoFTDvFWService function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCiscoFTDvFWService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteCiscoFTDvFWService(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteCiscoFTDvFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.deleteCiscoFTDvFWService('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteCiscoFTDvFWService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCiscoFTDvFWServiceInstanceConfiguration - errors', () => {
      it('should have a getCiscoFTDvFWServiceInstanceConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getCiscoFTDvFWServiceInstanceConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getCiscoFTDvFWServiceInstanceConfiguration(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCiscoFTDvFWServiceInstanceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.getCiscoFTDvFWServiceInstanceConfiguration('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCiscoFTDvFWServiceInstanceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.getCiscoFTDvFWServiceInstanceConfiguration('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCiscoFTDvFWServiceInstanceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZscalerInternetAccessServices - errors', () => {
      it('should have a getZscalerInternetAccessServices function', (done) => {
        try {
          assert.equal(true, typeof a.getZscalerInternetAccessServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getZscalerInternetAccessServices(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getZscalerInternetAccessServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createZscalerInternetAccessService - errors', () => {
      it('should have a createZscalerInternetAccessService function', (done) => {
        try {
          assert.equal(true, typeof a.createZscalerInternetAccessService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createZscalerInternetAccessService(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createZscalerInternetAccessService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createZscalerInternetAccessService('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createZscalerInternetAccessService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZscalerInternetAccessService - errors', () => {
      it('should have a getZscalerInternetAccessService function', (done) => {
        try {
          assert.equal(true, typeof a.getZscalerInternetAccessService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getZscalerInternetAccessService(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getZscalerInternetAccessService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.getZscalerInternetAccessService('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getZscalerInternetAccessService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateZscalerInternetAccessService - errors', () => {
      it('should have a updateZscalerInternetAccessService function', (done) => {
        try {
          assert.equal(true, typeof a.updateZscalerInternetAccessService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateZscalerInternetAccessService(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateZscalerInternetAccessService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.updateZscalerInternetAccessService('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateZscalerInternetAccessService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateZscalerInternetAccessService('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateZscalerInternetAccessService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteZscalerInternetAccessService - errors', () => {
      it('should have a deleteZscalerInternetAccessService function', (done) => {
        try {
          assert.equal(true, typeof a.deleteZscalerInternetAccessService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteZscalerInternetAccessService(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteZscalerInternetAccessService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.deleteZscalerInternetAccessService('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteZscalerInternetAccessService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfoBloxServices - errors', () => {
      it('should have a getInfoBloxServices function', (done) => {
        try {
          assert.equal(true, typeof a.getInfoBloxServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getInfoBloxServices(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getInfoBloxServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createInfoBloxService - errors', () => {
      it('should have a createInfoBloxService function', (done) => {
        try {
          assert.equal(true, typeof a.createInfoBloxService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createInfoBloxService(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createInfoBloxService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createInfoBloxService('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createInfoBloxService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfoBloxService - errors', () => {
      it('should have a getInfoBloxService function', (done) => {
        try {
          assert.equal(true, typeof a.getInfoBloxService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getInfoBloxService(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getInfoBloxService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.getInfoBloxService('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getInfoBloxService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateInfoBloxService - errors', () => {
      it('should have a updateInfoBloxService function', (done) => {
        try {
          assert.equal(true, typeof a.updateInfoBloxService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateInfoBloxService(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateInfoBloxService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.updateInfoBloxService('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateInfoBloxService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateInfoBloxService('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateInfoBloxService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInfoBloxService - errors', () => {
      it('should have a deleteInfoBloxService function', (done) => {
        try {
          assert.equal(true, typeof a.deleteInfoBloxService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteInfoBloxService(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteInfoBloxService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.deleteInfoBloxService('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteInfoBloxService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFeatureFlag - errors', () => {
      it('should have a getFeatureFlag function', (done) => {
        try {
          assert.equal(true, typeof a.getFeatureFlag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing feature', (done) => {
        try {
          a.getFeatureFlag(null, (data, error) => {
            try {
              const displayE = 'feature is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getFeatureFlag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenantEnabledFeatures - errors', () => {
      it('should have a getTenantEnabledFeatures function', (done) => {
        try {
          assert.equal(true, typeof a.getTenantEnabledFeatures === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#capturePacketsFromConnector - errors', () => {
      it('should have a capturePacketsFromConnector function', (done) => {
        try {
          assert.equal(true, typeof a.capturePacketsFromConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.capturePacketsFromConnector(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-capturePacketsFromConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorId', (done) => {
        try {
          a.capturePacketsFromConnector('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'connectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-capturePacketsFromConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.capturePacketsFromConnector('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-capturePacketsFromConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPacketCaptureResponses - errors', () => {
      it('should have a getPacketCaptureResponses function', (done) => {
        try {
          assert.equal(true, typeof a.getPacketCaptureResponses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadPacketCapture - errors', () => {
      it('should have a downloadPacketCapture function', (done) => {
        try {
          assert.equal(true, typeof a.downloadPacketCapture === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.downloadPacketCapture(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-downloadPacketCapture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllBYOIP - errors', () => {
      it('should have a getAllBYOIP function', (done) => {
        try {
          assert.equal(true, typeof a.getAllBYOIP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getAllBYOIP(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getAllBYOIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createBYOIP - errors', () => {
      it('should have a createBYOIP function', (done) => {
        try {
          assert.equal(true, typeof a.createBYOIP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createBYOIP(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createBYOIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createBYOIP('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createBYOIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAllBYOIP - errors', () => {
      it('should have a deleteAllBYOIP function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAllBYOIP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteAllBYOIP(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteAllBYOIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBYOIP - errors', () => {
      it('should have a getBYOIP function', (done) => {
        try {
          assert.equal(true, typeof a.getBYOIP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getBYOIP(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getBYOIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing byoipId', (done) => {
        try {
          a.getBYOIP('fakeparam', null, (data, error) => {
            try {
              const displayE = 'byoipId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getBYOIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateBYOIP - errors', () => {
      it('should have a updateBYOIP function', (done) => {
        try {
          assert.equal(true, typeof a.updateBYOIP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateBYOIP(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateBYOIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing byoipId', (done) => {
        try {
          a.updateBYOIP('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'byoipId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateBYOIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateBYOIP('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateBYOIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBYOIP - errors', () => {
      it('should have a deleteBYOIP function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBYOIP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteBYOIP(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteBYOIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing byoipId', (done) => {
        try {
          a.deleteBYOIP('fakeparam', null, (data, error) => {
            try {
              const displayE = 'byoipId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteBYOIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBYOIPReferences - errors', () => {
      it('should have a getBYOIPReferences function', (done) => {
        try {
          assert.equal(true, typeof a.getBYOIPReferences === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getBYOIPReferences(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getBYOIPReferences', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing byoipId', (done) => {
        try {
          a.getBYOIPReferences('fakeparam', null, (data, error) => {
            try {
              const displayE = 'byoipId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getBYOIPReferences', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getallcredentials - errors', () => {
      it('should have a getallcredentials function', (done) => {
        try {
          assert.equal(true, typeof a.getallcredentials === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getallcredentials(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getallcredentials', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addAWSVPCCredentialsUsingPOST - errors', () => {
      it('should have a addAWSVPCCredentialsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addAWSVPCCredentialsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.addAWSVPCCredentialsUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addAWSVPCCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addAWSVPCCredentialsUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addAWSVPCCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAWSVPCCredentialsUsingPUT - errors', () => {
      it('should have a updateAWSVPCCredentialsUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updateAWSVPCCredentialsUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateAWSVPCCredentialsUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateAWSVPCCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.updateAWSVPCCredentialsUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateAWSVPCCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAWSVPCCredentialsUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateAWSVPCCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAwsVpcCredentialsUsingDELETE - errors', () => {
      it('should have a deleteAwsVpcCredentialsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAwsVpcCredentialsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteAwsVpcCredentialsUsingDELETE(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteAwsVpcCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.deleteAwsVpcCredentialsUsingDELETE('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteAwsVpcCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addAzureCredentialsUsingPOST - errors', () => {
      it('should have a addAzureCredentialsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addAzureCredentialsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.addAzureCredentialsUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addAzureCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addAzureCredentialsUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addAzureCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAzureCredentialsUsingPUT - errors', () => {
      it('should have a updateAzureCredentialsUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updateAzureCredentialsUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateAzureCredentialsUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateAzureCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.updateAzureCredentialsUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateAzureCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAzureCredentialsUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateAzureCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAzureCredentialsUsingDELETE - errors', () => {
      it('should have a deleteAzureCredentialsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAzureCredentialsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteAzureCredentialsUsingDELETE(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteAzureCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.deleteAzureCredentialsUsingDELETE('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteAzureCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addOCIVCNCredentialsUsingPOST - errors', () => {
      it('should have a addOCIVCNCredentialsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addOCIVCNCredentialsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.addOCIVCNCredentialsUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addOCIVCNCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addOCIVCNCredentialsUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addOCIVCNCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOCIVCNCredentialsUsingPUT - errors', () => {
      it('should have a updateOCIVCNCredentialsUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updateOCIVCNCredentialsUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateOCIVCNCredentialsUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateOCIVCNCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.updateOCIVCNCredentialsUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateOCIVCNCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateOCIVCNCredentialsUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateOCIVCNCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePOCIVCNCredentialsUsingDELETE - errors', () => {
      it('should have a deletePOCIVCNCredentialsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deletePOCIVCNCredentialsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deletePOCIVCNCredentialsUsingDELETE(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deletePOCIVCNCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.deletePOCIVCNCredentialsUsingDELETE('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deletePOCIVCNCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addCiscoSDWANInstanceCredentialsUsingPOST - errors', () => {
      it('should have a addCiscoSDWANInstanceCredentialsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addCiscoSDWANInstanceCredentialsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.addCiscoSDWANInstanceCredentialsUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addCiscoSDWANInstanceCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addCiscoSDWANInstanceCredentialsUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addCiscoSDWANInstanceCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCiscoSDWANInstanceCredentialsUsingPUT - errors', () => {
      it('should have a updateCiscoSDWANInstanceCredentialsUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updateCiscoSDWANInstanceCredentialsUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateCiscoSDWANInstanceCredentialsUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCiscoSDWANInstanceCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.updateCiscoSDWANInstanceCredentialsUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCiscoSDWANInstanceCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateCiscoSDWANInstanceCredentialsUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCiscoSDWANInstanceCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCiscosdwanCredentialsUsingDELETE - errors', () => {
      it('should have a deleteCiscosdwanCredentialsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCiscosdwanCredentialsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteCiscosdwanCredentialsUsingDELETE(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteCiscosdwanCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.deleteCiscosdwanCredentialsUsingDELETE('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteCiscosdwanCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addGCPCredentialsUsingPOST - errors', () => {
      it('should have a addGCPCredentialsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addGCPCredentialsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.addGCPCredentialsUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addGCPCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addGCPCredentialsUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addGCPCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateGCPCredentialsUsingPUT - errors', () => {
      it('should have a updateGCPCredentialsUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updateGCPCredentialsUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateGCPCredentialsUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateGCPCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.updateGCPCredentialsUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateGCPCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateGCPCredentialsUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateGCPCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGcpvpcCredentialsUsingDELETE - errors', () => {
      it('should have a deleteGcpvpcCredentialsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGcpvpcCredentialsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteGcpvpcCredentialsUsingDELETE(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteGcpvpcCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.deleteGcpvpcCredentialsUsingDELETE('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteGcpvpcCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addLDAPCredentialsUsingPOST - errors', () => {
      it('should have a addLDAPCredentialsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addLDAPCredentialsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.addLDAPCredentialsUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addLDAPCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addLDAPCredentialsUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addLDAPCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateLDAPCredentialsUsingPUT - errors', () => {
      it('should have a updateLDAPCredentialsUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updateLDAPCredentialsUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateLDAPCredentialsUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateLDAPCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.updateLDAPCredentialsUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateLDAPCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateLDAPCredentialsUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateLDAPCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLDAPCredentialsUsingDELETE - errors', () => {
      it('should have a deleteLDAPCredentialsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLDAPCredentialsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteLDAPCredentialsUsingDELETE(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteLDAPCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.deleteLDAPCredentialsUsingDELETE('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteLDAPCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPANCredentialsUsingPOST - errors', () => {
      it('should have a addPANCredentialsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addPANCredentialsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.addPANCredentialsUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addPANCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addPANCredentialsUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addPANCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePANCredentialsUsingPUT - errors', () => {
      it('should have a updatePANCredentialsUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updatePANCredentialsUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updatePANCredentialsUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updatePANCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.updatePANCredentialsUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updatePANCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePANCredentialsUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updatePANCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePanCredentialsUsingDELETE - errors', () => {
      it('should have a deletePanCredentialsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deletePanCredentialsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deletePanCredentialsUsingDELETE(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deletePanCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.deletePanCredentialsUsingDELETE('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deletePanCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPANInstanceCredentialsUsingPOST - errors', () => {
      it('should have a addPANInstanceCredentialsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addPANInstanceCredentialsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.addPANInstanceCredentialsUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addPANInstanceCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addPANInstanceCredentialsUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addPANInstanceCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePANInstanceCredentialsUsingPUT - errors', () => {
      it('should have a updatePANInstanceCredentialsUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updatePANInstanceCredentialsUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updatePANInstanceCredentialsUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updatePANInstanceCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.updatePANInstanceCredentialsUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updatePANInstanceCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePANInstanceCredentialsUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updatePANInstanceCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePanInstanceCredentialsUsingDELETE - errors', () => {
      it('should have a deletePanInstanceCredentialsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deletePanInstanceCredentialsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deletePanInstanceCredentialsUsingDELETE(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deletePanInstanceCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.deletePanInstanceCredentialsUsingDELETE('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deletePanInstanceCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPANMasterKeyCredentialsUsingPOST - errors', () => {
      it('should have a addPANMasterKeyCredentialsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addPANMasterKeyCredentialsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.addPANMasterKeyCredentialsUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addPANMasterKeyCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addPANMasterKeyCredentialsUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addPANMasterKeyCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePANMasterKeyCredentialsUsingPUT - errors', () => {
      it('should have a updatePANMasterKeyCredentialsUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updatePANMasterKeyCredentialsUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updatePANMasterKeyCredentialsUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updatePANMasterKeyCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.updatePANMasterKeyCredentialsUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updatePANMasterKeyCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePANMasterKeyCredentialsUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updatePANMasterKeyCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePanMasterKeyCredentialsUsingDELETE - errors', () => {
      it('should have a deletePanMasterKeyCredentialsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deletePanMasterKeyCredentialsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deletePanMasterKeyCredentialsUsingDELETE(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deletePanMasterKeyCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.deletePanMasterKeyCredentialsUsingDELETE('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deletePanMasterKeyCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPANRegistrationCredentialsUsingPOST - errors', () => {
      it('should have a addPANRegistrationCredentialsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addPANRegistrationCredentialsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.addPANRegistrationCredentialsUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addPANRegistrationCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addPANRegistrationCredentialsUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addPANRegistrationCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePANRegistrationCredentialsUsingPUT - errors', () => {
      it('should have a updatePANRegistrationCredentialsUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updatePANRegistrationCredentialsUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updatePANRegistrationCredentialsUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updatePANRegistrationCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.updatePANRegistrationCredentialsUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updatePANRegistrationCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePANRegistrationCredentialsUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updatePANRegistrationCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePanRegistrationCredentialsUsingDELETE - errors', () => {
      it('should have a deletePanRegistrationCredentialsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deletePanRegistrationCredentialsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deletePanRegistrationCredentialsUsingDELETE(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deletePanRegistrationCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.deletePanRegistrationCredentialsUsingDELETE('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deletePanRegistrationCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addKeyPairCredentialsUsingPOST - errors', () => {
      it('should have a addKeyPairCredentialsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addKeyPairCredentialsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.addKeyPairCredentialsUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addKeyPairCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addKeyPairCredentialsUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addKeyPairCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteKeyPairCredentialsUsingDELETE - errors', () => {
      it('should have a deleteKeyPairCredentialsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteKeyPairCredentialsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteKeyPairCredentialsUsingDELETE(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteKeyPairCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.deleteKeyPairCredentialsUsingDELETE('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteKeyPairCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addArubaEdgeConnectCredentialsUsingPOST - errors', () => {
      it('should have a addArubaEdgeConnectCredentialsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addArubaEdgeConnectCredentialsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.addArubaEdgeConnectCredentialsUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addArubaEdgeConnectCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addArubaEdgeConnectCredentialsUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addArubaEdgeConnectCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteArubaEdgeConnectCredentialsUsingDELETE - errors', () => {
      it('should have a deleteArubaEdgeConnectCredentialsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteArubaEdgeConnectCredentialsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteArubaEdgeConnectCredentialsUsingDELETE(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteArubaEdgeConnectCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.deleteArubaEdgeConnectCredentialsUsingDELETE('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteArubaEdgeConnectCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addAkamaiProlexicCredentialsUsingPOST - errors', () => {
      it('should have a addAkamaiProlexicCredentialsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addAkamaiProlexicCredentialsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.addAkamaiProlexicCredentialsUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addAkamaiProlexicCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addAkamaiProlexicCredentialsUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addAkamaiProlexicCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAkamaiProlexicCredentialsUsingDELETE - errors', () => {
      it('should have a deleteAkamaiProlexicCredentialsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAkamaiProlexicCredentialsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteAkamaiProlexicCredentialsUsingDELETE(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteAkamaiProlexicCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.deleteAkamaiProlexicCredentialsUsingDELETE('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteAkamaiProlexicCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addCheckPointFWServiceCredentialsUsingPOST - errors', () => {
      it('should have a addCheckPointFWServiceCredentialsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addCheckPointFWServiceCredentialsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.addCheckPointFWServiceCredentialsUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addCheckPointFWServiceCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addCheckPointFWServiceCredentialsUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addCheckPointFWServiceCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCheckPointFWServiceCredentialsUsingPUT - errors', () => {
      it('should have a updateCheckPointFWServiceCredentialsUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updateCheckPointFWServiceCredentialsUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateCheckPointFWServiceCredentialsUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCheckPointFWServiceCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.updateCheckPointFWServiceCredentialsUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCheckPointFWServiceCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateCheckPointFWServiceCredentialsUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCheckPointFWServiceCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePCheckPointFWServiceCredentialsUsingDELETE - errors', () => {
      it('should have a deletePCheckPointFWServiceCredentialsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deletePCheckPointFWServiceCredentialsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deletePCheckPointFWServiceCredentialsUsingDELETE(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deletePCheckPointFWServiceCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.deletePCheckPointFWServiceCredentialsUsingDELETE('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deletePCheckPointFWServiceCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addCheckPointFWServiceInstanceCredentialsUsingPOST - errors', () => {
      it('should have a addCheckPointFWServiceInstanceCredentialsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addCheckPointFWServiceInstanceCredentialsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.addCheckPointFWServiceInstanceCredentialsUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addCheckPointFWServiceInstanceCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addCheckPointFWServiceInstanceCredentialsUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addCheckPointFWServiceInstanceCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCheckPointFWServiceInstanceCredentialsUsingPUT - errors', () => {
      it('should have a updateCheckPointFWServiceInstanceCredentialsUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updateCheckPointFWServiceInstanceCredentialsUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateCheckPointFWServiceInstanceCredentialsUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCheckPointFWServiceInstanceCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.updateCheckPointFWServiceInstanceCredentialsUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCheckPointFWServiceInstanceCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateCheckPointFWServiceInstanceCredentialsUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCheckPointFWServiceInstanceCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePCheckPointFWServiceInstanceCredentialsUsingDELETE - errors', () => {
      it('should have a deletePCheckPointFWServiceInstanceCredentialsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deletePCheckPointFWServiceInstanceCredentialsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deletePCheckPointFWServiceInstanceCredentialsUsingDELETE(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deletePCheckPointFWServiceInstanceCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.deletePCheckPointFWServiceInstanceCredentialsUsingDELETE('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deletePCheckPointFWServiceInstanceCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addCheckPointFWManagementServerCredentialsUsingPOST - errors', () => {
      it('should have a addCheckPointFWManagementServerCredentialsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addCheckPointFWManagementServerCredentialsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.addCheckPointFWManagementServerCredentialsUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addCheckPointFWManagementServerCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addCheckPointFWManagementServerCredentialsUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addCheckPointFWManagementServerCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCheckPointFWManagementServerCredentialsUsingPUT - errors', () => {
      it('should have a updateCheckPointFWManagementServerCredentialsUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updateCheckPointFWManagementServerCredentialsUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateCheckPointFWManagementServerCredentialsUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCheckPointFWManagementServerCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.updateCheckPointFWManagementServerCredentialsUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCheckPointFWManagementServerCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateCheckPointFWManagementServerCredentialsUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCheckPointFWManagementServerCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePCheckPointFWManagementServerCredentialsUsingDELETE - errors', () => {
      it('should have a deletePCheckPointFWManagementServerCredentialsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deletePCheckPointFWManagementServerCredentialsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deletePCheckPointFWManagementServerCredentialsUsingDELETE(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deletePCheckPointFWManagementServerCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.deletePCheckPointFWManagementServerCredentialsUsingDELETE('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deletePCheckPointFWManagementServerCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addUsernamePasswordCredentialsUsingPOST - errors', () => {
      it('should have a addUsernamePasswordCredentialsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addUsernamePasswordCredentialsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.addUsernamePasswordCredentialsUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addUsernamePasswordCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addUsernamePasswordCredentialsUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addUsernamePasswordCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateUsernamePasswordCredentialsUsingPUT - errors', () => {
      it('should have a updateUsernamePasswordCredentialsUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updateUsernamePasswordCredentialsUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateUsernamePasswordCredentialsUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateUsernamePasswordCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.updateUsernamePasswordCredentialsUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateUsernamePasswordCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateUsernamePasswordCredentialsUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateUsernamePasswordCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsernamePasswordCredentialsUsingDELETE - errors', () => {
      it('should have a deleteUsernamePasswordCredentialsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUsernamePasswordCredentialsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteUsernamePasswordCredentialsUsingDELETE(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteUsernamePasswordCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.deleteUsernamePasswordCredentialsUsingDELETE('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteUsernamePasswordCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSplunkHECTokenCredentialsUsingPOST - errors', () => {
      it('should have a addSplunkHECTokenCredentialsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addSplunkHECTokenCredentialsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.addSplunkHECTokenCredentialsUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addSplunkHECTokenCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSplunkHECTokenCredentialsUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addSplunkHECTokenCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSplunkHECTokenCredentialsUsingPUT - errors', () => {
      it('should have a updateSplunkHECTokenCredentialsUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updateSplunkHECTokenCredentialsUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateSplunkHECTokenCredentialsUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateSplunkHECTokenCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.updateSplunkHECTokenCredentialsUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateSplunkHECTokenCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSplunkHECTokenCredentialsUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateSplunkHECTokenCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSplunkHECTokenCredentialsUsingDELETE - errors', () => {
      it('should have a deleteSplunkHECTokenCredentialsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSplunkHECTokenCredentialsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteSplunkHECTokenCredentialsUsingDELETE(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteSplunkHECTokenCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.deleteSplunkHECTokenCredentialsUsingDELETE('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteSplunkHECTokenCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addFtntFWServiceCredentialsUsingPOST - errors', () => {
      it('should have a addFtntFWServiceCredentialsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addFtntFWServiceCredentialsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.addFtntFWServiceCredentialsUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addFtntFWServiceCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addFtntFWServiceCredentialsUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addFtntFWServiceCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFtntFWServiceCredentialsUsingPUT - errors', () => {
      it('should have a updateFtntFWServiceCredentialsUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updateFtntFWServiceCredentialsUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateFtntFWServiceCredentialsUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateFtntFWServiceCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.updateFtntFWServiceCredentialsUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateFtntFWServiceCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFtntFWServiceCredentialsUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateFtntFWServiceCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFtntFWServiceCredentialsUsingDELETE - errors', () => {
      it('should have a deleteFtntFWServiceCredentialsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFtntFWServiceCredentialsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteFtntFWServiceCredentialsUsingDELETE(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteFtntFWServiceCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.deleteFtntFWServiceCredentialsUsingDELETE('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteFtntFWServiceCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addFtntFWServiceInstanceCredentialsUsingPOST - errors', () => {
      it('should have a addFtntFWServiceInstanceCredentialsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addFtntFWServiceInstanceCredentialsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.addFtntFWServiceInstanceCredentialsUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addFtntFWServiceInstanceCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addFtntFWServiceInstanceCredentialsUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addFtntFWServiceInstanceCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFtntFWServiceInstanceCredentialsUsingPUT - errors', () => {
      it('should have a updateFtntFWServiceInstanceCredentialsUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updateFtntFWServiceInstanceCredentialsUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateFtntFWServiceInstanceCredentialsUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateFtntFWServiceInstanceCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.updateFtntFWServiceInstanceCredentialsUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateFtntFWServiceInstanceCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFtntFWServiceInstanceCredentialsUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateFtntFWServiceInstanceCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFtntFWServiceInstanceCredentialsUsingDELETE - errors', () => {
      it('should have a deleteFtntFWServiceInstanceCredentialsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFtntFWServiceInstanceCredentialsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteFtntFWServiceInstanceCredentialsUsingDELETE(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteFtntFWServiceInstanceCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.deleteFtntFWServiceInstanceCredentialsUsingDELETE('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteFtntFWServiceInstanceCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addCiscoFTDvFWCredentialsUsingPOST - errors', () => {
      it('should have a addCiscoFTDvFWCredentialsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addCiscoFTDvFWCredentialsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.addCiscoFTDvFWCredentialsUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addCiscoFTDvFWCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addCiscoFTDvFWCredentialsUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addCiscoFTDvFWCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCiscoFTDvFWCredentialsUsingPUT - errors', () => {
      it('should have a updateCiscoFTDvFWCredentialsUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updateCiscoFTDvFWCredentialsUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateCiscoFTDvFWCredentialsUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCiscoFTDvFWCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.updateCiscoFTDvFWCredentialsUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCiscoFTDvFWCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateCiscoFTDvFWCredentialsUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCiscoFTDvFWCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCiscoFTDvFWCredentialsUsingDELETE - errors', () => {
      it('should have a deleteCiscoFTDvFWCredentialsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCiscoFTDvFWCredentialsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteCiscoFTDvFWCredentialsUsingDELETE(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteCiscoFTDvFWCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.deleteCiscoFTDvFWCredentialsUsingDELETE('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteCiscoFTDvFWCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addCiscoFTDvFWInstanceCredentialsUsingPOST - errors', () => {
      it('should have a addCiscoFTDvFWInstanceCredentialsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addCiscoFTDvFWInstanceCredentialsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.addCiscoFTDvFWInstanceCredentialsUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addCiscoFTDvFWInstanceCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addCiscoFTDvFWInstanceCredentialsUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addCiscoFTDvFWInstanceCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCiscoFTDvFWInstanceCredentialsUsingPUT - errors', () => {
      it('should have a updateCiscoFTDvFWInstanceCredentialsUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updateCiscoFTDvFWInstanceCredentialsUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateCiscoFTDvFWInstanceCredentialsUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCiscoFTDvFWInstanceCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.updateCiscoFTDvFWInstanceCredentialsUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCiscoFTDvFWInstanceCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateCiscoFTDvFWInstanceCredentialsUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCiscoFTDvFWInstanceCredentialsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCiscoFTDvFWInstanceCredentialsUsingDELETE - errors', () => {
      it('should have a deleteCiscoFTDvFWInstanceCredentialsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCiscoFTDvFWInstanceCredentialsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteCiscoFTDvFWInstanceCredentialsUsingDELETE(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteCiscoFTDvFWInstanceCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.deleteCiscoFTDvFWInstanceCredentialsUsingDELETE('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteCiscoFTDvFWInstanceCredentialsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addInfoBloxCredentialsUsingPOST - errors', () => {
      it('should have a addInfoBloxCredentialsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addInfoBloxCredentialsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.addInfoBloxCredentialsUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addInfoBloxCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addInfoBloxCredentialsUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addInfoBloxCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addInfoBloxInstanceCredentialsUsingPOST - errors', () => {
      it('should have a addInfoBloxInstanceCredentialsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addInfoBloxInstanceCredentialsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.addInfoBloxInstanceCredentialsUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addInfoBloxInstanceCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addInfoBloxInstanceCredentialsUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addInfoBloxInstanceCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addInfoBloxGridMasterCredentialsUsingPOST - errors', () => {
      it('should have a addInfoBloxGridMasterCredentialsUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addInfoBloxGridMasterCredentialsUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.addInfoBloxGridMasterCredentialsUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addInfoBloxGridMasterCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addInfoBloxGridMasterCredentialsUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-addInfoBloxGridMasterCredentialsUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllUnexpiredOTP - errors', () => {
      it('should have a getAllUnexpiredOTP function', (done) => {
        try {
          assert.equal(true, typeof a.getAllUnexpiredOTP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOneTimePassword - errors', () => {
      it('should have a createOneTimePassword function', (done) => {
        try {
          assert.equal(true, typeof a.createOneTimePassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteValidOneTimePasswords - errors', () => {
      it('should have a deleteValidOneTimePasswords function', (done) => {
        try {
          assert.equal(true, typeof a.deleteValidOneTimePasswords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOneTimePasswordById - errors', () => {
      it('should have a deleteOneTimePasswordById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOneTimePasswordById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing otpId', (done) => {
        try {
          a.deleteOneTimePasswordById(null, (data, error) => {
            try {
              const displayE = 'otpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteOneTimePasswordById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoginSpecification - errors', () => {
      it('should have a getLoginSpecification function', (done) => {
        try {
          assert.equal(true, typeof a.getLoginSpecification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOrUpdateLoginSpecification - errors', () => {
      it('should have a createOrUpdateLoginSpecification function', (done) => {
        try {
          assert.equal(true, typeof a.createOrUpdateLoginSpecification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLoginSpecification - errors', () => {
      it('should have a deleteLoginSpecification function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLoginSpecification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPasswordSpecification - errors', () => {
      it('should have a getPasswordSpecification function', (done) => {
        try {
          assert.equal(true, typeof a.getPasswordSpecification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOrUpdatePasswordSpecification - errors', () => {
      it('should have a createOrUpdatePasswordSpecification function', (done) => {
        try {
          assert.equal(true, typeof a.createOrUpdatePasswordSpecification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePasswordSpecification - errors', () => {
      it('should have a deletePasswordSpecification function', (done) => {
        try {
          assert.equal(true, typeof a.deletePasswordSpecification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRoutePolicies - errors', () => {
      it('should have a getAllRoutePolicies function', (done) => {
        try {
          assert.equal(true, typeof a.getAllRoutePolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getAllRoutePolicies(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getAllRoutePolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRoutePolicies - errors', () => {
      it('should have a createRoutePolicies function', (done) => {
        try {
          assert.equal(true, typeof a.createRoutePolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createRoutePolicies(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createRoutePolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRoutePolicies('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createRoutePolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAllRoutePolicies - errors', () => {
      it('should have a deleteAllRoutePolicies function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAllRoutePolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteAllRoutePolicies(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteAllRoutePolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoutePolicy - errors', () => {
      it('should have a getRoutePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getRoutePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getRoutePolicy(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getRoutePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routePolicyId', (done) => {
        try {
          a.getRoutePolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'routePolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getRoutePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRoutePolicy - errors', () => {
      it('should have a updateRoutePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.updateRoutePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateRoutePolicy(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateRoutePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routePolicyId', (done) => {
        try {
          a.updateRoutePolicy('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'routePolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateRoutePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateRoutePolicy('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateRoutePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRoutePolicy - errors', () => {
      it('should have a deleteRoutePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRoutePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteRoutePolicy(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteRoutePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routePolicyId', (done) => {
        try {
          a.deleteRoutePolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'routePolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteRoutePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInvoiceDetails - errors', () => {
      it('should have a getInvoiceDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getInvoiceDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInvoiceDetail - errors', () => {
      it('should have a getInvoiceDetail function', (done) => {
        try {
          assert.equal(true, typeof a.getInvoiceDetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing invoiceId', (done) => {
        try {
          a.getInvoiceDetail(null, (data, error) => {
            try {
              const displayE = 'invoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getInvoiceDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMeters - errors', () => {
      it('should have a getMeters function', (done) => {
        try {
          assert.equal(true, typeof a.getMeters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMeterItems - errors', () => {
      it('should have a getMeterItems function', (done) => {
        try {
          assert.equal(true, typeof a.getMeterItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing meterId', (done) => {
        try {
          a.getMeterItems(null, null, null, null, (data, error) => {
            try {
              const displayE = 'meterId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getMeterItems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoutes - errors', () => {
      it('should have a getRoutes function', (done) => {
        try {
          assert.equal(true, typeof a.getRoutes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getRoutes(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getRoutes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicies - errors', () => {
      it('should have a getPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.getPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getPolicies(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPolicy - errors', () => {
      it('should have a createPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.createPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createPolicy(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicy - errors', () => {
      it('should have a getPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getPolicy(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePolicy - errors', () => {
      it('should have a updatePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.updatePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updatePolicy(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updatePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updatePolicy('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updatePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePolicy('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updatePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicy - errors', () => {
      it('should have a deletePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deletePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deletePolicy(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deletePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deletePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRules - errors', () => {
      it('should have a getRules function', (done) => {
        try {
          assert.equal(true, typeof a.getRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getRules(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRule - errors', () => {
      it('should have a createRule function', (done) => {
        try {
          assert.equal(true, typeof a.createRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createRule(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRule - errors', () => {
      it('should have a getRule function', (done) => {
        try {
          assert.equal(true, typeof a.getRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getRule(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRule - errors', () => {
      it('should have a updateRule function', (done) => {
        try {
          assert.equal(true, typeof a.updateRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateRule(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateRule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateRule('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRule - errors', () => {
      it('should have a deleteRule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteRule(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRulelists - errors', () => {
      it('should have a getRulelists function', (done) => {
        try {
          assert.equal(true, typeof a.getRulelists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getRulelists(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getRulelists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRulelist - errors', () => {
      it('should have a createRulelist function', (done) => {
        try {
          assert.equal(true, typeof a.createRulelist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createRulelist(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createRulelist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRulelist('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createRulelist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRulelist - errors', () => {
      it('should have a getRulelist function', (done) => {
        try {
          assert.equal(true, typeof a.getRulelist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getRulelist(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getRulelist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRulelist('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getRulelist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRulelist - errors', () => {
      it('should have a updateRulelist function', (done) => {
        try {
          assert.equal(true, typeof a.updateRulelist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateRulelist(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateRulelist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateRulelist('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateRulelist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateRulelist('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateRulelist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRulelist - errors', () => {
      it('should have a deleteRulelist function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRulelist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteRulelist(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteRulelist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRulelist('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteRulelist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPrefixlists - errors', () => {
      it('should have a getPrefixlists function', (done) => {
        try {
          assert.equal(true, typeof a.getPrefixlists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getPrefixlists(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getPrefixlists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPrefixlist - errors', () => {
      it('should have a createPrefixlist function', (done) => {
        try {
          assert.equal(true, typeof a.createPrefixlist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createPrefixlist(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createPrefixlist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createPrefixlist('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createPrefixlist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPrefixlist - errors', () => {
      it('should have a getPrefixlist function', (done) => {
        try {
          assert.equal(true, typeof a.getPrefixlist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getPrefixlist(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getPrefixlist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPrefixlist('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getPrefixlist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePrefixlist - errors', () => {
      it('should have a updatePrefixlist function', (done) => {
        try {
          assert.equal(true, typeof a.updatePrefixlist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updatePrefixlist(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updatePrefixlist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updatePrefixlist('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updatePrefixlist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePrefixlist('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updatePrefixlist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePrefixlist - errors', () => {
      it('should have a deletePrefixlist function', (done) => {
        try {
          assert.equal(true, typeof a.deletePrefixlist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deletePrefixlist(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deletePrefixlist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePrefixlist('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deletePrefixlist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveshealthforallconnectorsandservicesinthistenant - errors', () => {
      it('should have a retrieveshealthforallconnectorsandservicesinthistenant function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveshealthforallconnectorsandservicesinthistenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.retrieveshealthforallconnectorsandservicesinthistenant(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-retrieveshealthforallconnectorsandservicesinthistenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveshealthforaspecificconnectorinthistenant - errors', () => {
      it('should have a retrieveshealthforaspecificconnectorinthistenant function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveshealthforaspecificconnectorinthistenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.retrieveshealthforaspecificconnectorinthistenant(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-retrieveshealthforaspecificconnectorinthistenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorId', (done) => {
        try {
          a.retrieveshealthforaspecificconnectorinthistenant('fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-retrieveshealthforaspecificconnectorinthistenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveshealthforaspecificinstanceofaconnectorinthistenant - errors', () => {
      it('should have a retrieveshealthforaspecificinstanceofaconnectorinthistenant function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveshealthforaspecificinstanceofaconnectorinthistenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.retrieveshealthforaspecificinstanceofaconnectorinthistenant(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-retrieveshealthforaspecificinstanceofaconnectorinthistenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorId', (done) => {
        try {
          a.retrieveshealthforaspecificinstanceofaconnectorinthistenant('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'connectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-retrieveshealthforaspecificinstanceofaconnectorinthistenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.retrieveshealthforaspecificinstanceofaconnectorinthistenant('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-retrieveshealthforaspecificinstanceofaconnectorinthistenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveshealthforallinternetapplicationsinthistenant - errors', () => {
      it('should have a retrieveshealthforallinternetapplicationsinthistenant function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveshealthforallinternetapplicationsinthistenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.retrieveshealthforallinternetapplicationsinthistenant(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-retrieveshealthforallinternetapplicationsinthistenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveshealthforaspecificserviceinthistenant - errors', () => {
      it('should have a retrieveshealthforaspecificserviceinthistenant function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveshealthforaspecificserviceinthistenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.retrieveshealthforaspecificserviceinthistenant(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-retrieveshealthforaspecificserviceinthistenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.retrieveshealthforaspecificserviceinthistenant('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-retrieveshealthforaspecificserviceinthistenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveshealthforaspecificinstanceofaserviceinthistenant - errors', () => {
      it('should have a retrieveshealthforaspecificinstanceofaserviceinthistenant function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveshealthforaspecificinstanceofaserviceinthistenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.retrieveshealthforaspecificinstanceofaserviceinthistenant(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-retrieveshealthforaspecificinstanceofaserviceinthistenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.retrieveshealthforaspecificinstanceofaserviceinthistenant('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-retrieveshealthforaspecificinstanceofaserviceinthistenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.retrieveshealthforaspecificinstanceofaserviceinthistenant('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-retrieveshealthforaspecificinstanceofaserviceinthistenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveshealthforaspecificfirewalzoneinthistenant - errors', () => {
      it('should have a retrieveshealthforaspecificfirewalzoneinthistenant function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveshealthforaspecificfirewalzoneinthistenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.retrieveshealthforaspecificfirewalzoneinthistenant(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-retrieveshealthforaspecificfirewalzoneinthistenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneId', (done) => {
        try {
          a.retrieveshealthforaspecificfirewalzoneinthistenant('fakeparam', null, (data, error) => {
            try {
              const displayE = 'zoneId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-retrieveshealthforaspecificfirewalzoneinthistenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRetrieveshealthforallconnectorsandservicesinthistenant - errors', () => {
      it('should have a getRetrieveshealthforallconnectorsandservicesinthistenant function', (done) => {
        try {
          assert.equal(true, typeof a.getRetrieveshealthforallconnectorsandservicesinthistenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getRetrieveshealthforallconnectorsandservicesinthistenant(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getRetrieveshealthforallconnectorsandservicesinthistenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#captureFlowsFromConnector - errors', () => {
      it('should have a captureFlowsFromConnector function', (done) => {
        try {
          assert.equal(true, typeof a.captureFlowsFromConnector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.captureFlowsFromConnector(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-captureFlowsFromConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorId', (done) => {
        try {
          a.captureFlowsFromConnector('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'connectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-captureFlowsFromConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.captureFlowsFromConnector('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-captureFlowsFromConnector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#captureFlowsFromResourceShare - errors', () => {
      it('should have a captureFlowsFromResourceShare function', (done) => {
        try {
          assert.equal(true, typeof a.captureFlowsFromResourceShare === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.captureFlowsFromResourceShare(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-captureFlowsFromResourceShare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing segmentResourceShareId', (done) => {
        try {
          a.captureFlowsFromResourceShare('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'segmentResourceShareId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-captureFlowsFromResourceShare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.captureFlowsFromResourceShare('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-captureFlowsFromResourceShare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveNatTranslation - errors', () => {
      it('should have a getActiveNatTranslation function', (done) => {
        try {
          assert.equal(true, typeof a.getActiveNatTranslation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getActiveNatTranslation(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getActiveNatTranslation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorId', (done) => {
        try {
          a.getActiveNatTranslation('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'connectorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getActiveNatTranslation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getActiveNatTranslation('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getActiveNatTranslation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveNatTranslationForResourceShare - errors', () => {
      it('should have a getActiveNatTranslationForResourceShare function', (done) => {
        try {
          assert.equal(true, typeof a.getActiveNatTranslationForResourceShare === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getActiveNatTranslationForResourceShare(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getActiveNatTranslationForResourceShare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing segmentResourceShareId', (done) => {
        try {
          a.getActiveNatTranslationForResourceShare('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'segmentResourceShareId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getActiveNatTranslationForResourceShare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getActiveNatTranslationForResourceShare('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getActiveNatTranslationForResourceShare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllAsPathLists - errors', () => {
      it('should have a getAllAsPathLists function', (done) => {
        try {
          assert.equal(true, typeof a.getAllAsPathLists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getAllAsPathLists(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getAllAsPathLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAsPathLists - errors', () => {
      it('should have a createAsPathLists function', (done) => {
        try {
          assert.equal(true, typeof a.createAsPathLists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createAsPathLists(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createAsPathLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAsPathLists('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createAsPathLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAllAsPathLists - errors', () => {
      it('should have a deleteAllAsPathLists function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAllAsPathLists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteAllAsPathLists(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteAllAsPathLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAsPathList - errors', () => {
      it('should have a getAsPathList function', (done) => {
        try {
          assert.equal(true, typeof a.getAsPathList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getAsPathList(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getAsPathList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing asPathListId', (done) => {
        try {
          a.getAsPathList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'asPathListId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getAsPathList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAsPathList - errors', () => {
      it('should have a updateAsPathList function', (done) => {
        try {
          assert.equal(true, typeof a.updateAsPathList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateAsPathList(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateAsPathList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing asPathListId', (done) => {
        try {
          a.updateAsPathList('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'asPathListId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateAsPathList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAsPathList('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateAsPathList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAsPathList - errors', () => {
      it('should have a deleteAsPathList function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAsPathList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteAsPathList(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteAsPathList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing asPathListId', (done) => {
        try {
          a.deleteAsPathList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'asPathListId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteAsPathList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllCommunityLists - errors', () => {
      it('should have a getAllCommunityLists function', (done) => {
        try {
          assert.equal(true, typeof a.getAllCommunityLists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getAllCommunityLists(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getAllCommunityLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCommunityLists - errors', () => {
      it('should have a createCommunityLists function', (done) => {
        try {
          assert.equal(true, typeof a.createCommunityLists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createCommunityLists(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createCommunityLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCommunityLists('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createCommunityLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAllCommunityLists - errors', () => {
      it('should have a deleteAllCommunityLists function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAllCommunityLists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteAllCommunityLists(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteAllCommunityLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCommunityList - errors', () => {
      it('should have a getCommunityList function', (done) => {
        try {
          assert.equal(true, typeof a.getCommunityList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getCommunityList(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCommunityList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing communityListId', (done) => {
        try {
          a.getCommunityList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'communityListId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getCommunityList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCommunityList - errors', () => {
      it('should have a updateCommunityList function', (done) => {
        try {
          assert.equal(true, typeof a.updateCommunityList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateCommunityList(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCommunityList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing communityListId', (done) => {
        try {
          a.updateCommunityList('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'communityListId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCommunityList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateCommunityList('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateCommunityList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCommunityList - errors', () => {
      it('should have a deleteCommunityList function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCommunityList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteCommunityList(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteCommunityList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing communityListId', (done) => {
        try {
          a.deleteCommunityList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'communityListId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteCommunityList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllExtendedCommunityLists - errors', () => {
      it('should have a getAllExtendedCommunityLists function', (done) => {
        try {
          assert.equal(true, typeof a.getAllExtendedCommunityLists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getAllExtendedCommunityLists(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getAllExtendedCommunityLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createExtendedCommunityLists - errors', () => {
      it('should have a createExtendedCommunityLists function', (done) => {
        try {
          assert.equal(true, typeof a.createExtendedCommunityLists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.createExtendedCommunityLists(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createExtendedCommunityLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createExtendedCommunityLists('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-createExtendedCommunityLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAllExtendedCommunityLists - errors', () => {
      it('should have a deleteAllExtendedCommunityLists function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAllExtendedCommunityLists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteAllExtendedCommunityLists(null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteAllExtendedCommunityLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtendedCommunityList - errors', () => {
      it('should have a getExtendedCommunityList function', (done) => {
        try {
          assert.equal(true, typeof a.getExtendedCommunityList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.getExtendedCommunityList(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getExtendedCommunityList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extendedCommunityListId', (done) => {
        try {
          a.getExtendedCommunityList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'extendedCommunityListId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-getExtendedCommunityList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateExtendedCommunityList - errors', () => {
      it('should have a updateExtendedCommunityList function', (done) => {
        try {
          assert.equal(true, typeof a.updateExtendedCommunityList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.updateExtendedCommunityList(null, null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateExtendedCommunityList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extendedCommunityListId', (done) => {
        try {
          a.updateExtendedCommunityList('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extendedCommunityListId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateExtendedCommunityList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateExtendedCommunityList('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-updateExtendedCommunityList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtendedCommunityList - errors', () => {
      it('should have a deleteExtendedCommunityList function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtendedCommunityList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantNetworkId', (done) => {
        try {
          a.deleteExtendedCommunityList(null, null, (data, error) => {
            try {
              const displayE = 'tenantNetworkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteExtendedCommunityList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extendedCommunityListId', (done) => {
        try {
          a.deleteExtendedCommunityList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'extendedCommunityListId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-alkira-adapter-deleteExtendedCommunityList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
