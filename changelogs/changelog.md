
## 0.1.7 [08-01-2023]

* Bug fixes and performance improvements

See commit a5678d1

---

## 0.1.6 [08-01-2023]

* Migrate adapter base with broker changes

See merge request itentialopensource/adapters/cloud/adapter-alkira!4

---

## 0.1.5 [05-02-2023]

* Migrate adapter base with broker changes

See merge request itentialopensource/adapters/cloud/adapter-alkira!4

---

## 0.1.4 [04-12-2023]

* Patch/fixed get config and get count in adapterBase.json

See merge request itentialopensource/adapters/cloud/adapter-alkira!3

---

## 0.1.3 [04-06-2023]

* bug fix for getConfig and iapGetDeviceCount in adapterBase.js

See merge request itentialopensource/adapters/cloud/adapter-alkira!2

---

## 0.1.2 [12-21-2022]

* Updates to authentication schemas

See merge request itentialopensource/adapters/cloud/adapter-alkira!1

---

## 0.1.1 [11-28-2022]

* Bug fixes and performance improvements

See commit c896979

---
